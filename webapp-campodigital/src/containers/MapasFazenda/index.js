import React, { Component } from 'react';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from "react-router-dom";
import axios from "axios";
import { Table, Button, Alert, Spin } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import actions from '../../redux/mapaTalhao/actions';
import settings from '../../settings';
import { getToken } from '../../helpers/utility';

const { apiUrl } = settings;
const config = {
  headers: {
    Authorization : getToken().get('idToken'),
  },
  responseType: 'blob',
};

class MapasFazenda extends Component {

  componentDidMount() {
    this.props.fetchAll();
  }

  componentWillUnmount() {
    this.props.clearResposta();
  }

  download(fileName) {
    axios.get(apiUrl+'mapas-talhao/'+fileName, config)
    .then(response => {
      console.log(response);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      console.log(error);
    });
  }

  renderAlert() {
    if (this.props.resposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  render() {
    const columns = [
      {
        title: 'Talhão', dataIndex: 'numero', key: 'numero',
        sorter: (a, b) => a.numero === b.numero ? 0 : a.numero > b.numero ? 1 : -1,
      },
      {
        title: 'Seção', dataIndex: 'secao', key: 'secao',
        sorter: (a, b) =>
          a.record.secao === b.record.secao
          ?
          0 : a.record.secao > b.record.secao ? 1 : -1,
      },
      {
        title: 'Fazenda', dataIndex: 'fazenda', key: 'fazenda',
        sorter: (a, b) =>
          a.record.fazenda === b.record.fazenda
          ?
          0 : a.record.fazenda > b.record.fazenda ? 1 : -1,
      },
      {
        title: 'Arquivo', dataIndex: 'file', key: 'file',
        render: (text, record) =>
          <a onClick={() => this.download(record.mapaPDF.fileName)}>{record.mapaPDF.fileName}</a>,
      },
      { title: 'Ativo?', dataIndex: 'ativo', key: 'ativo',
        render: (text, record) => record.ativo ? 'Ativo' : 'Inativo',
        sorter: (a, b) => a.ativo > b.ativo,
      },
    ];

    return (
      <LayoutContent>
        <PageHeader>Mapas &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          <Link to={`/dashboard/mapas/new`}>
            <Button type="primary">
              Novo
            </Button>
          </Link>
          <br/><br/>
          <Box>
            <Table
              pagination={false}
              columns={columns}
              dataSource={this.props.usuarios}
            />
          </Box>
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.MapaTalhao.resposta,
  tipoResposta: state.MapaTalhao.tipoResposta,
  usuarios: state.MapaTalhao.all,
  loading: state.MapaTalhao.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  { 
    fetchAll: actions.fetchAll,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(MapasFazenda);