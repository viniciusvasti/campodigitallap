import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import {
  Spin,
  Select,
  Form,
  Row,
  Col,
  Upload,
  Button,
  Icon,
  message,
} from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import GroupButtonForm from '../../components/groupButtonForm';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actionsFazenda from '../../redux/fazenda/actions';
import { getToken } from '../../helpers/utility';
import settings from '../../settings';

const FormItem = Form.Item;
const Option = Select.Option;
const { apiUrl } = settings;

class MapaNew extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      uploading: false,
      tipoResposta: '',
      resposta: '',
    };
  }

  componentWillMount() {
    this.props.fetchAllFazenda();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { file } = this.state;
        const isPDF = file.type === 'application/pdf';
        if (!isPDF) {
          message.error('O arquivo deve ser um PDF!');
          return false;
        }
        let fileName = `f${values.fazenda.id}s${values.secao.id}${values.talhoes.map(x => 't'+x)}.pdf`;
        fileName = fileName.replace(',','');
        
        const formData = new FormData();
        formData.append('file', file, fileName);
        formData.append('talhoes', values.talhoes);

        axios.post(apiUrl+'mapas-talhao', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization : getToken().get('idToken'),
          }
        }).then(() => {
          console.log('teste')
          this.setState({ tipoResposta: 'success', resposta: 'Arquivo enviado com sucesso!' });
        }).catch((erro) => {
          this.setState({ tipoResposta: 'error', resposta: 'Erro ao enviar arquivo.' });
        });
      }
    });
  }

  clearResposta = () => {
    this.setState({ tipoResposta: '', resposta: '' });
  }

  renderAlert() {
    if (this.state.tipoResposta && this.state.tipoResposta !== 'success') {
      message.error(this.state.resposta);
    }
    if (this.state.tipoResposta && this.state.tipoResposta === 'success') {
      message.success(this.state.resposta);
    }
  }

  render() {

    this.renderAlert();
    if (this.state.tipoResposta === 'success') {
      this.props.history.push('/dashboard/mapas');
    }

    const { getFieldDecorator, getFieldValue } = this.props.form;

    let optionsSecoes = [];
    let optionsTalhoes = [];
    let optionsFazendas = [];
    
    try {
      optionsFazendas = this.props.fazendas ?
        this.props.fazendas
        .filter(fazenda => fazenda.ativo)
        .map(fazenda => 
          (<Option value={fazenda.id} key={fazenda.id}>{fazenda.numero+' - '+fazenda.nome}</Option>)
        )
        : '';
      
      let selectedFazenda = getFieldValue('fazenda');
      selectedFazenda = selectedFazenda ?
        this.props.fazendas
        .filter(fazenda => fazenda.id === selectedFazenda.id)[0]
        : null;

      if (selectedFazenda) {
        optionsSecoes = selectedFazenda ?
          selectedFazenda.secoes
          .filter(secao => secao.ativo )
          .map(secao => 
            (<Option value={secao.id} key={secao.id}>{secao.numero}</Option>)
          )
          : [];
        
        let selectedSecao = getFieldValue('secao.id');
        if (selectedSecao) {
          optionsTalhoes = selectedSecao ?
            selectedFazenda.secoes
            .filter(secao => secao.id === selectedSecao)[0]
            .talhoes
            .filter(talhao => talhao.ativo)
            .map(talhao => 
              (<Option value={talhao.id} key={talhao.id}>{talhao.numero}</Option>)
            )
            : [];
        }
      }
    } catch (exception) {
      console.log(exception);
    }

    return (
      <LayoutContent>
        <PageHeader>Mapas &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>

        <Box title="Cadastro">
          <ContentHolder>
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <Col md={12} sm={12} xs={24}>
                  <b>Fazenda</b><br/>
                  <FormItem>
                    {getFieldDecorator('fazenda.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsFazendas}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Seção</b><br/>
                  <FormItem>
                    {getFieldDecorator('secao.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsSecoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Talhão</b><br/>
                  <FormItem>
                    {getFieldDecorator('talhoes', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      mode="multiple"
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsTalhoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Mapa em PDF</b><br/>
                  <Upload
                    action={apiUrl+'mapa-talhao'}
                    beforeUpload={(file) => {
                      const isPDF = file.type === 'application/pdf';
                      if (!isPDF) {
                        message.error('O arquivo deve ser um PDF!');
                        return false;
                      }
                      this.setState({ file });
                      return true;
                    }}
                    onRemove={() => {
                      this.setState({ file: null });
                      return {};
                    }}
                  >
                    <Button disabled={this.state.file}>
                      <Icon type="upload" /> Selecione um arquivo PDF
                    </Button>
                  </Upload>
                </Col>
              </Row>
              <GroupButtonForm
                disableSubmitButton={this.props.loading || !this.state.file}
                submitButtonText="Salvar"
                disableCancelButton={this.props.loading}
                onClickCancel={() => this.props.history.push('/dashboard/escala/')}
                cancelButtonText="Cancelar"
                disableClearButton={this.props.loading}
                onClickClear={() => this.props.form.resetFields()}
                clearButtonText="Limpar"
              />
            </Form>
          </ContentHolder>
        </Box>
      </LayoutContent>
    )
  }

}

const mapStateToProps = state => ({
  fazendas: state.Fazenda.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchAllFazenda: actionsFazenda.fetchAll,
  }, dispatch
);
MapaNew = Form.create()(MapaNew);
export default connect(mapStateToProps, mapDispatchToProps)(MapaNew);