import React, { Component } from 'react';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from "react-router-dom";
import { Table, Alert, Spin } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import actions from '../../redux/aplicacaoInsumo/actions';

class AplicacaoInsumo extends Component {

  componentDidMount() {
    this.props.fetchAll();
  }

  componentWillUnmount() {
    this.props.clearResposta();
  }

  renderAlert() {
    if (this.props.resposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  render() {

    const columns = [
      {
        title: 'Boletim', dataIndex: 'boletim', key: 'boletim',
        render: (text, record) => <Link to={`/dashboard/aplicacao-insumo/apontamento/${record.id}`}>{record.id}</Link>,
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Fazenda', dataIndex: 'fazenda', key: 'fazenda',
        render: (text, record) => record.fazenda.numero + ' - ' +record.fazenda.nome,
        sorter: (a, b) => a.name === b.name ? 0 : a.name > b.name ? 1 : -1,
      },
      {
        title: 'Funcionário (Cadastro)', dataIndex: 'funcionario', key: 'funcionario',
        render: (text, record) => record.funcionario.matricula + ' - ' +record.createdBy,
        sorter: (a, b) => a.createdBy.name === b.createdBy.name ? 0 : a.createdBy.name > b.createdBy.name ? 1 : -1,
      },
      { title: 'Ativo?', dataIndex: 'ativo', key: 'ativo',
        render: (text, record) => record.ativo ? 'Ativo' : 'Inativo',
        sorter: (a, b) => a.ativo > b.ativo,
      },
    ];

    return (
      <LayoutContent>
        <PageHeader>Aplicações de Insumo &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          <Box>
            <Table
              pagination={false}
              columns={columns}
              dataSource={this.props.aplicacaoInsumos}
            />
          </Box>
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.AplicacaoInsumo.resposta,
  tipoResposta: state.AplicacaoInsumo.tipoResposta,
  aplicacaoInsumos: state.AplicacaoInsumo.all,
  loading: state.AplicacaoInsumo.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  { 
    fetchAll: actions.fetchAll,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(AplicacaoInsumo);