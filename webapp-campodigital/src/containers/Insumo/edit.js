import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin, Alert, Form, Input, Switch, Row, Col } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import GroupButtonForm from '../../components/groupButtonForm';
import actions from '../../redux/insumo/actions';
const FormItem = Form.Item;

class InsumoEdit extends Component {

  componentWillMount() {
    this.props.fetchOne(this.props.match.params.id);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.save(values);
      }
    });
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  render() {

    const { getFieldDecorator, getFieldValue } = this.props.form;
    const insumo = this.props.insumo || {};

    const colStyle = {
      marginBottom: '16px'
    };

    if (this.props.tipoResposta === 'success') {
      this.props.history.push('/dashboard/insumo');
    }

    return (
      <LayoutContent>        
        <PageHeader>Insumo &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          <Box title="Edição">
          <ContentHolder>
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <FormItem>
                  {getFieldDecorator('id', { initialValue: insumo.id })(
                  <Input type="hidden" />
                  )}
                </FormItem>
                <Col md={12} sm={12} xs={24} style={colStyle}>
                  <b>Número</b>
                  <FormItem>
                    {getFieldDecorator('numero', {
                      initialValue: insumo.numero,
                      // { type: 'integer', message: 'Matrícula deve ser um número!' },
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24} style={colStyle}>
                  <b>Descrição</b>
                  <FormItem>
                    {getFieldDecorator('descricao', {
                      initialValue: insumo.descricao,
                      rules: [
                        { required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={12} xs={24} style={colStyle}>
                  <b>Status</b>
                  <FormItem>
                    {getFieldDecorator('ativo', { initialValue: insumo.ativo, })(
                    <Switch
                      checkedChildren={'Ativo'}
                      unCheckedChildren={'Inativo'}
                      defaultChecked
                      checked={getFieldValue('ativo')}
                    />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <GroupButtonForm
                disableSubmitButton={this.props.loading}
                submitButtonText="Salvar Alteração"
                disableCancelButton={this.props.loading}
                onClickCancel={() => this.props.history.push('/dashboard/insumo/')}
                cancelButtonText="Cancelar"
                disableClearButton={this.props.loading}
                onClickClear={() => this.props.history.push('/dashboard/insumo/new')}
                clearButtonText="Novo"
              />
            </Form>
          </ContentHolder>
          </Box>
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  insumo: state.Insumo.insumo,
  resposta: state.Insumo.resposta,
  tipoResposta: state.Insumo.tipoResposta,
  loading: state.Insumo.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    fetchOne: actions.fetchOne,
    clearResposta: actions.clearResposta,
  }, dispatch
);
InsumoEdit = Form.create()(InsumoEdit);
export default connect(mapStateToProps, mapDispatchToProps)(InsumoEdit);