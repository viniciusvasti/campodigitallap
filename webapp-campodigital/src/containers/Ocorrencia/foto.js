import React, { Component, } from 'react';
import axios from 'axios';
import settings from '../../settings';
import { getToken } from '../../helpers/utility';

const { apiUrl } = settings;
const config = {
  headers: {
    Authorization : getToken().get('idToken')
  },
  responseType: 'arraybuffer',
};

class Foto extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fotoBase64: null,
    }
  }

  componentDidMount() {
    axios.get(apiUrl+'ocorrencias/foto/'+this.props.fileName, config)
    .then(response => {
      console.log(response);
      this.setState({ fotoBase64: Buffer.from(response.data, 'binary').toString('base64') });
    })
    .catch(error => {
      console.log(error);
    });
  }

  render() {
    console.log(this.state.fotoBase64);
    if (!this.state.fotoBase64) {
      return 'Loading...';
    }
    return (
      <div style={{ flex: 1, alignContent: 'center', alignItems: 'center' }}>
        <img style={{ width: 300, height: 300 }} alt="Foto ocorrência" src={'data:image/jpg;base64,'+this.state.fotoBase64} />
      </div>
    );
  };
}

export default Foto;