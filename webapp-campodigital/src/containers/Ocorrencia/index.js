import React, { Component } from 'react';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from "react-router-dom";
import { Table, Alert, Spin, Button, } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import actions from '../../redux/ocorrencia/actions';

class Ocorrencia extends Component {

  componentDidMount() {
    this.props.fetchAll();
  }

  componentWillUnmount() {
    this.props.clearResposta();
  }

  renderAlert() {
    if (this.props.resposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  render() {

    const columns = [
      {
        title: 'Código', dataIndex: 'id', key: 'id',
        render: (text, record) => <Link to={`/dashboard/ocorrencia/ocorrencia/${record.id}`}>{record.id}</Link>,
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Grupo', dataIndex: 'permissao', key: 'permissao',
        render: (text, record) => record.permissao.modulo,
        sorter: (a, b) => a.name === b.name ? 0 : a.name > b.name ? 1 : -1,
      },
      {
        title: 'Funcionário (Cadastro)', dataIndex: 'funcionario', key: 'funcionario',
        render: (text, record) => record.createdBy.funcionario ?
                                  record.funcionario.matricula
                                  + ' - ' +record.createdBy
                                  : record.createdBy,
        sorter: (a, b) => a.createdBy.name === b.createdBy.name ? 0 : a.createdBy.name > b.createdBy.name ? 1 : -1,
      },
      { title: 'Ativo?', dataIndex: 'ativo', key: 'ativo',
        render: (text, record) => record.ativo ? 'Ativo' : 'Inativo',
        sorter: (a, b) => a.ativo > b.ativo,
      },
    ];

    return (
      <LayoutContent>
        <PageHeader>Ocorrências &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          <Link to={`/dashboard/ocorrencia/new`}>
            <Button type="primary">
              Novo
            </Button>
          </Link>
          <br/><br/>
          <Box>
            <Table
              pagination={false}
              columns={columns}
              dataSource={this.props.ocorrencias}
            />
          </Box>
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.Ocorrencia.resposta,
  tipoResposta: state.Ocorrencia.tipoResposta,
  ocorrencias: state.Ocorrencia.all,
  loading: state.Ocorrencia.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  { 
    fetchAll: actions.fetchAll,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Ocorrencia);