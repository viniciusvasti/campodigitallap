import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin, Alert, Switch, Row, Col, Modal, Icon, } from 'antd';
import GoogleMapReact from 'google-map-react';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actions from '../../redux/ocorrencia/actions';
import Foto from './foto';

const AnyReactComponent = () => <Icon type="pushpin" style={{ color: 'red' }} />;

class OcorrenciaEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalTitle: '',
      modalText: '',
      modalVisible: false,
      modalFotosVisible: false,
      foto: null,
      handleModalOk: () => {}
    }
  }
  
  componentWillMount() {
    this.props.fetchOne(this.props.match.params.id);
  }

  ocorrenciaConcluida = (v) => {
    const ocorrencia = {
      id: this.props.ocorrencia.id,
      ocorrenciaConcluida: v,
    };
    this.props.patch(ocorrencia);
    this.setState({ modalVisible: false });
  }

  getMapOptions = (maps) => {
    return {
        streetViewControl: false,
        scaleControl: true,
        fullscreenControl: false,
        styles: [{
            featureType: "poi.business",
            elementType: "labels",
            stylers: [{
                visibility: "off"
            }]
        }],
        gestureHandling: "greedy",
        disableDoubleClickZoom: true,
        minZoom: 11,
        maxZoom: 18,

        mapTypeControl: true,
        mapTypeId: maps.MapTypeId.SATELLITE,
        mapTypeControlOptions: {
            style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: maps.ControlPosition.BOTTOM_CENTER,
            mapTypeIds: [
                maps.MapTypeId.ROADMAP,
                maps.MapTypeId.SATELLITE,
                maps.MapTypeId.HYBRID
            ]
        },

        zoomControl: true,
        clickableIcons: false
    };
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  renderModal() {
    return(
      <Modal title={this.state.modalTitle}
        visible={this.state.modalVisible}
        onOk={this.state.handleModalOk}
        confirmLoading={this.props.loading}
        onCancel={() => this.setState({ modalVisible: false })}
      >
        <p>{this.state.modalText}</p>
      </Modal>
    );
  }

  renderModalFotos() {
    if (this.state.foto) {
      return(
        <Modal title={this.state.foto.fileName}
          visible={this.state.modalFotosVisible}
          onOk={() => this.setState({ modalFotosVisible: false })}
          onCancel={() => this.setState({ modalFotosVisible: false })}
        >
          <Foto fileName={this.state.foto.fileName} />
        </Modal>
      );
    }
  }

  renderLinksFotos = (fotos) => fotos.map(foto => 
    <div key={foto.id}>
      <Row gutter={16}>
        <Col md={12} sm={12} xs={24}>
          <a onClick={() => this.setState({ modalFotosVisible: true, foto })}>{foto.fileName}</a>
        </Col>
      </Row>
      <hr style={{color: '#FFF'}} />
    </div>
  );

  render() {

    const {
      id,
      talhao,
      permissao,
      texto,
      location,
      nivelCritico,
      concluido,
      createdBy,
      funcionario,
      fotos,
    } = this.props.ocorrencia || {};
    
    const locationJson = location ? JSON.parse(location.replace('\\','')) : undefined;

    return (
      <LayoutContent>        
        <PageHeader>Ocorrências &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          {this.renderModal()}
          {this.renderModalFotos()}
          { this.props.ocorrencia ?
          <div>
            <Row gutter={16}>
              <Col md={12} sm={12} xs={24}>
                <Box title="">
                  <ContentHolder>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Código:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {id}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Funcionário:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {funcionario ?
                          funcionario.matricula + ' - ' + createdBy
                        : createdBy}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Fazenda:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {talhao.fazenda}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Talhão:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {talhao.numero}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Grupo:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {permissao.modulo}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Nível Crítico:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {nivelCritico}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        <b>Texto:</b>
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {texto}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </ContentHolder>
                </Box>
              </Col>
              <Col md={12} sm={12} xs={24}>
                <Box title="">
                  <ContentHolder>
                    <Box>
                      <Row gutter={16}>
                        <Col md={12} sm={12} xs={24}>
                          Concluído:
                        </Col>
                        <Col md={12} sm={12} xs={24}>
                          <Switch
                            checkedChildren={'Sim'}
                            unCheckedChildren={'Não'}
                            checked={concluido}
                            disabled={concluido}
                            onChange={(v) => this.setState({
                              modalTitle: 'Concluído',
                              modalText: 'Confirma a conclusão da Ocorrência?',
                              modalVisible: true,
                              handleModalOk: () => this.ocorrenciaConcluida(true)
                            })}
                          />
                        </Col>
                      </Row>
                      <hr style={{color: '#FFF'}} />
                    </Box>
                  </ContentHolder>
                </Box>
                <Box title="Fotos">
                  <ContentHolder>
                    {this.renderLinksFotos(fotos)}
                  </ContentHolder>
                </Box>
              </Col>
            </Row>
            <br/>
            <div style={{ height: '100vh', width: '100%' }}>
            { locationJson ?
              <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyBBOOG3AU3jQwefdbKKofOoczVfrEbN3wo' }}
                defaultCenter={{
                  lat: locationJson.latitude,
                  lng: locationJson.longitude
                }}
                defaultZoom={16}
                options={this.getMapOptions}
              >
                <AnyReactComponent
                  lat={locationJson.latitude}
                  lng={locationJson.longitude}
                  text="Local da Ocorrência"
                />
              </GoogleMapReact>
            : '' }
            </div>
          </div>
          : ''}
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  ocorrencia: state.Ocorrencia.ocorrencia,
  resposta: state.Ocorrencia.resposta,
  tipoResposta: state.Ocorrencia.tipoResposta,
  loading: state.Ocorrencia.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    patch: actions.patch,
    fetchOne: actions.fetchOne,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(OcorrenciaEdit);