import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Spin,
  Alert,
  Select,
  Form,
  Input,
  Radio,
  Switch,
  Row,
  Col,
  Icon,
  Upload,
  Button,
  message,
} from 'antd';
import GoogleMapReact from 'google-map-react';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import GroupButtonForm from '../../components/groupButtonForm';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actions from '../../redux/ocorrencia/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsPermissao from '../../redux/permissao/actions';

const AnyReactComponent = () => <Icon type="pushpin" style={{ color: 'red' }} />;

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class OcorrenciaNew extends Component {

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      lat: null,
      lng: null,
    }
  }

  componentWillMount() {
    this.props.fetchAllPermissoes();
    this.props.fetchAllFazenda();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (values.mapa) {
          values = {
            ...values,
            location: JSON.stringify({ latitude: this.state.lat, longitude: this.state.lng }),
          }
        }
        
        const formData = new FormData();
        this.state.files.forEach(f => formData.append('files', f));
        formData.append('nivelCritico', values.nivelCritico);
        formData.append('permissao', values.permissao.id);
        formData.append('talhao', values.talhao.id);
        formData.append('texto', values.texto);
        formData.append('location', values.location ? values.location : '');

        console.log('Received values of form: ', values);
        this.props.save(formData);
      }
    });
  }

  renderAlert() {
    if (this.props.tipoResposta && this.props.tipoResposta !== 'success') {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  render() {

    const { getFieldDecorator, getFieldValue } = this.props.form;

    const colStyle = {
      marginBottom: '16px'
    };

    let optionsSecoes = [];
    let optionsTalhoes = [];
    let optionsFazendas = [];
    
    try {
      
      optionsFazendas = this.props.fazendas ?
        this.props.fazendas
        .filter(fazenda => fazenda.ativo)
        .map(fazenda => 
          (<Option value={fazenda.id} key={fazenda.id}>{fazenda.numero+' - '+fazenda.nome}</Option>)
        )
        : '';
      
      let selectedFazenda = getFieldValue('fazenda');
      selectedFazenda = selectedFazenda ?
        this.props.fazendas
        .filter(fazenda => fazenda.id === selectedFazenda.id)[0]
        : null;

      if (selectedFazenda) {
        optionsSecoes = selectedFazenda ?
          selectedFazenda.secoes
          .filter(secao => secao.ativo )
          .map(secao => 
            (<Option value={secao.id} key={secao.id}>{secao.numero}</Option>)
          )
          : [];
        
        let selectedSecao = getFieldValue('secao.id');
        if (selectedSecao) {
          optionsTalhoes = selectedSecao ?
            selectedFazenda.secoes
            .filter(secao => secao.id === selectedSecao)[0]
            .talhoes
            .filter(talhao => talhao.ativo)
            .map(talhao => 
              (<Option value={talhao.id} key={talhao.id}>{talhao.numero}</Option>)
            )
            : [];
        }
      }
    } catch (exception) {

    }

    if (this.props.tipoResposta === 'success') {
      this.props.history.push('/dashboard/ocorrencia');
    }
    
    const optionsPermissoes = this.props.permissoes ?
      this.props.permissoes
      .map(permissao => 
        (<Option value={permissao.id} key={permissao.id}>{permissao.modulo}</Option>)
      )
      : '';

    return (
      <LayoutContent>        
        <PageHeader>Ocorrência &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          <Box title="Cadastro">
          <ContentHolder>
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <Col md={24} sm={24} xs={24} style={colStyle}>
                  <b>Descrição da Ocorrência</b>
                  <FormItem>
                    {getFieldDecorator('texto', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Fazenda</b><br/>
                  <FormItem>
                    {getFieldDecorator('fazenda.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsFazendas}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Seção</b><br/>
                  <FormItem>
                    {getFieldDecorator('secao.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsSecoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Talhão</b><br/>
                  <FormItem>
                    {getFieldDecorator('talhao.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsTalhoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24} style={colStyle}>
                  <b>Grupo de Usuários</b>
                  <FormItem>
                    {getFieldDecorator('permissao.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsPermissoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24} style={colStyle}>
                  <b>Nível Crítico</b>
                  <FormItem>
                    {getFieldDecorator('nivelCritico', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                    })(
                    <RadioGroup>
                      <Radio value={0}>Baixo</Radio>
                      <Radio value={1}>Médio</Radio>
                      <Radio value={2}>Alto</Radio>
                      <Radio value={3}>Muito Alto</Radio>
                    </RadioGroup>
                    )}
                  </FormItem>
                </Col>
                <Col md={6} sm={12} xs={24} style={colStyle}>
                  <b>Localizada no Mapa?</b>
                  <FormItem>
                    {getFieldDecorator('mapa')(
                    <Switch
                      checkedChildren={'Sim'}
                      unCheckedChildren={'Não'} />
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Fotos (.png ou .jpeg)</b><br/>
                  <Upload
                    beforeUpload={(file) => {
                      const isImg = file.type === 'image/jpeg' || file.type === 'image/png';
                      if (!isImg) {
                        message.error('O arquivo deve ser uma imagem!');
                        return true;
                      } else {
                        this.setState(({ files }) => ({
                          files: [...files, file],
                        }));
                        return false;
                      }
                    }}
                    onRemove={() => {
                      // this.setState({ files: null });
                      return {};
                    }}
                  >
                    <Button>
                      <Icon type="upload" /> Anexar Imagem
                    </Button>
                  </Upload>
                </Col>
              </Row>
              { getFieldValue('mapa') ?
                <Box title="Mapa">
                  <ContentHolder>
                    <div style={{ height: '50vh', width: '100%' }}>
                      <GoogleMapReact
                        bootstrapURLKeys={{
                          key: 'AIzaSyBBOOG3AU3jQwefdbKKofOoczVfrEbN3wo', language: 'pt',
                        }}
                        defaultCenter={{
                          lat: -23.533773,
                          lng: -46.625290
                        }}
                        defaultZoom={5}
                        onChildMouseEnter={this.onChildMouseEnter}
                        onChildMouseLeave={this.onChildMouseLeave}
                        onClick={({ lat, lng }) => this.setState({ lat, lng })}
                      >
                        <AnyReactComponent
                          lat={this.state.lat}
                          lng={this.state.lng}
                          text="Local da Ocorrência"
                        />
                      </GoogleMapReact>
                    </div>
                  </ContentHolder>
                </Box>
                : ""
              }
              <br/>
              <GroupButtonForm
                disableSubmitButton={this.props.loading}
                submitButtonText="Salvar"
                disableCancelButton={this.props.loading}
                onClickCancel={() => this.props.history.push('/dashboard/admin-usuario/')}
                cancelButtonText="Cancelar"
                disableClearButton={this.props.loading}
                onClickClear={() => this.props.form.resetFields()}
                clearButtonText="Limpar"
              />
            </Form>
          </ContentHolder>
          </Box>
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.Ocorrencia.resposta,
  tipoResposta: state.Ocorrencia.tipoResposta,
  loading: state.Ocorrencia.loading,
  permissoes: state.Permissao.all,
  fazendas: state.Fazenda.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllPermissoes: actionsPermissao.fetchAll,
    fetchAllFazenda: actionsFazenda.fetchAll,
  }, dispatch
);
OcorrenciaNew = Form.create()(OcorrenciaNew);
export default connect(mapStateToProps, mapDispatchToProps)(OcorrenciaNew);