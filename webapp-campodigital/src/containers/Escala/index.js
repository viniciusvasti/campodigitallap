import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { Spin, Alert, Row, Col, Form, Input, Select, Icon } from 'antd';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import GroupButtonForm from '../../components/groupButtonForm';
import actions from '../../redux/escala/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsEquipamento from '../../redux/equipamento/actions';
import actionsCentroCusto from '../../redux/centroCusto/actions';

const Option = Select.Option;
const FormItem = Form.Item;

class Escala extends Component {

  componentWillMount() {
    if (this.props.match.params.id) {
      this.props.fetchOne(this.props.match.params.id);
    }
    this.props.fetchAllEscala();
    this.props.fetchAllFazenda();
    this.props.fetchAllEquipamento();
    this.props.fetchAllCentroCusto();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.equipamentos = values.equipamentos.map(
          equip => ({ id: equip })
        );
        console.log('Received values of form: ', values);
        this.props.save(values);
      }
    });
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  renderEscalas(escalas, externalRowStyle, internalRowStyle, colStyle) {
    const borderBottomColStyle = {
      padding: 5,
      border: 'solid',
      borderTopWidth: 0,
      borderBottomWidth: 1,
      borderRightWidth: 1,
      borderLeftWidth: 0,
    };
    return escalas.map((escala, index) => (
      <Row
        style={{
          ...externalRowStyle,
          backgroundColor: index % 2 === 0 ? '#d9d9d9' : '#f2f2f2'
        }}
      >
        <Col md={15} sm={24} xs={24} style={colStyle}>
          <Row style={internalRowStyle}>
            <Col md={8} sm={8} xs={8} style={borderBottomColStyle}>
              {escala.nomeFazenda}
            </Col>
            <Col md={2} sm={2} xs={2} style={borderBottomColStyle}>
              F
            </Col>
            <Col md={3} sm={3} xs={3} style={borderBottomColStyle}>
              {escala.numeroFazenda}
            </Col>
            <Col md={2} sm={2} xs={2} style={borderBottomColStyle}>
              S
            </Col>
            <Col md={3} sm={3} xs={3} style={borderBottomColStyle}>
              {escala.secao}
            </Col>
            <Col md={2} sm={2} xs={2} style={borderBottomColStyle}>
              T
            </Col>
            <Col md={3} sm={3} xs={3} style={borderBottomColStyle}>
              {escala.talhao}
            </Col>
          </Row>
          <Row style={internalRowStyle}>
            <Col md={24} sm={24} xs={24} style={{ padding: 5 }}>
              {
                escala.equipamentos.map((equip, index) => {
                  return equip.numero + (index === escala.equipamentos.length-1 ? '' : ', ');
                })
              }
            </Col>
          </Row>
        </Col>
        <Col md={9} sm={24} xs={24}>
          <Row style={internalRowStyle}>
            <Col md={11} sm={11} xs={11} style={borderBottomColStyle}>
              {escala.atividade}
            </Col>
            <Col md={3} sm={3} xs={3} style={borderBottomColStyle}>
              {escala.coletor}
            </Col>
            <Col md={6} sm={6} xs={6} style={borderBottomColStyle}>
              {escala.centroCusto}
            </Col>
            <Col md={4} sm={4} xs={4} style={borderBottomColStyle}>
              {escala.complemento}
            </Col>
          </Row>
          <Row style={internalRowStyle}>
            <Col md={24} sm={24} xs={24} style={{ padding: 5 }}>
              <div
                onClick={() => {
                  window.scroll(0, 0);
                  this.props.history.push('/dashboard/escala/'+escala.id);
                }}
                style={{ cursor: 'pointer', }} >
                <u style={{ color: 'blue' }} >Editar</u>&nbsp;
                <Icon type="edit" style={{ color: 'blue' }} />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    ));
  }

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    console.log(this.props.escala);

    const escala = this.props.escala || {};

    if (this.props.tipoResposta === 'success') {
      this.props.form.resetFields();
      window.location.reload();
    }

    const externalRowStyle = {
      border: 'solid',
      borderWidth: 0,
      borderTopWidth: 1,
      backgroundColor: '#999999',
    };

    const internalRowStyle = {
    };

    const colStyle = {
      padding: 5,
      border: 'solid',
      borderTopWidth: 0,
      borderBottomWidth: 0,
      borderRightWidth: 1,
      borderLeftWidth: 0,
    }

    let optionsSecoes = [];
    let optionsTalhoes = [];
    let optionsEquipamentos = [];
    let optionsFazendas = [];
    let optionsCentroCusto = [];
    
    try {
      optionsEquipamentos = this.props.equipamentos ?
        this.props.equipamentos
        .filter(equip => equip.ativo)
        .map(equip => 
          (<Option value={equip.id} key={equip.id}>{equip.numero+' - '+equip.descricao}</Option>)
        )
        : '';
      
      optionsFazendas = this.props.fazendas ?
        this.props.fazendas
        .filter(fazenda => fazenda.ativo)
        .map(fazenda => 
          (<Option value={fazenda.id} key={fazenda.id}>{fazenda.numero+' - '+fazenda.nome}</Option>)
        )
        : '';
      
      optionsCentroCusto = this.props.centrosCusto ?
        this.props.centrosCusto
        .filter(cc => cc.ativo)
        .map(cc => 
          (<Option value={cc.id} key={cc.id}>{cc.numero+' - '+cc.descricao}</Option>)
        )
        : '';
      
      let selectedFazenda = getFieldValue('fazenda');
      selectedFazenda = selectedFazenda ?
        this.props.fazendas
        .filter(fazenda => fazenda.id === selectedFazenda.id)[0]
        : null;

      if (selectedFazenda) {
        optionsSecoes = selectedFazenda ?
          selectedFazenda.secoes
          .filter(secao => secao.ativo )
          .map(secao => 
            (<Option value={secao.id} key={secao.id}>{secao.numero}</Option>)
          )
          : [];
        
        let selectedSecao = getFieldValue('secao.id');
        if (selectedSecao) {
          optionsTalhoes = selectedSecao ?
            selectedFazenda.secoes
            .filter(secao => secao.id === selectedSecao)[0]
            .talhoes
            .filter(talhao => talhao.ativo)
            .map(talhao => 
              (<Option value={talhao.id} key={talhao.id}>{talhao.numero}</Option>)
            )
            : [];
        }
      }
    } catch (exception) {

    }

    return (
      <LayoutContent>
        <PageHeader>Escala &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
        {this.renderAlert()}

        <Box title="Cadastro">
          <ContentHolder>
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <Col md={12} sm={12} xs={24}>
                  <b>Fazenda</b><br/>
                  <FormItem>
                    {getFieldDecorator('fazenda.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.idFazenda,
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsFazendas}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Seção</b><br/>
                  <FormItem>
                    {getFieldDecorator('secao.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.idSecao,
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsSecoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Talhão</b><br/>
                  <FormItem>
                    {getFieldDecorator('talhao.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.idTalhao,
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsTalhoes}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Centro de Custo</b><br/>
                  <FormItem>
                    {getFieldDecorator('centroCusto.id', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.idCentroCusto,
                    })(
                    <Select
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsCentroCusto}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Equipamentos</b><br/>
                  <FormItem>
                    {getFieldDecorator(
                      'equipamentos',
                      { initialValue: escala && escala.equipamentos ? escala.equipamentos.map(e => e.id) : [] }
                    )(
                    <Select
                      mode="multiple"
                      showSearch
                      style={{ width: '90%' }}
                      placeholder="Selecione..."
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {optionsEquipamentos}
                    </Select>
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Atividade</b>
                  <FormItem>
                    {getFieldDecorator('atividade', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.atividade,
                    })(
                    <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Coletor</b>
                  <FormItem>
                    {getFieldDecorator('coletor', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.coletor,
                    })(
                    <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={12} sm={12} xs={24}>
                  <b>Complemento</b>
                  <FormItem>
                    {getFieldDecorator('complemento', {
                      rules: [{ required: true, message: 'Preenchimento obrigatório!' }],
                      initialValue: escala.complemento,
                    })(
                    <Input />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <GroupButtonForm
                disableSubmitButton={this.props.loading}
                submitButtonText="Salvar"
                disableCancelButton={this.props.loading}
                onClickCancel={() => this.props.history.push('/dashboard/escala/')}
                cancelButtonText="Cancelar"
                disableClearButton={this.props.loading}
                onClickClear={() => this.props.form.resetFields()}
                clearButtonText="Limpar"
              />
            </Form>
          </ContentHolder>
        </Box>

        <Box title="Escalas">
          <ContentHolder style={{ border: 'solid', borderWidth: 1, borderTopWidth: 0 }}>
            <Row style={externalRowStyle} type='flex'>
              <Col md={20} sm={24} xs={24} style={colStyle}>
                <b>ESCALA DE TROCA</b>
              </Col>
              <Col md={4} sm={24} xs={24} style={{ padding: 5 }}>
                <span>DATA:<br/></span>
                {moment(new Date()).format('DD/MM/YYYY')}
              </Col>
            </Row>
            <Row style={externalRowStyle}>
              <Col md={24} sm={24} xs={24} style={{ padding: 5 }}>
                <b>SETOR</b>
              </Col>
            </Row>
            {
              this.props.escalas ?
                this.renderEscalas(
                  this.props.escalas, externalRowStyle, internalRowStyle, colStyle
                )
              : ''
            }
          </ContentHolder>
        </Box>
      </LayoutContent>
    )
  }
};

const mapStateToProps = state => ({
  resposta: state.Escala.resposta,
  tipoResposta: state.Escala.tipoResposta,
  loading: state.Escala.loading,
  escala: state.Escala.escala,
  escalas: state.Escala.all,
  fazendas: state.Fazenda.all,
  equipamentos: state.Equipamento.all,
  centrosCusto: state.CentroCusto.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchOne: actions.fetchOne,
    fetchAllEscala: actions.fetchAll,
    fetchAllFazenda: actionsFazenda.fetchAll,
    fetchAllEquipamento: actionsEquipamento.fetchAllToEscala,
    fetchAllCentroCusto: actionsCentroCusto.fetchAll,
  }, dispatch
);

Escala = Form.create()(Escala);
export default connect(mapStateToProps, mapDispatchToProps)(Escala);
