import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin, Alert, Switch, Row, Col, Modal } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actions from '../../redux/apontamentoTerceirosMecanizado/actions';

class ApontamentoTerceirosMecanizadoEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalTitle: '',
      modalText: '',
      modalVisible: false,
      handleModalOk: () => {}
    }
  }
  
  componentWillMount() {
    this.props.fetchOne(this.props.match.params.id);
  }

  digitadoPims = (v) => {
    const apontamento = {
      id: this.props.apontamentoTerceirosMecanizado.id,
      digitadoPims: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  fichaEntregue = (v) => {
    const apontamento = {
      id: this.props.apontamentoTerceirosMecanizado.id,
      fichaEntregue: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  renderModal() {
    return(
      <Modal title={this.state.modalTitle}
          visible={this.state.modalVisible}
          onOk={this.state.handleModalOk}
          confirmLoading={this.props.loading}
          onCancel={() => this.setState({ modalVisible: false })}
        >
          <p>{this.state.modalText}</p>
        </Modal>
    );
  }

  render() {
    // TODO cadastrar novamente com atenção ao valor digitando em hOperador
    const {
      createdBy,
      funcionario,
      id,
      dataApontamento,
      digitadoPims,
      fichaEntregue,
      usuarioDigitouPims,
      fazenda,
      talhao,
      sequencia,
      quantidade,
      observacao,
      centroCusto,
      operacao,
      equipamento,
    } = this.props.apontamentoTerceirosMecanizado || {};

    // armengue para pegar a seção
    let secao = '';
    if (fazenda) {
      const secoes = fazenda.secoes;
      for (let i=0; i < secoes.length; i++) {
        for (let j=0; j < secoes[i].talhoes.length; j++) {
          if (secoes[i].talhoes[j].id === talhao.id) secao = secoes[i].numero;
        }
      }
    }

    return (
      <LayoutContent>        
        <PageHeader>Apontamento de Terceiros Mecanizado &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          {this.renderModal()}
          { this.props.apontamentoTerceirosMecanizado ?
          <Row gutter={16}>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Funcionário:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {funcionario.matricula} - {createdBy}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Boletim:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {id}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Fazenda:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {fazenda.numero} - {fazenda.nome}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Seção:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {secao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Talhão:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {talhao.numero}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Data:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {dataApontamento}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Sequência:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {sequencia}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Operação:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {operacao.numero + ' - ' + operacao.descricao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Centro de Custo:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {centroCusto.numero + ' - ' + centroCusto.descricao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Equipamento:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {equipamento.numero + ' - ' + equipamento.descricao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Quantidade:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {quantidade}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Observação:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {observacao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                </ContentHolder>
              </Box>
            </Col>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Pims foi digitado:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={digitadoPims}
                          onChange={(v) => this.setState({
                            modalTitle: 'Pims',
                            modalText: digitadoPims ?
                            'Marcar Pims como "não digitado"?'
                            : 'Confirma que o Pims já foi digitado?',
                            modalVisible: true,
                            handleModalOk: () => this.digitadoPims(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Funcionário que digitou o Pims:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {usuarioDigitouPims}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Ficha foi entregue:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={fichaEntregue}
                          onChange={(v) => this.setState({
                            modalTitle: 'Ficha',
                            modalText: fichaEntregue ?
                              'Deseja marcar ficha como "não entregue"?'
                              : 'Confirma que a Ficha já foi entregue?',
                            modalVisible: true,
                            handleModalOk: () => this.fichaEntregue(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                </ContentHolder>
              </Box>
            </Col>
          </Row>
          : ''}
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  apontamentoTerceirosMecanizado: state.ApontamentoTerceirosMecanizado.apontamentoTerceirosMecanizado,
  resposta: state.ApontamentoTerceirosMecanizado.resposta,
  tipoResposta: state.ApontamentoTerceirosMecanizado.tipoResposta,
  loading: state.ApontamentoTerceirosMecanizado.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    patch: actions.patch,
    fetchOne: actions.fetchOne,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(ApontamentoTerceirosMecanizadoEdit);