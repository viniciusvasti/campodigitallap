import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin, Alert, Switch, Row, Col, Modal } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actions from '../../redux/apontamentoCombustivel/actions';

class ApontamentoCombustivelEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalTitle: '',
      modalText: '',
      modalVisible: false,
      handleModalOk: () => {}
    }
  }
  
  componentWillMount() {
    this.props.fetchOne(this.props.match.params.id);
  }

  digitadoPims = (v) => {
    const apontamento = {
      id: this.props.apontamentoCombustivel.id,
      digitadoPims: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  fichaEntregue = (v) => {
    const apontamento = {
      id: this.props.apontamentoCombustivel.id,
      fichaEntregue: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  renderModal() {
    return(
      <Modal title={this.state.modalTitle}
          visible={this.state.modalVisible}
          onOk={this.state.handleModalOk}
          confirmLoading={this.props.loading}
          onCancel={() => this.setState({ modalVisible: false })}
        >
          <p>{this.state.modalText}</p>
        </Modal>
    );
  }

  render() {
    // TODO cadastrar novamente com atenção ao valor digitando em hOperador
    const {
      createdBy,
      funcionario,
      id,
      dataApontamento,
      horaApontamento,
      digitadoPims,
      fichaEntregue,
      usuarioDigitouPims,
      fazenda,
      pontoDeLubrificacao,
      equipamento,
      operador,
      combustivel,
      quantidade,
      odometro,
    } = this.props.apontamentoCombustivel || {};

    return (
      <LayoutContent>        
        <PageHeader>Apontamento de Combustível &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          {this.renderModal()}
          { this.props.apontamentoCombustivel ?
          <Row gutter={16}>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Funcionário:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {funcionario.matricula} - {createdBy}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Boletim:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {id}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Fazenda:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {fazenda.numero} - {fazenda.nome}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Data/Hora:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {dataApontamento + ' ' + horaApontamento}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Ponto de Lubrificação:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {pontoDeLubrificacao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Equipamento:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {equipamento.numero + ' - ' + equipamento.descricao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Operador:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {operador}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Combustível:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {combustivel}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Quantidade:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {quantidade}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Horim/Odômetro:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {odometro}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                </ContentHolder>
              </Box>
            </Col>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Pims foi digitado:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={digitadoPims}
                          onChange={(v) => this.setState({
                            modalTitle: 'Pims',
                            modalText: digitadoPims ?
                            'Marcar Pims como "não digitado"?'
                            : 'Confirma que o Pims já foi digitado?',
                            modalVisible: true,
                            handleModalOk: () => this.digitadoPims(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Funcionário que digitou o Pims:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {usuarioDigitouPims}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Ficha foi entregue:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={fichaEntregue}
                          onChange={(v) => this.setState({
                            modalTitle: 'Ficha',
                            modalText: fichaEntregue ?
                              'Deseja marcar ficha como "não entregue"?'
                              : 'Confirma que a Ficha já foi entregue?',
                            modalVisible: true,
                            handleModalOk: () => this.fichaEntregue(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                </ContentHolder>
              </Box>
            </Col>
          </Row>
          : ''}
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  apontamentoCombustivel: state.ApontamentoCombustivel.apontamentoCombustivel,
  resposta: state.ApontamentoCombustivel.resposta,
  tipoResposta: state.ApontamentoCombustivel.tipoResposta,
  loading: state.ApontamentoCombustivel.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    patch: actions.patch,
    fetchOne: actions.fetchOne,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(ApontamentoCombustivelEdit);