import React, { Component } from 'react';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from "react-router-dom";
import { Table, Alert, Spin } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import actions from '../../redux/apontamentoChuva/actions';

class ApontamentoChuva extends Component {

  componentDidMount() {
    this.props.fetchAll();
  }

  componentWillUnmount() {
    this.props.clearResposta();
  }

  renderAlert() {
    if (this.props.resposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  render() {

    const columns = [
      {
        title: 'Boletim', dataIndex: 'boletim', key: 'boletim',
        render: (text, record) => <Link to={`/dashboard/apontamento-chuva/apontamento/${record.id}`}>{record.id}</Link>,
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Filial', dataIndex: 'filial', key: 'filial',
        render: (text, record) => record.filial.nomeFantasia ? record.filial.nomeFantasia : record.filial.razaoSocial,
        sorter: (a, b) => a.name === b.name ? 0 : a.name > b.name ? 1 : -1,
      },
      {
        title: 'Funcionário (Cadastro)', dataIndex: 'funcionario', key: 'funcionario',
        render: (text, record) => record.funcionario.matricula + ' - ' +record.createdBy,
        sorter: (a, b) => a.createdBy.name === b.createdBy.name ? 0 : a.createdBy.name > b.createdBy.name ? 1 : -1,
      },
      { title: 'Ativo?', dataIndex: 'ativo', key: 'ativo',
        render: (text, record) => record.ativo ? 'Ativo' : 'Inativo',
        sorter: (a, b) => a.ativo > b.ativo,
      },
    ];

    return (
      <LayoutContent>
        <PageHeader>Apontamentos de Chuva &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          <Box>
            <Table
              pagination={false}
              columns={columns}
              dataSource={this.props.apontamentoChuvas}
            />
          </Box>
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.ApontamentoChuva.resposta,
  tipoResposta: state.ApontamentoChuva.tipoResposta,
  apontamentoChuvas: state.ApontamentoChuva.all,
  loading: state.ApontamentoChuva.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  { 
    fetchAll: actions.fetchAll,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(ApontamentoChuva);