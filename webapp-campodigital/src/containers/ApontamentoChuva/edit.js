import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin, Alert, Switch, Row, Col, Modal } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actions from '../../redux/apontamentoChuva/actions';

class AdminApontamentoChuvaEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalTitle: '',
      modalText: '',
      modalVisible: false,
      handleModalOk: () => {}
    }
  }
  
  componentWillMount() {
    this.props.fetchOne(this.props.match.params.id);
  }

  digitadoPims = (v) => {
    const apontamento = {
      id: this.props.apontamentoChuva.id,
      digitadoPims: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  digitadoSol = (v) => {
    const apontamento = {
      id: this.props.apontamentoChuva.id,
      digitadoSOL: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  fichaEntregue = (v) => {
    const apontamento = {
      id: this.props.apontamentoChuva.id,
      fichaEntregue: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  renderModal() {
    return(
      <Modal title={this.state.modalTitle}
          visible={this.state.modalVisible}
          onOk={this.state.handleModalOk}
          confirmLoading={this.props.loading}
          onCancel={() => this.setState({ modalVisible: false })}
        >
          <p>{this.state.modalText}</p>
        </Modal>
    );
  }

  render() {

    const {
      createdBy,
      funcionario,
      id,
      dataApontamento,
      hora,
      digitadoPims,
      digitadoSOL,
      fichaEntregue,
      usuarioDigitouPims,
      usuarioDigitouSol,
      elemento,
      leitura,
      posto,
    } = this.props.apontamentoChuva || {};

    return (
      <LayoutContent>        
        <PageHeader>Apontamento de Chuva &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          {this.renderModal()}
          { this.props.apontamentoChuva ?
          <Row gutter={16}>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Funcionário:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {funcionario.matricula} - {createdBy}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Boletim:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {id}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Data:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {dataApontamento}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Hora:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {hora}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Posto:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {posto}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Elemento:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {elemento}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Leitura:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {leitura}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                </ContentHolder>
              </Box>
            </Col>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Pims foi digitado:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={digitadoPims}
                          onChange={(v) => this.setState({
                            modalTitle: 'Pims',
                            modalText: digitadoPims ?
                            'Marcar Pims como "não digitado"?'
                            : 'Confirma que o Pims já foi digitado?',
                            modalVisible: true,
                            handleModalOk: () => this.digitadoPims(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Funcionário que digitou o Pims:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {usuarioDigitouPims}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        SOL foi digitado:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={digitadoSOL}
                          onChange={(v) => this.setState({
                            modalTitle: 'SOL',
                            modalText: digitadoSOL ?
                              'Marcar SOL como "não digitado"?'
                              : 'Confirma que o SOL já foi digitado?',
                            modalVisible: true,
                            handleModalOk: () => this.digitadoSol(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Funcionário que digitou o SOL:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {usuarioDigitouSol}
                        <br/>
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Ficha foi entregue:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={fichaEntregue}
                          onChange={(v) => this.setState({
                            modalTitle: 'Ficha',
                            modalText: fichaEntregue ?
                              'Deseja marcar ficha como "não entregue"?'
                              : 'Confirma que a Ficha já foi entregue?',
                            modalVisible: true,
                            handleModalOk: () => this.fichaEntregue(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                </ContentHolder>
              </Box>
            </Col>
          </Row>
          : ''}
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  apontamentoChuva: state.ApontamentoChuva.apontamentoChuva,
  resposta: state.ApontamentoChuva.resposta,
  tipoResposta: state.ApontamentoChuva.tipoResposta,
  loading: state.ApontamentoChuva.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    patch: actions.patch,
    fetchOne: actions.fetchOne,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(AdminApontamentoChuvaEdit);