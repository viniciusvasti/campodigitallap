import React, { Component, } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin, Alert, Switch, Row, Col, Modal } from 'antd';
import Box from '../../components/utility/box';
import PageHeader from '../../components/utility/pageHeader';
import LayoutContent from '../../components/utility/layoutContent';
import ContentHolder from '../../components/utility/contentHolder';
import actions from '../../redux/apontamentoPlantio/actions';

class ApontamentoPlantioEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalTitle: '',
      modalText: '',
      modalVisible: false,
      handleModalOk: () => {}
    }
  }
  
  componentWillMount() {
    this.props.fetchOne(this.props.match.params.id);
  }

  digitadoPims = (v) => {
    const apontamento = {
      id: this.props.apontamentoPlantio.id,
      digitadoPims: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  digitadoSol = (v) => {
    const apontamento = {
      id: this.props.apontamentoPlantio.id,
      digitadoSOL: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  fichaEntregue = (v) => {
    const apontamento = {
      id: this.props.apontamentoPlantio.id,
      fichaEntregue: v,
    };
    this.props.patch(apontamento);
    this.setState({ modalVisible: false });
  }

  renderAlert() {
    if (this.props.tipoResposta) {
      return (
        <div>
          <Alert
            message={this.props.resposta}
            type={this.props.tipoResposta}
            closable
            afterClose={this.props.clearResposta}
          />
          <br/>
        </div>
      );
    } 
  }

  renderModal() {
    return(
      <Modal title={this.state.modalTitle}
          visible={this.state.modalVisible}
          onOk={this.state.handleModalOk}
          confirmLoading={this.props.loading}
          onCancel={() => this.setState({ modalVisible: false })}
        >
          <p>{this.state.modalText}</p>
        </Modal>
    );
  }

  render() {
    // TODO cadastrar novamente com atenção ao valor digitando em hOperador
    const {
      createdBy,
      funcionario,
      id,
      dataApontamento,
      digitadoPims,
      digitadoSOL,
      fichaEntregue,
      usuarioDigitouSol,
      usuarioDigitouPims,
      fazenda,
      talhao,
      variedade,
      sistemaDePlantio,
      espacamento,
      cicloDeDesenvolvimento,
      area,
      replantacao,
      localConcluido,
      procedenciaMuda,
      secaoMuda,
      talhaoMuda,
      nrCargas,
      pesoMedioKg,
      nrEquipes,
      nrCaminhoes,
      distanciaMuda,
    } = this.props.apontamentoPlantio || {};

    // armengue para pegar a seção
    let secao = '';
    if (fazenda) {
      const secoes = fazenda.secoes;
      for (let i=0; i < secoes.length; i++) {
        for (let j=0; j < secoes[i].talhoes.length; j++) {
          if (secoes[i].talhoes[j].id === talhao.id) secao = secoes[i].numero;
        }
      }
    }

    return (
      <LayoutContent>        
        <PageHeader>Apontamento de Plantio &nbsp;&nbsp; {this.props.loading ? <Spin/> : ''}</PageHeader>
          {this.renderAlert()}
          {this.renderModal()}
          { this.props.apontamentoPlantio ?
          <Row gutter={16}>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Funcionário:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {funcionario.matricula} - {createdBy}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Boletim:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {id}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Fazenda:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {fazenda.numero} - {fazenda.nome}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Seção:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {secao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Talhão:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {talhao.numero}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Data:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {dataApontamento}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Sistema de Plantio:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {sistemaDePlantio.numero + ' - ' + sistemaDePlantio.descricao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Variedade:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {variedade}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Espaçamento:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {espacamento}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Ciclo de Desenvolvimento:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {cicloDeDesenvolvimento.numero + ' - ' + cicloDeDesenvolvimento.descricao}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Área:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {area}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Plantação/Replantação:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {replantacao ? 'Replantação' : 'Plantação'}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Local Concluído:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {localConcluido ? 'Sim' : 'Não'}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Procedência (Muda):</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {procedenciaMuda}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Seção (Muda):</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {secaoMuda}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Talhão (Muda):</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {talhaoMuda}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Nº de Cargas:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {nrCargas}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Peso Médio (Kg):</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {pesoMedioKg}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Nº de Equipes:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {nrEquipes}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Nº de Caminhões:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {nrCaminhoes}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                  <Row gutter={16}>
                    <Col md={12} sm={12} xs={24}>
                      <b>Distância da Muda:</b>
                    </Col>
                    <Col md={12} sm={12} xs={24}>
                      {distanciaMuda}
                    </Col>
                  </Row>
                  <hr style={{color: '#FFF'}} />
                </ContentHolder>
              </Box>
            </Col>
            <Col md={12} sm={12} xs={24}>
              <Box title="">
                <ContentHolder>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Pims foi digitado:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={digitadoPims}
                          onChange={(v) => this.setState({
                            modalTitle: 'Pims',
                            modalText: digitadoPims ?
                            'Marcar Pims como "não digitado"?'
                            : 'Confirma que o Pims já foi digitado?',
                            modalVisible: true,
                            handleModalOk: () => this.digitadoPims(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Funcionário que digitou o Pims:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {usuarioDigitouPims}
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        SOL foi digitado:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={digitadoSOL}
                          onChange={(v) => this.setState({
                            modalTitle: 'SOL',
                            modalText: digitadoSOL ?
                              'Marcar SOL como "não digitado"?'
                              : 'Confirma que o SOL já foi digitado?',
                            modalVisible: true,
                            handleModalOk: () => this.digitadoSol(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Funcionário que digitou o SOL:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        {usuarioDigitouSol}
                        <br/>
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                  <Box>
                    <Row gutter={16}>
                      <Col md={12} sm={12} xs={24}>
                        Ficha foi entregue:
                      </Col>
                      <Col md={12} sm={12} xs={24}>
                        <Switch
                          checkedChildren={'Sim'}
                          unCheckedChildren={'Não'}
                          checked={fichaEntregue}
                          onChange={(v) => this.setState({
                            modalTitle: 'Ficha',
                            modalText: fichaEntregue ?
                              'Deseja marcar ficha como "não entregue"?'
                              : 'Confirma que a Ficha já foi entregue?',
                            modalVisible: true,
                            handleModalOk: () => this.fichaEntregue(v)
                          })}
                        />
                      </Col>
                    </Row>
                    <hr style={{color: '#FFF'}} />
                  </Box>
                </ContentHolder>
              </Box>
            </Col>
          </Row>
          : ''}
      </LayoutContent>
    );
  }
}

const mapStateToProps = state => ({
  apontamentoPlantio: state.ApontamentoPlantio.apontamentoPlantio,
  resposta: state.ApontamentoPlantio.resposta,
  tipoResposta: state.ApontamentoPlantio.tipoResposta,
  loading: state.ApontamentoPlantio.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    patch: actions.patch,
    fetchOne: actions.fetchOne,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(ApontamentoPlantioEdit);