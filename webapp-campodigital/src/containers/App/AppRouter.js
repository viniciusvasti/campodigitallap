import React, { Component } from "react";
import { Route } from "react-router-dom";
import asyncComponent from "../../helpers/AsyncFunc";

const routes = [
  {
    path: "",
    component: asyncComponent(() => import("../dashboard"))
  },
  {
    path: "admin-empresa",
    component: asyncComponent(() => import("../AdminEmpresa"))
  },
  {
    path: "admin-empresa/new",
    component: asyncComponent(() => import("../AdminEmpresa/new"))
  },
  {
    path: "admin-empresa/empresa/:id",
    component: asyncComponent(() => import("../AdminEmpresa/edit"))
  },
  {
    path: "admin-filial",
    component: asyncComponent(() => import("../AdminFilial"))
  },
  {
    path: "admin-filial/new",
    component: asyncComponent(() => import("../AdminFilial/new"))
  },
  {
    path: "admin-filial/filial/:id",
    component: asyncComponent(() => import("../AdminFilial/edit"))
  },
  {
    path: "admin-cargo",
    component: asyncComponent(() => import("../AdminCargo"))
  },
  {
    path: "admin-cargo/new",
    component: asyncComponent(() => import("../AdminCargo/new"))
  },
  {
    path: "admin-cargo/cargo/:id",
    component: asyncComponent(() => import("../AdminCargo/edit"))
  },
  {
    path: "admin-usuario",
    component: asyncComponent(() => import("../AdminUsuario"))
  },
  {
    path: "admin-usuario/new",
    component: asyncComponent(() => import("../AdminUsuario/new"))
  },
  {
    path: "admin-usuario/usuario/:id",
    component: asyncComponent(() => import("../AdminUsuario/edit"))
  },
  {
    path: "admin-fazenda",
    component: asyncComponent(() => import("../AdminFazenda"))
  },
  {
    path: "admin-fazenda/new",
    component: asyncComponent(() => import("../AdminFazenda/new"))
  },
  {
    path: "admin-fazenda/fazenda/:id",
    component: asyncComponent(() => import("../AdminFazenda/edit"))
  },
  {
    path: "insumo",
    component: asyncComponent(() => import("../Insumo"))
  },
  {
    path: "escala",
    component: asyncComponent(() => import("../Escala"))
  },
  {
    path: "escala/:id",
    component: asyncComponent(() => import("../Escala"))
  },
  {
    path: "insumo/new",
    component: asyncComponent(() => import("../AdminFazenda/new"))
  },
  {
    path: "insumo/edit/:id",
    component: asyncComponent(() => import("../Insumo/edit"))
  },
  {
    path: "aplicacao-insumo",
    component: asyncComponent(() => import("../AplicacaoInsumo"))
  },
  {
    path: "aplicacao-insumo/apontamento/:id",
    component: asyncComponent(() => import("../AplicacaoInsumo/edit"))
  },
  {
    path: "apontamento-chuva",
    component: asyncComponent(() => import("../ApontamentoChuva"))
  },
  {
    path: "apontamento-chuva/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoChuva/edit"))
  },
  {
    path: "apontamento-area",
    component: asyncComponent(() => import("../ApontamentoArea"))
  },
  {
    path: "apontamento-area/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoArea/edit"))
  },
  {
    path: "apontamento-area-preparo",
    component: asyncComponent(() => import("../ApontamentoAreaPreparo"))
  },
  {
    path: "apontamento-area-preparo/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoAreaPreparo/edit"))
  },
  {
    path: "apontamento-atividade-mecanizada",
    component: asyncComponent(() => import("../ApontamentoAtividadeMecanizada"))
  },
  {
    path: "apontamento-atividade-mecanizada/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoAtividadeMecanizada/edit"))
  },
  {
    path: "apontamento-atividades-manuais",
    component: asyncComponent(() => import("../ApontamentoAtividadesManuais"))
  },
  {
    path: "apontamento-atividades-manuais/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoAtividadesManuais/edit"))
  },
  {
    path: "apontamento-combustivel",
    component: asyncComponent(() => import("../ApontamentoCombustivel"))
  },
  {
    path: "apontamento-combustivel/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoCombustivel/edit"))
  },
  {
    path: "apontamento-lubrificante",
    component: asyncComponent(() => import("../ApontamentoLubrificante"))
  },
  {
    path: "apontamento-lubrificante/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoLubrificante/edit"))
  },
  {
    path: "apontamento-moagem-diaria",
    component: asyncComponent(() => import("../ApontamentoMoagemDiaria"))
  },
  {
    path: "apontamento-moagem-diaria/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoMoagemDiaria/edit"))
  },
  {
    path: "apontamento-plantio",
    component: asyncComponent(() => import("../ApontamentoPlantio"))
  },
  {
    path: "apontamento-plantio/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoPlantio/edit"))
  },
  {
    path: "apontamento-terceiros-mecanizado",
    component: asyncComponent(() => import("../ApontamentoTerceirosMecanizado"))
  },
  {
    path: "apontamento-terceiros-mecanizado/apontamento/:id",
    component: asyncComponent(() => import("../ApontamentoTerceirosMecanizado/edit"))
  },
  {
    path: "fechamento-corte",
    component: asyncComponent(() => import("../FechamentoCorte"))
  },
  {
    path: "fechamento-corte/fechamento/:id",
    component: asyncComponent(() => import("../FechamentoCorte/edit"))
  },
  {
    path: "ordem-corte",
    component: asyncComponent(() => import("../OrdemCorte"))
  },
  {
    path: "ordem-corte/ordem/:id",
    component: asyncComponent(() => import("../OrdemCorte/edit"))
  },
  {
    path: "ordem-servico",
    component: asyncComponent(() => import("../OrdemServico"))
  },
  {
    path: "ordem-servico/ordem/:id",
    component: asyncComponent(() => import("../OrdemServico/edit"))
  },
  {
    path: "mapas",
    component: asyncComponent(() => import("../MapasFazenda"))
  },
  {
    path: "mapas/new",
    component: asyncComponent(() => import("../MapasFazenda/new"))
  },
  {
    path: "ocorrencia",
    component: asyncComponent(() => import("../Ocorrencia"))
  },
  {
    path: "ocorrencia/new",
    component: asyncComponent(() => import("../Ocorrencia/new"))
  },
  {
    path: "ocorrencia/ocorrencia/:id",
    component: asyncComponent(() => import("../Ocorrencia/edit"))
  },
];

class AppRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default AppRouter;
