export const endpoint = 'apontamentos-chuva/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_CHUVA',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_CHUVA',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_CHUVA',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_CHUVA',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_CHUVA',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_CHUVA',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_CHUVA',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_CHUVA',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_APONTAMENTO_CHUVA',
  PATCH_SUCCESS: 'PATCH_SUCCESS_APONTAMENTO_CHUVA',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;