import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import empresasSagas from './empresa/saga';
import filialSagas from './filial/saga';
import cargoSagas from './cargo/saga';
import usuarioSagas from './usuario/saga';
import fazendaSagas from './fazenda/saga';
import insumoChuvaSagas from './insumo/saga';
import apontamentoChuvaSagas from './apontamentoChuva/saga';
import apontamentoAreaSagas from './apontamentoArea/saga';
import apontamentoAreaPreparoSagas from './apontamentoAreaPreparo/saga';
import apontamentoAtividadeMecanizadaSagas from './apontamentoAtividadeMecanizada/saga';
import apontamentoAtividadesManuaisSagas from './apontamentoAtividadesManuais/saga';
import apontamentoCombustivelSagas from './apontamentoCombustivel/saga';
import apontamentoLubrificanteSagas from './apontamentoLubrificante/saga';
import apontamentoMoagemDiariaSagas from './apontamentoMoagemDiaria/saga';
import apontamentoPlantioSagas from './apontamentoPlantio/saga';
import apontamentoTerceirosMecanizadoSagas from './apontamentoTerceirosMecanizado/saga';
import aplicacaoInsumoSagas from './aplicacaoInsumo/saga';
import fechamentoCorteSagas from './fechamentoCorte/saga';
import ordemCorteSagas from './ordemCorte/saga';
import ordemServicoSagas from './ordemServico/saga';
import equipamentoSagas from './equipamento/saga';
import centroCustoSagas from './centroCusto/saga';
import escalaSagas from './escala/saga';
import permissaoSagas from './permissao/saga';
import mapaTalhaoSagas from './mapaTalhao/saga';
import ocorrenciaSagas from './ocorrencia/saga';

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    empresasSagas(),
    filialSagas(),
    cargoSagas(),
    usuarioSagas(),
    fazendaSagas(),
    apontamentoChuvaSagas(),
    apontamentoAreaSagas(),
    apontamentoAreaPreparoSagas(),
    apontamentoAtividadeMecanizadaSagas(),
    apontamentoAtividadesManuaisSagas(),
    apontamentoCombustivelSagas(),
    apontamentoLubrificanteSagas(),
    apontamentoMoagemDiariaSagas(),
    apontamentoPlantioSagas(),
    apontamentoTerceirosMecanizadoSagas(),
    fechamentoCorteSagas(),
    ordemCorteSagas(),
    ordemServicoSagas(),
    insumoChuvaSagas(),
    aplicacaoInsumoSagas(),
    equipamentoSagas(),
    centroCustoSagas(),
    escalaSagas(),
    permissaoSagas(),
    mapaTalhaoSagas(),
    ocorrenciaSagas(),
  ]);
}
