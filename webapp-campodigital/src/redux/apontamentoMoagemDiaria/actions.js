export const endpoint = 'apontamentos-moagem-diaria/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_MOAGEM_DIARIA',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_MOAGEM_DIARIA',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_MOAGEM_DIARIA',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_MOAGEM_DIARIA',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_MOAGEM_DIARIA',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_MOAGEM_DIARIA',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_MOAGEM_DIARIA',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_MOAGEM_DIARIA',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_APONTAMENTO_MOAGEM_DIARIA',
  PATCH_SUCCESS: 'PATCH_SUCCESS_APONTAMENTO_MOAGEM_DIARIA',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;