export const endpoint = 'ordens-corte/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_ORDEM_CORTE',
  SAVE_SUCCESS: 'SAVE_SUCCESS_ORDEM_CORTE',
  FETCH_ALL: 'FETCH_ALL_ORDEM_CORTE',
  FETCHED_ALL: 'FETCHED_ALL_ORDEM_CORTE',
  FETCH_ONE: 'FETCH_ONE_ORDEM_CORTE',
  FETCHED_ONE: 'FETCHED_ONE_ORDEM_CORTE',
  REQUEST_ERROR: 'REQUEST_ERROR_ORDEM_CORTE',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_ORDEM_CORTE',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_ORDEM_CORTE',
  PATCH_SUCCESS: 'PATCH_SUCCESS_ORDEM_CORTE',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;