export const endpoint = 'mapas-talhao/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_MAPA_TALHAO',
  SAVE_SUCCESS: 'SAVE_SUCCESS',
  FETCH_ALL: 'FETCH_ALL_MAPA_TALHAO',
  FETCHED_ALL: 'FETCHED_ALL_MAPA_TALHAO',
  FETCH_ONE: 'FETCH_ONE_MAPA_TALHAO',
  FETCHED_ONE: 'FETCHED_ONE_MAPA_TALHAO',
  REQUEST_ERROR: 'REQUEST_ERROR',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;