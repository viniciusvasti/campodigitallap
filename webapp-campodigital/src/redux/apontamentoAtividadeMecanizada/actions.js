export const endpoint = 'apontamentos-atividade-mecanizada/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_ATIVIDADE_MECANIZADA',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_ATIVIDADE_MECANIZADA',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_ATIVIDADE_MECANIZADA',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_ATIVIDADE_MECANIZADA',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_ATIVIDADE_MECANIZADA',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_ATIVIDADE_MECANIZADA',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_ATIVIDADE_MECANIZADA',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_ATIVIDADE_MECANIZADA',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_APONTAMENTO_ATIVIDADE_MECANIZADA',
  PATCH_SUCCESS: 'PATCH_SUCCESS_APONTAMENTO_ATIVIDADE_MECANIZADA',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;