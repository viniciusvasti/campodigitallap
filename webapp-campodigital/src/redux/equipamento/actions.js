export const endpoint = 'equipamentos/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_EQUIPAMENTO',
  SAVE_SUCCESS: 'SAVE_SUCCESS',
  FETCH_ALL: 'FETCH_ALL_EQUIPAMENTO',
  FETCH_ALL_ESCALA: 'FETCH_ALL_EQUIPAMENTO_ESCALA',
  FETCHED_ALL: 'FETCHED_ALL_EQUIPAMENTO',
  FETCH_ONE: 'FETCH_ONE_EQUIPAMENTO',
  FETCHED_ONE: 'FETCHED_ONE_EQUIPAMENTO',
  REQUEST_ERROR: 'REQUEST_ERROR',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchAllToEscala: () => ({
    type: actions.FETCH_ALL_ESCALA,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;