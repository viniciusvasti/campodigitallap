import Auth from './auth/reducer';
import App from './app/reducer';
import Empresa from './empresa/reducer';
import Filial from './filial/reducer';
import Cargo from './cargo/reducer';
import Usuario from './usuario/reducer';
import Fazenda from './fazenda/reducer';
import Insumo from './insumo/reducer';
import ApontamentoChuva from './apontamentoChuva/reducer';
import ApontamentoArea from './apontamentoArea/reducer';
import ApontamentoAreaPreparo from './apontamentoAreaPreparo/reducer';
import ApontamentoAtividadeMecanizada from './apontamentoAtividadeMecanizada/reducer';
import ApontamentoAtividadesManuais from './apontamentoAtividadesManuais/reducer';
import ApontamentoCombustivel from './apontamentoCombustivel/reducer';
import ApontamentoLubrificante from './apontamentoLubrificante/reducer';
import ApontamentoMoagemDiaria from './apontamentoMoagemDiaria/reducer';
import ApontamentoPlantio from './apontamentoPlantio/reducer';
import ApontamentoTerceirosMecanizado from './apontamentoTerceirosMecanizado/reducer';
import AplicacaoInsumo from './aplicacaoInsumo/reducer';
import FechamentoCorte from './fechamentoCorte/reducer';
import OrdemCorte from './ordemCorte/reducer';
import OrdemServico from './ordemServico/reducer';
import Equipamento from './equipamento/reducer';
import CentroCusto from './centroCusto/reducer';
import Escala from './escala/reducer';
import Permissao from './permissao/reducer';
import MapaTalhao from './mapaTalhao/reducer';
import Ocorrencia from './ocorrencia/reducer';

export default {
  Auth,
  App,
  Empresa,
  Filial,
  Cargo,
  Usuario,
  Fazenda,
  Insumo,
  ApontamentoChuva,
  ApontamentoArea,
  ApontamentoAreaPreparo,
  ApontamentoAtividadeMecanizada,
  ApontamentoAtividadesManuais,
  ApontamentoCombustivel,
  ApontamentoLubrificante,
  ApontamentoMoagemDiaria,
  ApontamentoPlantio,
  ApontamentoTerceirosMecanizado,
  AplicacaoInsumo,
  FechamentoCorte,
  OrdemCorte,
  OrdemServico,
  Equipamento,
  CentroCusto,
  Escala,
  Permissao,
  MapaTalhao,
  Ocorrencia,
};
