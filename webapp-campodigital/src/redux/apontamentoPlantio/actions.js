export const endpoint = 'apontamentos-plantio/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_PLANTIO',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_PLANTIO',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_PLANTIO',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_PLANTIO',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_PLANTIO',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_PLANTIO',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_PLANTIO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_PLANTIO',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_APONTAMENTO_PLANTIO',
  PATCH_SUCCESS: 'PATCH_SUCCESS_APONTAMENTO_PLANTIO',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;