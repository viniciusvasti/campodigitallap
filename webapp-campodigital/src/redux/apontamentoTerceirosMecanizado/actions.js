export const endpoint = 'apontamentos-terceiros-mecanizado/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_TERCEIROS_MECANIZADO',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_TERCEIROS_MECANIZADO',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_TERCEIROS_MECANIZADO',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_TERCEIROS_MECANIZADO',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_TERCEIROS_MECANIZADO',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_TERCEIROS_MECANIZADO',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_TERCEIROS_MECANIZADO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_TERCEIROS_MECANIZADO',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_APONTAMENTO_TERCEIROS_MECANIZADO',
  PATCH_SUCCESS: 'PATCH_SUCCESS_APONTAMENTO_TERCEIROS_MECANIZADO',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => {
    return {
      type: actions.SAVE_REQUEST,
      payload: entity
    }
  },
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;