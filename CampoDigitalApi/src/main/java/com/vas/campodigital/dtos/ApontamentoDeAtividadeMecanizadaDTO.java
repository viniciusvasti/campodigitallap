package com.vas.campodigital.dtos;

import java.util.Date;

/**
 *
 * @author vinicius
 */
public class ApontamentoDeAtividadeMecanizadaDTO {

    private Long id;
    private String dataApontamento;
    private int hOperador;
    private int implemento;
    private String horaKmInicial;
    private String horaKmFinal;
    private String horaKmTotal;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private CentroCustoDTO centroCusto;
    private OperacaoDTO operacao;
    private EquipamentoDTO equipamento;
    private TalhaoDTO talhao;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public int gethOperador() {
        return hOperador;
    }

    public void sethOperador(int hOperador) {
        this.hOperador = hOperador;
    }

    public EquipamentoDTO getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(EquipamentoDTO equipamento) {
        this.equipamento = equipamento;
    }

    public OperacaoDTO getOperacao() {
        return operacao;
    }

    public void setOperacao(OperacaoDTO operacao) {
        this.operacao = operacao;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public int getImplemento() {
        return implemento;
    }

    public void setImplemento(int implemento) {
        this.implemento = implemento;
    }

    public String getHoraKmInicial() {
        return horaKmInicial;
    }

    public void setHoraKmInicial(String horaKmInicial) {
        this.horaKmInicial = horaKmInicial;
    }

    public String getHoraKmFinal() {
        return horaKmFinal;
    }

    public void setHoraKmFinal(String horaKmFinal) {
        this.horaKmFinal = horaKmFinal;
    }

    public String getHoraKmTotal() {
        return horaKmTotal;
    }

    public void setHoraKmTotal(String horaKmTotal) {
        this.horaKmTotal = horaKmTotal;
    }

    public CentroCustoDTO getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCustoDTO centroCusto) {
        this.centroCusto = centroCusto;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeAtividadeMecanizadaDTO other = (ApontamentoDeAtividadeMecanizadaDTO) obj;
        return this.id.equals(other.id);
    }

}
