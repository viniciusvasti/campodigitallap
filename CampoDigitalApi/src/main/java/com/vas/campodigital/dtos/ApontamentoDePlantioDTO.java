package com.vas.campodigital.dtos;

import java.util.Date;

/**
 *
 * @author vinicius
 */
public class ApontamentoDePlantioDTO {

    private Long id;
    private String dataApontamento;
    private int variedade;
    private int espacamento;
    private double area;
    private boolean replantacao;
    private boolean localConcluido;
    private int procedenciaMuda;
    private int secaoMuda;
    private int talhaoMuda;
    private int nrCargas;
    private int pesoMedioKg;
    private int nrEquipes;
    private int nrCaminhoes;
    private double distanciaMuda;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private boolean digitadoSOL;
    private String usuarioDigitouSol;
    private CicloDeDesenvolvimentoDTO cicloDeDesenvolvimento;
    private SistemaDePlantioDTO sistemaDePlantio;
    private TalhaoDTO talhao;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public int getVariedade() {
        return variedade;
    }

    public void setVariedade(int variedade) {
        this.variedade = variedade;
    }

    public int getEspacamento() {
        return espacamento;
    }

    public void setEspacamento(int espacamento) {
        this.espacamento = espacamento;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public boolean isReplantacao() {
        return replantacao;
    }

    public void setReplantacao(boolean replantacao) {
        this.replantacao = replantacao;
    }

    public boolean isLocalConcluido() {
        return localConcluido;
    }

    public void setLocalConcluido(boolean localConcluido) {
        this.localConcluido = localConcluido;
    }

    public int getProcedenciaMuda() {
        return procedenciaMuda;
    }

    public void setProcedenciaMuda(int procedenciaMuda) {
        this.procedenciaMuda = procedenciaMuda;
    }

    public int getSecaoMuda() {
        return secaoMuda;
    }

    public void setSecaoMuda(int secaoMuda) {
        this.secaoMuda = secaoMuda;
    }

    public int getTalhaoMuda() {
        return talhaoMuda;
    }

    public void setTalhaoMuda(int talhaoMuda) {
        this.talhaoMuda = talhaoMuda;
    }

    public int getNrCargas() {
        return nrCargas;
    }

    public void setNrCargas(int nrCargas) {
        this.nrCargas = nrCargas;
    }

    public int getPesoMedioKg() {
        return pesoMedioKg;
    }

    public void setPesoMedioKg(int pesoMedioKg) {
        this.pesoMedioKg = pesoMedioKg;
    }

    public int getNrEquipes() {
        return nrEquipes;
    }

    public void setNrEquipes(int nrEquipes) {
        this.nrEquipes = nrEquipes;
    }

    public int getNrCaminhoes() {
        return nrCaminhoes;
    }

    public void setNrCaminhoes(int nrCaminhoes) {
        this.nrCaminhoes = nrCaminhoes;
    }

    public double getDistanciaMuda() {
        return distanciaMuda;
    }

    public void setDistanciaMuda(double distanciaMuda) {
        this.distanciaMuda = distanciaMuda;
    }

    public CicloDeDesenvolvimentoDTO getCicloDeDesenvolvimento() {
        return cicloDeDesenvolvimento;
    }

    public void setCicloDeDesenvolvimento(CicloDeDesenvolvimentoDTO cicloDeDesenvolvimento) {
        this.cicloDeDesenvolvimento = cicloDeDesenvolvimento;
    }

    public SistemaDePlantioDTO getSistemaDePlantio() {
        return sistemaDePlantio;
    }

    public void setSistemaDePlantio(SistemaDePlantioDTO sistemaDePlantio) {
        this.sistemaDePlantio = sistemaDePlantio;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setDigitadoSOL(boolean digitadoSOL) {
        this.digitadoSOL = digitadoSOL;
    }

    public void setUsuarioDigitouSol(String usuarioDigitouSol) {
        this.usuarioDigitouSol = usuarioDigitouSol;
    }

    public boolean isDigitadoSOL() {
        return digitadoSOL;
    }

    public String getUsuarioDigitouSol() {
        return usuarioDigitouSol;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDePlantioDTO other = (ApontamentoDePlantioDTO) obj;
        return this.id.equals(other.id);
    }

}
