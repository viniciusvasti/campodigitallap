package com.vas.campodigital.dtos;

import java.util.Date;

/**
 *
 * @author vinicius
 */
public class ApontamentoMoagemDiariaDTO {

    private Long id;
    private String dataApontamento;
    private int convencionalCrua;
    private int mecanizadaCrua;
    private int convencionalQueimada;
    private int mecanizadaQueimada;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public int getConvencionalCrua() {
        return convencionalCrua;
    }

    public void setConvencionalCrua(int convencionalCrua) {
        this.convencionalCrua = convencionalCrua;
    }

    public int getMecanizadaCrua() {
        return mecanizadaCrua;
    }

    public void setMecanizadaCrua(int mecanizadaCrua) {
        this.mecanizadaCrua = mecanizadaCrua;
    }

    public int getConvencionalQueimada() {
        return convencionalQueimada;
    }

    public void setConvencionalQueimada(int convencionalQueimada) {
        this.convencionalQueimada = convencionalQueimada;
    }

    public int getMecanizadaQueimada() {
        return mecanizadaQueimada;
    }

    public void setMecanizadaQueimada(int mecanizadaQueimada) {
        this.mecanizadaQueimada = mecanizadaQueimada;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoMoagemDiariaDTO other = (ApontamentoMoagemDiariaDTO) obj;
        return this.id.equals(other.id);
    }

}
