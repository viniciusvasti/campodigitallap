package com.vas.campodigital.dtos;

import com.vas.campodigital.enums.NivelCritico;
import com.vas.campodigital.models.secondary.Permissao;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author vinicius
 */
public class OcorrenciaDTO {

    private Long id;
    private TalhaoDTO talhao;
    private Permissao permissao;
    private String texto;
    private String location;
    private Set<UserFileDTO> fotos;
    private NivelCritico nivelCritico;
    private boolean concluido;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Set<UserFileDTO> getFotos() {
        return fotos;
    }

    public void setFotos(Set<UserFileDTO> fotos) {
        this.fotos = fotos;
    }

    public NivelCritico getNivelCritico() {
        return nivelCritico;
    }

    public void setNivelCritico(NivelCritico nivelCritico) {
        this.nivelCritico = nivelCritico;
    }

    public boolean isConcluido() {
        return concluido;
    }

    public void setConcluido(boolean concluido) {
        this.concluido = concluido;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OcorrenciaDTO other = (OcorrenciaDTO) obj;
        return this.id.equals(other.id);
    }

}
