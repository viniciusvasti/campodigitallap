package com.vas.campodigital.dtos;

import com.vas.campodigital.enums.FinalidadeLubrificante;
import java.util.Date;

/**
 *
 * @author vinicius
 */
public class ApontamentoDeLubrificanteDTO {

    private Long id;
    private String dataApontamento;
    private String pontoDeLubrificacao;
    private String justificativaRemontagem;
    private int objeto;
    private int sistemaVeicular;
    private int subSistemaVeicular;
    private int lubrificante;
    private int quantidade;
    private FinalidadeLubrificante finalidade;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private EquipamentoDTO equipamento;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getPontoDeLubrificacao() {
        return pontoDeLubrificacao;
    }

    public void setPontoDeLubrificacao(String pontoDeLubrificacao) {
        this.pontoDeLubrificacao = pontoDeLubrificacao;
    }

    public String getJustificativaRemontagem() {
        return justificativaRemontagem;
    }

    public void setJustificativaRemontagem(String justificativaRemontagem) {
        this.justificativaRemontagem = justificativaRemontagem;
    }

    public int getObjeto() {
        return objeto;
    }

    public void setObjeto(int objeto) {
        this.objeto = objeto;
    }

    public EquipamentoDTO getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(EquipamentoDTO equipamento) {
        this.equipamento = equipamento;
    }

    public int getSistemaVeicular() {
        return sistemaVeicular;
    }

    public void setSistemaVeicular(int sistemaVeicular) {
        this.sistemaVeicular = sistemaVeicular;
    }

    public int getSubSistemaVeicular() {
        return subSistemaVeicular;
    }

    public void setSubSistemaVeicular(int subSistemaVeicular) {
        this.subSistemaVeicular = subSistemaVeicular;
    }

    public int getLubrificante() {
        return lubrificante;
    }

    public void setLubrificante(int lubrificante) {
        this.lubrificante = lubrificante;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public FinalidadeLubrificante getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(FinalidadeLubrificante finalidade) {
        this.finalidade = finalidade;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeLubrificanteDTO other = (ApontamentoDeLubrificanteDTO) obj;
        return this.id.equals(other.id);
    }

}
