package com.vas.campodigital.dtos;

import java.util.Objects;

/**
 *
 * @author Vinicius
 */
public class FuncionarioDTO {

    private Long id;
    private String matricula;
    private FilialDTO filial;
    private CargoDTO cargo;

    public FuncionarioDTO() {
    }

    public void setFilial(FilialDTO filial) {
        this.filial = filial;
    }

    public FilialDTO getFilial() {
        return filial;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setCargo(CargoDTO cargo) {
        this.cargo = cargo;
    }

    public CargoDTO getCargo() {
        return cargo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.matricula);
        hash = 29 * hash + Objects.hashCode(this.filial);
        return hash;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FuncionarioDTO other = (FuncionarioDTO) obj;
        return this.id.equals(other.id);
    }

}
