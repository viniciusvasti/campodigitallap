
package com.vas.campodigital.dtos;

import java.util.Date;
import java.util.Set;

/**
 *
 * @author vinicius
 */
public class FazendaDTO {

    private Long id;
    private String nome;
    private String numero;
    private Set<SecaoDTO> secoes;
    private FilialDTO filial;
    
    private Date createdAt;
    private Date lastUpdated;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSecoes(Set<SecaoDTO> secoes) {
        this.secoes = secoes;
    }

    public Set<SecaoDTO> getSecoes() {
        return secoes;
    }

    public FilialDTO getFilial() {
        return filial;
    }

    public void setFilial(FilialDTO filial) {
        this.filial = filial;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FazendaDTO other = (FazendaDTO) obj;
        return this.id.equals(other.id);
    }
    
}
