package com.vas.campodigital.dtos;

import com.vas.campodigital.enums.CondicaoSolo;
import com.vas.campodigital.enums.CondicaoTempo;
import com.vas.campodigital.enums.RestoVegetacao;
import com.vas.campodigital.enums.TipoPreparacaoSolo;
import com.vas.campodigital.enums.Turno;
import java.util.Date;

/**
 *
 * @author vinicius
 */
public class AplicacaoDeInsumoDTO {

    private Long id;
    private String dataAplicacao;
    private boolean visualizado;
    private Turno turno;
    private double qntdProgHa;
    private CondicaoSolo condicaoSolo;
    private TipoPreparacaoSolo preparacaoSolo;
    private double doseProgHa;
    private int totaGeral;
    private double area;
    private CondicaoTempo condicaoTempo;
    private RestoVegetacao restoVegetacao;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private CentroCustoDTO centroCusto;
    private SistemaDeAplicacaoDTO sistemaDeAplicacao;
    private InsumoDTO insumo;
    private OperacaoDTO operacao;
    private TalhaoDTO talhao;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataAplicacao() {
        return dataAplicacao;
    }

    public void setDataAplicacao(String dataAplicacao) {
        this.dataAplicacao = dataAplicacao;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public double getQntdProgHa() {
        return qntdProgHa;
    }

    public void setQntdProgHa(double qntdProgHa) {
        this.qntdProgHa = qntdProgHa;
    }

    public CondicaoSolo getCondicaoSolo() {
        return condicaoSolo;
    }

    public void setCondicaoSolo(CondicaoSolo condicaoSolo) {
        this.condicaoSolo = condicaoSolo;
    }

    public TipoPreparacaoSolo getPreparacaoSolo() {
        return preparacaoSolo;
    }

    public void setPreparacaoSolo(TipoPreparacaoSolo preparacaoSolo) {
        this.preparacaoSolo = preparacaoSolo;
    }

    public double getDoseProgHa() {
        return doseProgHa;
    }

    public void setDoseProgHa(double doseProgHa) {
        this.doseProgHa = doseProgHa;
    }

    public int getTotaGeral() {
        return totaGeral;
    }

    public void setTotaGeral(int totaGeral) {
        this.totaGeral = totaGeral;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public CondicaoTempo getCondicaoTempo() {
        return condicaoTempo;
    }

    public void setCondicaoTempo(CondicaoTempo condicaoTempo) {
        this.condicaoTempo = condicaoTempo;
    }

    public RestoVegetacao getRestoVegetacao() {
        return restoVegetacao;
    }

    public void setRestoVegetacao(RestoVegetacao restoVegetacao) {
        this.restoVegetacao = restoVegetacao;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public CentroCustoDTO getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCustoDTO centroCusto) {
        this.centroCusto = centroCusto;
    }

    public SistemaDeAplicacaoDTO getSistemaDeAplicacao() {
        return sistemaDeAplicacao;
    }

    public void setSistemaDeAplicacao(SistemaDeAplicacaoDTO sistemaDeAplicacao) {
        this.sistemaDeAplicacao = sistemaDeAplicacao;
    }

    public InsumoDTO getInsumo() {
        return insumo;
    }

    public void setInsumo(InsumoDTO insumo) {
        this.insumo = insumo;
    }

    public OperacaoDTO getOperacao() {
        return operacao;
    }

    public void setOperacao(OperacaoDTO operacao) {
        this.operacao = operacao;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AplicacaoDeInsumoDTO other = (AplicacaoDeInsumoDTO) obj;
        return this.id.equals(other.id);
    }

}
