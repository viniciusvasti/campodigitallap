package com.vas.campodigital.dtos;

import com.vas.campodigital.enums.TipoCadastro;
import java.util.Date;

/**
 *
 * @author vinicius
 */
public class BloqueioCadastroDTO {

    private Long id;
    private TipoCadastro cadastro;
    private Date inicioBloqueio;
    private Date fimBloqueio;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public TipoCadastro getCadastro() {
        return cadastro;
    }

    public void setCadastro(TipoCadastro cadastro) {
        this.cadastro = cadastro;
    }

    public Date getInicioBloqueio() {
        return inicioBloqueio;
    }

    public void setInicioBloqueio(Date inicioBloqueio) {
        this.inicioBloqueio = inicioBloqueio;
    }

    public Date getFimBloqueio() {
        return fimBloqueio;
    }

    public void setFimBloqueio(Date fimBloqueio) {
        this.fimBloqueio = fimBloqueio;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BloqueioCadastroDTO other = (BloqueioCadastroDTO) obj;
        return this.id.equals(other.id);
    }

}
