package com.vas.campodigital.dtos;

import java.util.Date;
import java.util.Set;

/**
 *
 * @author vinicius
 */
public class EscalaDTO {

    private Long id;
    private Long idFazenda;
    private Long idSecao;
    private Long idTalhao;
    private Long idCentroCusto;
    private String atividade;
    private String coletor;
    private String complemento;
    private String nomeFazenda;
    private String numeroFazenda;
    private String secao;
    private String talhao;
    private String centroCusto;
    private Set<EquipamentoDTO> equipamentos;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    public String getColetor() {
        return coletor;
    }

    public void setColetor(String coletor) {
        this.coletor = coletor;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNomeFazenda() {
        return nomeFazenda;
    }

    public void setNomeFazenda(String nomeFazenda) {
        this.nomeFazenda = nomeFazenda;
    }

    public String getSecao() {
        return secao;
    }

    public void setSecao(String secao) {
        this.secao = secao;
    }

    public String getTalhao() {
        return talhao;
    }

    public void setTalhao(String talhao) {
        this.talhao = talhao;
    }

    public String getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(String centroCusto) {
        this.centroCusto = centroCusto;
    }

    public Set<EquipamentoDTO> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(Set<EquipamentoDTO> equipamentos) {
        this.equipamentos = equipamentos;
    }

    public void setNumeroFazenda(String numeroFazenda) {
        this.numeroFazenda = numeroFazenda;
    }

    public String getNumeroFazenda() {
        return numeroFazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    public void setIdFazenda(Long idFazenda) {
        this.idFazenda = idFazenda;
    }

    public Long getIdFazenda() {
        return idFazenda;
    }

    public void setIdTalhao(Long idTalhao) {
        this.idTalhao = idTalhao;
    }

    public Long getIdTalhao() {
        return idTalhao;
    }

    public void setIdSecao(Long idSecao) {
        this.idSecao = idSecao;
    }

    public Long getIdSecao() {
        return idSecao;
    }

    public void setIdCentroCusto(Long idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public Long getIdCentroCusto() {
        return idCentroCusto;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EscalaDTO other = (EscalaDTO) obj;
        return this.id.equals(other.id);
    }

}
