
package com.vas.campodigital.dtos;

import com.vas.campodigital.enums.TotalParcial;
import java.util.Date;

/**
 *
 * @author vinicius
 */
public class FechamentoDeCorteDTO {

    private Long id;
    private String dataFechamento;
    private TotalParcial tipoFechamento;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private OrdemDeCorteDTO ordemDeCorte;
    private String usuarioDigitouPims;
    private boolean digitadoSol;
    private String usuarioDigitouSol;
    private TalhaoDTO talhao;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public void setOrdemDeCorte(OrdemDeCorteDTO ordemDeCorte) {
        this.ordemDeCorte = ordemDeCorte;
    }

    public OrdemDeCorteDTO getOrdemDeCorte() {
        return ordemDeCorte;
    }

    public void setTipoFechamento(TotalParcial tipoFechamento) {
        this.tipoFechamento = tipoFechamento;
    }

    public TotalParcial getTipoFechamento() {
        return tipoFechamento;
    }

    public void setDataFechamento(String dataFechamento) {
        this.dataFechamento = dataFechamento;
    }

    public String getDataFechamento() {
        return dataFechamento;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setDigitadoSol(boolean digitadoSol) {
        this.digitadoSol = digitadoSol;
    }

    public void setUsuarioDigitouSol(String usuarioDigitouSol) {
        this.usuarioDigitouSol = usuarioDigitouSol;
    }

    public boolean isDigitadoSOL() {
        return digitadoSol;
    }

    public String getUsuarioDigitouSol() {
        return usuarioDigitouSol;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FechamentoDeCorteDTO other = (FechamentoDeCorteDTO) obj;
        return this.id.equals(other.id);
    }
    
}
