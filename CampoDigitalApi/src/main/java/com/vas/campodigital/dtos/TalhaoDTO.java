package com.vas.campodigital.dtos;

import com.vas.campodigital.models.UserFile;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author vinicius
 */
public class TalhaoDTO {

    private Long id;
    private String numero;
    private Set<EscalaDTO> escalas;
    private UserFile mapaPDF;
    private String fazenda;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setEscalas(Set<EscalaDTO> escalas) {
        this.escalas = escalas;
    }

    public Set<EscalaDTO> getEscalas() {
        return escalas;
    }

    public void setMapaPDF(UserFile mapaPDF) {
        this.mapaPDF = mapaPDF;
    }

    public UserFile getMapaPDF() {
        return mapaPDF;
    }

    public void setFazenda(String fazenda) {
        this.fazenda = fazenda;
    }

    public String getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TalhaoDTO other = (TalhaoDTO) obj;
        return this.id.equals(other.id);
    }

}
