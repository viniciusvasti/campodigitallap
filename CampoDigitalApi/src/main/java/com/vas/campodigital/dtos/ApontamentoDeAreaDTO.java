package com.vas.campodigital.dtos;

import java.util.Date;

/**
 *
 * @author vinicius
 */
public class ApontamentoDeAreaDTO {

    private Long id;
    private String dataApontamento;
    private boolean visualizado;
    private double producao;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private CentroCustoDTO centroCusto;
    private OperacaoDTO operacao;
    private TalhaoDTO talhao;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public double getProducao() {
        return producao;
    }

    public void setProducao(double producao) {
        this.producao = producao;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public CentroCustoDTO getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCustoDTO centroCusto) {
        this.centroCusto = centroCusto;
    }

    public OperacaoDTO getOperacao() {
        return operacao;
    }

    public void setOperacao(OperacaoDTO operacao) {
        this.operacao = operacao;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeAreaDTO other = (ApontamentoDeAreaDTO) obj;
        return this.id.equals(other.id);
    }

}
