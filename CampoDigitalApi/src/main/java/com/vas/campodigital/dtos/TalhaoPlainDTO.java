package com.vas.campodigital.dtos;

import com.vas.campodigital.models.UserFile;
import java.util.Set;

/**
 *
 * @author vinicius
 */
public class TalhaoPlainDTO {

    private Long id;
    private String numero;
    private Set<EscalaDTO> escalas;
    private UserFile mapaPDF;
    private String secao;
    private String fazenda;
    
    private boolean ativo;

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setEscalas(Set<EscalaDTO> escalas) {
        this.escalas = escalas;
    }

    public Set<EscalaDTO> getEscalas() {
        return escalas;
    }

    public void setMapaPDF(UserFile mapaPDF) {
        this.mapaPDF = mapaPDF;
    }

    public UserFile getMapaPDF() {
        return mapaPDF;
    }

    public String getSecao() {
        return secao;
    }

    public void setSecao(String secao) {
        this.secao = secao;
    }

    public String getFazenda() {
        return fazenda;
    }

    public void setFazenda(String fazenda) {
        this.fazenda = fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TalhaoPlainDTO other = (TalhaoPlainDTO) obj;
        return this.id.equals(other.id);
    }

}
