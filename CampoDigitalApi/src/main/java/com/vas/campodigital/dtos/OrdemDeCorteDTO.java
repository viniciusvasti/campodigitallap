
package com.vas.campodigital.dtos;

import com.vas.campodigital.enums.TotalParcial;
import com.vas.campodigital.models.secondary.SistemaDeCorte;
import java.util.Date;

/**
 *
 * @author vinicius
 */
public class OrdemDeCorteDTO {

    private Long id;
    private String dataCorte;
    private String hora;
    private boolean visualizado;
    private TotalParcial totalOuParcial;
    private double area;
    private double estimativaTonelada;
    private boolean fichaEntregue;
    private boolean digitadoPims;
    private String usuarioDigitouPims;
    private SistemaDeCorte sistemaDeCorte;
    private TalhaoDTO talhao;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataCorte() {
        return dataCorte;
    }

    public void setDataCorte(String dataCorte) {
        this.dataCorte = dataCorte;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public TalhaoDTO getTalhao() {
        return talhao;
    }

    public void setTalhao(TalhaoDTO talhao) {
        this.talhao = talhao;
    }

    public TotalParcial getTotalOuParcial() {
        return totalOuParcial;
    }

    public void setTotalOuParcial(TotalParcial totalOuParcial) {
        this.totalOuParcial = totalOuParcial;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getEstimativaTonelada() {
        return estimativaTonelada;
    }

    public void setEstimativaTonelada(double estimativaTonelada) {
        this.estimativaTonelada = estimativaTonelada;
    }

    public SistemaDeCorte getSistemaDeCorte() {
        return sistemaDeCorte;
    }

    public void setSistemaDeCorte(SistemaDeCorte sistemaDeCorte) {
        this.sistemaDeCorte = sistemaDeCorte;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(String usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public String getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdemDeCorteDTO other = (OrdemDeCorteDTO) obj;
        return this.id.equals(other.id);
    }
    
}
