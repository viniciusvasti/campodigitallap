/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.dtos;

import java.util.Date;

/**
 *
 * @author vinicius
 */
public class OrdemDeServicoDTO {

    private Long id;
    private String origem;
    private int objeto;
    private int classificacaoManutencao;
    private String motivo;
    private String saidaPrevista; // (data e hora)
    private double kmHEquipamento;
    private int pontoDeManutencao;
    private int modosDeOperacao;
    private int setoresDaOficina;
    private String observacao;
    private String servico;
    private boolean fechada;
    private boolean fichaEntregue;
    private String usuarioSolicitante;
    private String usuarioFechou;
    private EquipamentoDTO equipamento;
    private FazendaDTO fazenda;
    private FuncionarioDTO funcionario;
    private FuncionarioDTO funcionarioSolicitante;
    
    private Date createdAt;
    private Date lastUpdated;
    private String createdBy;
    private String updatedBy;
    private boolean ativo;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public int getObjeto() {
        return objeto;
    }

    public void setObjeto(int objeto) {
        this.objeto = objeto;
    }

    public int getClassificacaoManutencao() {
        return classificacaoManutencao;
    }

    public void setClassificacaoManutencao(int classificacaoManutencao) {
        this.classificacaoManutencao = classificacaoManutencao;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public EquipamentoDTO getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(EquipamentoDTO equipamento) {
        this.equipamento = equipamento;
    }

    public String getSaidaPrevista() {
        return saidaPrevista;
    }

    public void setSaidaPrevista(String saidaPrevista) {
        this.saidaPrevista = saidaPrevista;
    }

    public double getKmHEquipamento() {
        return kmHEquipamento;
    }

    public void setKmHEquipamento(double kmHEquipamento) {
        this.kmHEquipamento = kmHEquipamento;
    }

    public String getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    public void setUsuarioSolicitante(String usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }

    public int getPontoDeManutencao() {
        return pontoDeManutencao;
    }

    public void setPontoDeManutencao(int pontoDeManutencao) {
        this.pontoDeManutencao = pontoDeManutencao;
    }

    public int getModosDeOperacao() {
        return modosDeOperacao;
    }

    public void setModosDeOperacao(int modosDeOperacao) {
        this.modosDeOperacao = modosDeOperacao;
    }

    public int getSetoresDaOficina() {
        return setoresDaOficina;
    }

    public void setSetoresDaOficina(int setoresDaOficina) {
        this.setoresDaOficina = setoresDaOficina;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getServico() {
        return servico;
    }

    public void setServico(String servico) {
        this.servico = servico;
    }

    public boolean getFechada() {
        return fechada;
    }

    public void setFechada(boolean fechada) {
        this.fechada = fechada;
    }

    public String getUsuarioFechou() {
        return usuarioFechou;
    }

    public void setUsuarioFechou(String usuarioFechou) {
        this.usuarioFechou = usuarioFechou;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(FazendaDTO fazenda) {
        this.fazenda = fazenda;
    }

    public FazendaDTO getFazenda() {
        return fazenda;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    public void setFuncionarioSolicitante(FuncionarioDTO funcionarioSolicitante) {
        this.funcionarioSolicitante = funcionarioSolicitante;
    }

    public FuncionarioDTO getFuncionarioSolicitante() {
        return funcionarioSolicitante;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdemDeServicoDTO other = (OrdemDeServicoDTO) obj;
        return this.id.equals(other.id);
    }

}
