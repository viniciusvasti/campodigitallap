/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.enums;

/**
 *
 * @author vinicius
 */
public enum TipoCadastro {
    AIN, // Aplicação de Insumo
    APC, // Apontamento de Chuva
    APA, // Apontamento de Área
    APP, // Apontamento de Área de Preparo
    AAM, // Apontamento de Atividade Mecanizada
    AMA, // Apontamento de Atividades Manuais
    ACO, // Apontamento de Combustível
    APL, // Apontamento de Lubrificante
    APN, // Apontamento de Plantio
    ATM, // Apontamento de Terceiros Mecanizado
    AMD, // Apontamento de Moagem Diária
    FCO, // Fechamento de Corte
    OCO, // Ordem de Corte
    ODS, // Ordem de Servico
}
