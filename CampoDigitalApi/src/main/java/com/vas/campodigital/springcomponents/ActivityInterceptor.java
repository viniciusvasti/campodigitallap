package com.vas.campodigital.springcomponents;

import com.vas.campodigital.enums.ApplicationPlatform;
import com.vas.campodigital.models.Activity;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.Permissao;
import com.vas.campodigital.services.ActivityService;
import com.vas.campodigital.services.PermissaoService;
import com.vas.campodigital.services.UserService;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import static java.util.regex.Pattern.compile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author vinicius
 */
@Component
public class ActivityInterceptor extends HandlerInterceptorAdapter {

    private final ActivityService activityService;
    private final UserService userService;
    private final PermissaoService permissaoService;

    @Autowired
    public ActivityInterceptor(ActivityService activityService,
            PermissaoService permissaoService,
            UserService userService) {
        this.activityService = activityService;
        this.userService = userService;
        this.permissaoService = permissaoService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // get user
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);

        String userAgent = request.getHeader("User-Agent");
        if (userAgent == null) {
            userAgent = request.getHeader("user-agent");
        }

        verifyPermission(user, request.getRequestURI(), userAgent, response, request.getMethod());

        String expires = request.getHeader("Expires");
        Activity activity = new Activity();
        activity.setIp(this.getClientIpAddress(request));
        activity.setExpires(expires);
        activity.setRequestMethod(request.getMethod());
        activity.setUrl(request.getRequestURI());

        Matcher m = compile("\\(([^)]+)\\)").matcher(userAgent);
        if (m.find()) {
            activity.setUserAgent(m.group(1));
        }

        if (getContext().getAuthentication() != null
                && getContext().getAuthentication().isAuthenticated()
                && //when Anonymous Authentication is enabled
                !(getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
            activity.setUser(user);
            if (!activity.getUrl().contains("image") && !activity.getUrl().equals("/")) {
                activity = activityService.save(activity);
            }
            return super.preHandle(request, response, handler);
        } else if (activity.getUrl().equals("/")) {
            Activity existingActivity = this.activityService.findFirst();
            if (existingActivity != null) {
                activity.setId(existingActivity.getId());
                long totalVisitors = existingActivity.getTotalVisitors();
                activity.setTotalVisitors(++totalVisitors);
            } else {
                activity.setTotalVisitors(1L);
            }
            activity = this.activityService.save(activity);
        }
        return super.preHandle(request, response, handler);
    }

    private String getClientIpAddress(HttpServletRequest request) {
        String xForwardedForHeader = request.getHeader("X-Forwarded-For");
        if (xForwardedForHeader == null) {
            return request.getRemoteAddr();
        } else {
            // As of https://en.wikipedia.org/wiki/X-Forwarded-For
            // The general format of the field is: X-Forwarded-For: client, proxy1, proxy2 ...
            // we only want the client
            return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
        }
    }

    private void verifyPermission(User user, String endpoint, String platform, HttpServletResponse response, String method) throws Exception {
        if (method.equalsIgnoreCase("get")) {
            return;
        }
        ApplicationPlatform applicationPlatform
                = platform.contains("Mobile") ? ApplicationPlatform.MOBILE : ApplicationPlatform.WEB;
        if (!this.permissaoService.endpointsPermissao().contains(endpoint.replace("/", ""))) {
            return;
        }
        for (Permissao p : user.getPermissoes()) {
            if (p.getApplicationPlatform() == applicationPlatform
                    && endpoint.contains(p.getEndpoint())) {
                return;
            }
        }

        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getWriter().write("{ \"message\": \"Usuário sem permissão.\" }");
        response.getWriter().flush();
        response.getWriter().close();
        //throw new Exception("Usuário sem permissão.");
    }

}
