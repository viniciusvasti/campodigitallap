/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.springcomponents;

import com.vas.campodigital.enums.ApplicationPlatform;
import com.vas.campodigital.models.Empresa;
import com.vas.campodigital.models.Fazenda;
import com.vas.campodigital.models.Filial;
import com.vas.campodigital.models.Secao;
import com.vas.campodigital.models.Talhao;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.Cargo;
import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.models.secondary.CicloDeDesenvolvimento;
import com.vas.campodigital.models.secondary.Equipamento;
import com.vas.campodigital.models.secondary.Insumo;
import com.vas.campodigital.models.secondary.Operacao;
import com.vas.campodigital.models.secondary.Permissao;
import com.vas.campodigital.models.secondary.SistemaDeAplicacao;
import com.vas.campodigital.models.secondary.SistemaDeCorte;
import com.vas.campodigital.models.secondary.SistemaDePlantio;
import com.vas.campodigital.repositories.CargoRepository;
import com.vas.campodigital.repositories.CentroCustoRepository;
import com.vas.campodigital.repositories.CicloDeDesenvolvimentoRepository;
import com.vas.campodigital.repositories.EmpresaRepository;
import com.vas.campodigital.repositories.EquipamentoRepository;
import com.vas.campodigital.repositories.FazendaRepository;
import com.vas.campodigital.repositories.FilialRepository;
import com.vas.campodigital.repositories.InsumoRepository;
import com.vas.campodigital.repositories.OperacaoRepository;
import com.vas.campodigital.repositories.PermissaoRepository;
import com.vas.campodigital.repositories.SecaoRepository;
import com.vas.campodigital.repositories.SistemaDeAplicacaoRepository;
import com.vas.campodigital.repositories.SistemaDeCorteRepository;
import com.vas.campodigital.repositories.SistemaDePlantioRepository;
import com.vas.campodigital.repositories.TalhaoRepository;
import com.vas.campodigital.repositories.UserRepository;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author Vinícius
 */
@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;
    private final CargoRepository cargoRepository;
    private final CentroCustoRepository centroCustoRepository;
    private final OperacaoRepository operacaoRepository;
    private final SistemaDeAplicacaoRepository sistemaDeAplicacaoRepository;
    private final SistemaDeCorteRepository sistemaDeCorteRepository;
    private final SistemaDePlantioRepository sistemaDePlantioRepository;
    private final CicloDeDesenvolvimentoRepository cicloDeDesenvolvimentoRepository;
    private final EmpresaRepository empresaRepository;
    private final FilialRepository filialRepository;
    private final FazendaRepository fazendaRepository;
    private final SecaoRepository secaoRepository;
    private final TalhaoRepository talhaoRepository;
    private final InsumoRepository insumoRepository;
    private final EquipamentoRepository equipamentoRepository;
    private final PermissaoRepository permissaoRepository;

    public DevBootstrap(
            UserRepository userRepository,
            CentroCustoRepository centroCustoRepository,
            OperacaoRepository operacaoRepository,
            SistemaDeAplicacaoRepository sistemaDeAplicacaoRepository,
            SistemaDeCorteRepository sistemaDeCorteRepository,
            SistemaDePlantioRepository sistemaDePlantioRepository,
            CicloDeDesenvolvimentoRepository cicloDeDesenvolvimentoRepository,
            EmpresaRepository empresaRepository,
            FilialRepository filialRepository,
            FazendaRepository fazendaRepository,
            SecaoRepository secaoRepository,
            TalhaoRepository talhaoRepository,
            InsumoRepository insumoRepository,
            EquipamentoRepository equipamentoRepository,
            PermissaoRepository permissaoRepository,
            CargoRepository cargoRepository) {
        this.userRepository = userRepository;
        this.cargoRepository = cargoRepository;
        this.centroCustoRepository = centroCustoRepository;
        this.operacaoRepository = operacaoRepository;
        this.sistemaDeAplicacaoRepository = sistemaDeAplicacaoRepository;
        this.sistemaDeCorteRepository = sistemaDeCorteRepository;
        this.sistemaDePlantioRepository = sistemaDePlantioRepository;
        this.cicloDeDesenvolvimentoRepository = cicloDeDesenvolvimentoRepository;
        this.empresaRepository = empresaRepository;
        this.filialRepository = filialRepository;
        this.fazendaRepository = fazendaRepository;
        this.secaoRepository = secaoRepository;
        this.talhaoRepository = talhaoRepository;
        this.insumoRepository = insumoRepository;
        this.equipamentoRepository = equipamentoRepository;
        this.permissaoRepository = permissaoRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        try {
            initData();
        } catch (Exception ex) {
            out.println(ex.getMessage());
            getLogger(DevBootstrap.class.getName()).log(SEVERE, null, ex);
        }
    }

    private void initData() throws Exception {
        initPermissoes();
        initCentroCusto();
        initSistemaDeAplicacao();
        initSistemaDeCorte();
        initSistemaDePlantio();
        initCicloDeDesenvolvimento();
        initCargo();
        initInsumo();
        initUsers();
        initFazenda();
    }

    private void initUsers() throws Exception {
        if (userRepository.count() == 0) {
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            User userAdmin = new User("Administrador", "admin", bCryptPasswordEncoder.encode("admin123"));
            userAdmin.setEmail("admin@email.com");

            for (Permissao permissoe : permissaoRepository.findAll()) {
                userAdmin.getPermissoes().add(permissoe);
            }
            userRepository.save(userAdmin);
        }
    }

    private void initCentroCusto() {
        if (centroCustoRepository.count() == 0) {
            Set<Operacao> operacaos = new HashSet<>();
            operacaos.add(new Operacao("1", "Teste 1"));
            operacaos.add(new Operacao("2", "Teste 2"));
            operacaoRepository.saveAll(operacaos);
            Set<CentroCusto> centroCustos = new HashSet<>();
            centroCustos.add(new CentroCusto("101187", "Preparo de Solo", operacaos));
            centroCustos.add(new CentroCusto("101114", "Conservação Estrada", operacaos));
            centroCustos.add(new CentroCusto("101190", "Plantio Mecanizado", operacaos));
            centroCustos.add(new CentroCusto("101105", "Colheita Mecanizada", operacaos));
            centroCustos.add(new CentroCusto("101104", "Colheita Manual", operacaos));
            centroCustos.add(new CentroCusto("101951", "Pátio", operacaos));
            centroCustos.add(new CentroCusto("101191", "Cana Planta", operacaos));
            centroCustos.add(new CentroCusto("101192", "Cana Soca", operacaos));
            centroCustos.add(new CentroCusto("201040", "Patio Industrial", operacaos));
            centroCustos.add(new CentroCusto("101189", "Plantio Manual", operacaos));
            centroCustos.add(new CentroCusto("101970", "Pátio de Torta", operacaos));
            centroCustos.add(new CentroCusto("101201", "Polo Varietal", operacaos));
            centroCustos.add(new CentroCusto("101200", "Terceiros", operacaos));
            centroCustos.add(new CentroCusto("101115", "Construção de Estrada", operacaos));
            centroCustoRepository.saveAll(centroCustos);
        }
    }

    private void initSistemaDeAplicacao() {
        if (sistemaDeAplicacaoRepository.count() == 0) {
            Set<SistemaDeAplicacao> sistemaDeAplicacaos = new HashSet<>();
            sistemaDeAplicacaos.add(new SistemaDeAplicacao("1", "Pré - emergencial"));
            sistemaDeAplicacaos.add(new SistemaDeAplicacao("2", "Pós - (PT)"));
            sistemaDeAplicacaos.add(new SistemaDeAplicacao("3", "PPi - Plant. Inc"));
            sistemaDeAplicacaos.add(new SistemaDeAplicacao("4", "P.T Plantio Tardio"));
            sistemaDeAplicacaos.add(new SistemaDeAplicacao("99", "Genérico"));
            sistemaDeAplicacaoRepository.saveAll(sistemaDeAplicacaos);
        }
    }

    private void initSistemaDeCorte() {
        if (sistemaDeCorteRepository.count() == 0) {
            Set<SistemaDeCorte> sistemaDeCortes = new HashSet<>();
            sistemaDeCortes.add(new SistemaDeCorte("1", "Manual Queima"));
            sistemaDeCortes.add(new SistemaDeCorte("2", "Mecanizada Queimada"));
            sistemaDeCortes.add(new SistemaDeCorte("3", "Mecanizada Crua"));
            sistemaDeCortes.add(new SistemaDeCorte("4", "Manual Crua"));
            sistemaDeCortes.add(new SistemaDeCorte("5", "Manual C/Prentice"));
            sistemaDeCortes.add(new SistemaDeCorte("99", "A definir"));
            sistemaDeCorteRepository.saveAll(sistemaDeCortes);
        }
    }

    private void initSistemaDePlantio() {
        if (sistemaDePlantioRepository.count() == 0) {
            Set<SistemaDePlantio> sistemaDePlantios = new HashSet<>();
            sistemaDePlantios.add(new SistemaDePlantio("1", "Direto"));
            sistemaDePlantios.add(new SistemaDePlantio("2", "Convencional"));
            sistemaDePlantios.add(new SistemaDePlantio("3", "Mecanizado"));
            sistemaDePlantios.add(new SistemaDePlantio("4", "Meiosi - Cantosi"));
            sistemaDePlantios.add(new SistemaDePlantio("5", "Semi - Mecanizado"));
            sistemaDePlantios.add(new SistemaDePlantio("99", "Genérico"));
            sistemaDePlantioRepository.saveAll(sistemaDePlantios);
        }
    }

    private void initCicloDeDesenvolvimento() {
        if (cicloDeDesenvolvimentoRepository.count() == 0) {
            Set<CicloDeDesenvolvimento> cicloDeDesenvolvimentos = new HashSet<>();
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("1", "Cana de Ano - Expanção"));
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("2", "Cana Ano/Meio - Expanção"));
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("3", "Cana de Ano - Reform"));
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("4", "Cana Ano/Meio - Reform"));
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("5", "Cana 2 Verões - Expanção"));
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("6", "Cana 2 Verões - Reform"));
            cicloDeDesenvolvimentos.add(new CicloDeDesenvolvimento("99", "A definir"));
            cicloDeDesenvolvimentoRepository.saveAll(cicloDeDesenvolvimentos);
        }
    }

    private void initCargo() {
        if (cargoRepository.count() == 0) {
            Set<Cargo> cargos = new HashSet<>();
            cargos.add(new Cargo("1", "Teste Cargo 1"));
            cargos.add(new Cargo("2", "Teste Cargo 2"));
            cargos.add(new Cargo("3", "Teste Cargo 3"));
            cargoRepository.saveAll(cargos);
        }
    }

    private void initInsumo() {
        if (insumoRepository.count() == 0) {
            Set<Insumo> insumos = new HashSet<>();
            insumos.add(new Insumo("1", "Teste Insumo 1"));
            insumos.add(new Insumo("2", "Teste Insumo 2"));
            insumos.add(new Insumo("3", "Teste Insumo 3"));
            insumoRepository.saveAll(insumos);
        }
    }

    private void initFazenda() {
        if (fazendaRepository.count() == 0) {
            Empresa empresa = new Empresa("Vinicius Farm LTDA", "Vinicius Farm", "12345678912345");
            empresaRepository.save(empresa);
            Filial filial = new Filial("Vinicius Farm Matriz LTDA", "Vinicius Farm Matriz", "12345678912346");
            filial.setEmpresa(empresa);
            filialRepository.save(filial);
            Fazenda fazenda = new Fazenda();
            fazenda.setNome("Fazenda V & R");
            fazenda.setNumero("1234");
            fazenda.setFilial(filial);
            fazendaRepository.save(fazenda);
            Secao secao1 = new Secao();
            secao1.setFazenda(fazenda);
            secao1.setNumero("64363");
            secaoRepository.save(secao1);
            Secao secao2 = new Secao();
            secao2.setFazenda(fazenda);
            secao2.setNumero("5767");
            secaoRepository.save(secao2);
            Talhao talhao1 = new Talhao();
            talhao1.setSecao(secao1);
            talhao1.setNumero("8867678");
            talhaoRepository.save(talhao1);
            Talhao talhao2 = new Talhao();
            talhao2.setSecao(secao1);
            talhao2.setNumero("574467");
            talhaoRepository.save(talhao2);
            Talhao talhao11 = new Talhao();
            talhao11.setSecao(secao2);
            talhao11.setNumero("231546");
            talhaoRepository.save(talhao11);
            Talhao talhao12 = new Talhao();
            talhao12.setSecao(secao2);
            talhao12.setNumero("3462656");
            talhaoRepository.save(talhao12);

            Fazenda fazenda2 = new Fazenda();
            fazenda2.setNome("Fazenda L & N");
            fazenda2.setNumero("5346");
            fazenda2.setFilial(filialRepository.getOne(1L));
            fazendaRepository.save(fazenda2);
            Secao secao21 = new Secao();
            secao21.setFazenda(fazenda2);
            secao21.setNumero("9889");
            secaoRepository.save(secao21);
            Secao secao22 = new Secao();
            secao22.setFazenda(fazenda2);
            secao22.setNumero("4267");
            secaoRepository.save(secao22);
            Talhao talhao21 = new Talhao();
            talhao21.setSecao(secao21);
            talhao21.setNumero("87699");
            talhaoRepository.save(talhao21);
            Talhao talhao22 = new Talhao();
            talhao22.setSecao(secao21);
            talhao22.setNumero("12312");
            talhaoRepository.save(talhao22);
            Talhao talhao211 = new Talhao();
            talhao211.setSecao(secao22);
            talhao211.setNumero("75664");
            talhaoRepository.save(talhao211);
            Talhao talhao212 = new Talhao();
            talhao212.setSecao(secao22);
            talhao212.setNumero("800453");
            talhaoRepository.save(talhao212);

            Equipamento equipamento1 = new Equipamento();
            equipamento1.setDescricao("Equip 1");
            equipamento1.setNumero("00001");
            equipamento1.setFilial(filial);
            equipamentoRepository.save(equipamento1);
            Equipamento equipamento2 = new Equipamento();
            equipamento2.setDescricao("Equip 2");
            equipamento2.setNumero("00002");
            equipamento2.setFilial(filial);
            equipamentoRepository.save(equipamento2);
            Equipamento equipamento3 = new Equipamento();
            equipamento3.setDescricao("Equip 3");
            equipamento3.setNumero("00003");
            equipamento3.setFilial(filial);
            equipamentoRepository.save(equipamento3);
            Equipamento equipamento4 = new Equipamento();
            equipamento4.setDescricao("Equip 4");
            equipamento4.setNumero("00004");
            equipamento4.setFilial(filial);
            equipamentoRepository.save(equipamento4);
            Equipamento equipamento5 = new Equipamento();
            equipamento5.setDescricao("Equip 5");
            equipamento5.setNumero("00005");
            equipamento5.setFilial(filial);
            equipamentoRepository.save(equipamento5);
        }
    }

    private void initPermissoes() {
        if (permissaoRepository.count() == 0) {
            ArrayList<Permissao> permissoes = new ArrayList<>();

            // WEB
            permissoes.add(new Permissao(1l, "Aplicação de Insumo", "aplicacoes-insumo", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(2l, "Apontamento de Chuva", "apontamentos-chuva", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(3l, "Apontamento de Área", "apontamentos-area", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(4l, "Apontamento de Área de Preparo", "apontamentos-area-preparo", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(5l, "Apontamento de Atividade Mecanizada", "apontamentos-atividade-mecanizada", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(6l, "Apontamento de Atividades Manuais", "apontamentos-atividades-manuais", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(7l, "Apontamento de Combustível", "apontamentos-combustivel", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(8l, "Apontamento de Lubrificante", "apontamentos-lubrificante", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(9l, "Apontamento de Plantio", "apontamentos-plantio", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(10l, "Apontamento de Terceiros Mecanizado", "apontamentos-terceiros-mecanizado", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(11l, "Apontamento de Moagem Diária", "apontamentos-moagem-diaria", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(12l, "Ordem de Corte", "ordens-corte", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(13l, "Fechamento de Corte", "fechamentos-corte", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(14l, "Ordem de Servico", "ordens-servico", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(15l, "Escala", "escalas", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(16l, "Bloqueio de Cadastros", "bloqueios-cadastro", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(17l, "Fazenda", "fazendas", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(18l, "Filial", "filiais", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(19l, "Empresa", "empresas", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(20l, "Equipamento", "equipamentos", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(21l, "Insumo", "insumos", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(22l, "Ciclo de Desenvolvimento", "ciclos-desenvolvimento", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(23l, "Centro de Custo", "centros-custo", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(24l, "Cargo", "cargos", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(25l, "Operação", "operacaos", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(26l, "Sistema de Aplicação", "sistemas-aplicacao", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(27l, "Sistema de Corte", "sistemas-corte", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(28l, "Sistema de Plantio", "sistemas-plantio", ApplicationPlatform.WEB));
            permissoes.add(new Permissao(29l, "Usuários", "users", ApplicationPlatform.WEB));

            // MOBILE
            permissoes.add(new Permissao(30l, "Aplicação de Insumo", "aplicacoes-insumo", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(31l, "Apontamento de Chuva", "apontamentos-chuva", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(32l, "Apontamento de Área", "apontamentos-area", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(33l, "Apontamento de Área de Preparo", "apontamentos-area-preparo", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(34l, "Apontamento de Atividade Mecanizada", "apontamentos-atividade-mecanizada", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(35l, "Apontamento de Atividades Manuais", "apontamentos-atividades-manuais", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(36l, "Apontamento de Combustível", "apontamentos-combustivel", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(37l, "Apontamento de Lubrificante", "apontamentos-lubrificante", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(38l, "Apontamento de Plantio", "apontamentos-plantio", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(39l, "Apontamento de Terceiros Mecanizado", "apontamentos-terceiros-mecanizado", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(40l, "Apontamento de Moagem Diária", "apontamentos-moagem-diaria", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(41l, "Ordem de Corte", "ordens-corte", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(42l, "Fechamento de Corte", "fechamentos-corte", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(43l, "Ordem de Servico", "ordens-servico", ApplicationPlatform.MOBILE));
            permissoes.add(new Permissao(44l, "Escala", "escalas", ApplicationPlatform.MOBILE));

            permissaoRepository.saveAll(permissoes);
        }
    }
}
