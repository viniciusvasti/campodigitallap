/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeAtividadesManuaisDTO;
import com.vas.campodigital.models.ApontamentoDeAtividadesManuais;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeAtividadesManuaisService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeAtividadesManuaisController {

    private final ApontamentoDeAtividadesManuaisService apontamentoDeAtividadesManuaisService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeAtividadesManuaisController(
            ApontamentoDeAtividadesManuaisService apontamentoDeAtividadesManuaisService,
            UserService userService
    ) {
        this.apontamentoDeAtividadesManuaisService = apontamentoDeAtividadesManuaisService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-atividades-manuais")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeAtividadesManuais apontamentoDeAtividadesManuais) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeAtividadesManuais.setCreatedBy(user);
        apontamentoDeAtividadesManuais = apontamentoDeAtividadesManuaisService.save(apontamentoDeAtividadesManuais);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeAtividadesManuais.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-atividades-manuais")
    public @ResponseBody ResponseEntity<List<ApontamentoDeAtividadesManuaisDTO>> getAll() {
        List<ApontamentoDeAtividadesManuaisDTO> apontamentoDeAtividadesManuaiss = apontamentoDeAtividadesManuaisService.findAll();
        return new ResponseEntity<>(apontamentoDeAtividadesManuaiss, OK);
    }
    
    @GetMapping(value = "apontamentos-atividades-manuais/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeAtividadesManuaisDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeAtividadesManuais
        ApontamentoDeAtividadesManuaisDTO apontamentoDeAtividadesManuais = apontamentoDeAtividadesManuaisService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeAtividadesManuais, OK);
    }

    @PatchMapping(value = "/apontamentos-atividades-manuais/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeAtividadesManuais apontamentoDeAtividadesManuais) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeAtividadesManuais apontamentoDeAtividadesManuaisBD = apontamentoDeAtividadesManuaisService.findById(id);
        apontamentoDeAtividadesManuaisBD.setFichaEntregue(apontamentoDeAtividadesManuais.isFichaEntregue());
        apontamentoDeAtividadesManuaisService.update(apontamentoDeAtividadesManuaisBD, user);
        return ResponseEntity.ok(apontamentoDeAtividadesManuais.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
