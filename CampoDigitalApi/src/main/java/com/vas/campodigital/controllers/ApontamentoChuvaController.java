/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoChuvaDTO;
import com.vas.campodigital.models.ApontamentoChuva;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoChuvaService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoChuvaController {

    private final ApontamentoChuvaService apontamentoChuvaService;
    private final UserService userService;

    @Autowired
    public ApontamentoChuvaController(
            ApontamentoChuvaService apontamentoChuvaService,
            UserService userService
    ) {
        this.apontamentoChuvaService = apontamentoChuvaService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-chuva")
    public ResponseEntity<?> save(@RequestBody ApontamentoChuva apontamentoChuva) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoChuva.setCreatedBy(user);
        apontamentoChuva = apontamentoChuvaService.save(apontamentoChuva);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoChuva.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-chuva")
    public @ResponseBody
    ResponseEntity<List<ApontamentoChuvaDTO>> getAll() {
        List<ApontamentoChuvaDTO> apontamentoChuvas = apontamentoChuvaService.findAll();
        return new ResponseEntity<>(apontamentoChuvas, OK);
    }

    @GetMapping(value = "apontamentos-chuva/{id}")
    public @ResponseBody
    ResponseEntity<ApontamentoChuvaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoChuva
        ApontamentoChuvaDTO apontamentoChuva = apontamentoChuvaService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoChuva, OK);
    }

    @PatchMapping(value = "/apontamentos-chuva/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoChuva apontamentoChuva) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoChuva apontamentoChuvaBD = apontamentoChuvaService.findById(id);
        apontamentoChuvaBD.setUsuarioDigitouPims(user);
        apontamentoChuvaBD.setDigitadoPims(apontamentoChuva.isDigitadoPims());
        apontamentoChuvaService.update(apontamentoChuvaBD, user);
        return ResponseEntity.ok(apontamentoChuva.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-chuva/{id}/sol")
    public ResponseEntity<?> updateSol(
            @PathVariable Long id,
            @RequestBody ApontamentoChuva apontamentoChuva) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoChuva apontamentoChuvaBD = apontamentoChuvaService.findById(id);
        apontamentoChuvaBD.setUsuarioDigitouSol(user);
        apontamentoChuvaBD.setDigitadoSOL(apontamentoChuva.isDigitadoSOL());
        apontamentoChuvaService.update(apontamentoChuvaBD, user);
        return ResponseEntity.ok(apontamentoChuva.isDigitadoSOL() ? "Digitação do SOL confirmada" : "Digitação do SOL desfeita");
    }

    @PatchMapping(value = "/apontamentos-chuva/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoChuva apontamentoChuva) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoChuva apontamentoChuvaBD = apontamentoChuvaService.findById(id);
        apontamentoChuvaBD.setFichaEntregue(apontamentoChuva.isFichaEntregue());
        apontamentoChuvaService.update(apontamentoChuvaBD, user);
        return ResponseEntity.ok(apontamentoChuva.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
