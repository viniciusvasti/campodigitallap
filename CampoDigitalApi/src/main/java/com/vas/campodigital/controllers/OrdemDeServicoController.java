/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.OrdemDeServicoDTO;
import com.vas.campodigital.models.OrdemDeServico;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.OrdemDeServicoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class OrdemDeServicoController {

    private final OrdemDeServicoService ordemDeServicoService;
    private final UserService userService;

    @Autowired
    public OrdemDeServicoController(
            OrdemDeServicoService ordemDeServicoService,
            UserService userService
    ) {
        this.ordemDeServicoService = ordemDeServicoService;
        this.userService = userService;
    }

    @PostMapping(value = "/ordens-servico")
    public ResponseEntity<?> save(@RequestBody OrdemDeServico ordemDeServico) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ordemDeServico.setCreatedBy(user);
        ordemDeServico.setUsuarioSolicitante(user);
        ordemDeServico = ordemDeServicoService.save(ordemDeServico);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(ordemDeServico.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/ordens-servico")
    public @ResponseBody ResponseEntity<List<OrdemDeServicoDTO>> getAll() {
        List<OrdemDeServicoDTO> ordemDeServicos = ordemDeServicoService.findAll();
        return new ResponseEntity<>(ordemDeServicos, OK);
    }
    
    @GetMapping(value = "/ordens-servico/{id}")
    public @ResponseBody ResponseEntity<OrdemDeServicoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find ordemDeServico
        OrdemDeServicoDTO ordemDeServico = ordemDeServicoService.findByIdDTO(id);
        return new ResponseEntity<>(ordemDeServico, OK);
    }

    @PatchMapping(value = "/ordens-servico/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody OrdemDeServico ordemDeServico) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        OrdemDeServico ordemDeServicoBD = ordemDeServicoService.findById(id);
        ordemDeServicoBD.setFichaEntregue(ordemDeServico.isFichaEntregue());
        ordemDeServicoService.update(ordemDeServicoBD, user);
        return ResponseEntity.ok(ordemDeServico.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
