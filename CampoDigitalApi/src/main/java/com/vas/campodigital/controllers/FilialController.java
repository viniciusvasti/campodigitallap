/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.FilialDTO;
import com.vas.campodigital.models.Empresa;
import com.vas.campodigital.models.Filial;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.FilialService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class FilialController {

    private final FilialService filialService;
    private final UserService userService;

    @Autowired
    public FilialController(
            FilialService filialService,
            UserService userService
    ) {
        this.filialService = filialService;
        this.userService = userService;
    }

    @PostMapping(value = "/filiais")
    public ResponseEntity<?> save(@RequestBody Filial filial) {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        filial.setCreatedBy(user);
        filial = filialService.save(filial);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(filial.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/filiais/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Filial filial) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Filial filialDB = filialService.findById(id);
        filialDB.setUpdatedBy(user);
        Empresa empresa = new Empresa();
        empresa.setId(filial.getEmpresa().getId());
        filialDB.setEmpresa(empresa);
        filialDB.setAtivo(filial.isAtivo());
        filialDB.setCnpj(filial.getCnpj());
        filialDB.setNomeFantasia(filial.getNomeFantasia());
        filialDB.setRazaoSocial(filial.getRazaoSocial());
        filialService.save(filialDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return ok(location);
    }

    @GetMapping(value = "/filiais")
    public @ResponseBody
    ResponseEntity<List<FilialDTO>> getAll() {
        List<FilialDTO> filials = filialService.findAll();
        return new ResponseEntity<>(filials, OK);
    }

    @GetMapping(value = "filiais/{id}")
    public @ResponseBody
    ResponseEntity<FilialDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find filial
        FilialDTO filial = filialService.findByIdDTO(id);
        return new ResponseEntity<>(filial, OK);
    }

}
