package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.TalhaoPlainDTO;
import com.vas.campodigital.models.UserFile;
import com.vas.campodigital.models.Talhao;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.FileService;
import com.vas.campodigital.services.TalhaoService;
import com.vas.campodigital.services.UserService;
import com.vas.campodigital.storage.StorageFileNotFoundException;
import com.vas.campodigital.storage.StorageService;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class MapaTalhaoUploadController {

    private final StorageService storageService;
    private final TalhaoService talhaoService;
    private final FileService fileService;
    private final UserService userService;

    @Autowired
    public MapaTalhaoUploadController(
            StorageService storageService,
            FileService fileService,
            UserService userService,
            TalhaoService talhaoService) {
        this.storageService = storageService;
        this.talhaoService = talhaoService;
        this.fileService = fileService;
        this.userService = userService;
    }

    @GetMapping("/mapas-talhao")
    public @ResponseBody
    ResponseEntity<List<TalhaoPlainDTO>> getAll() {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        List<TalhaoPlainDTO> talhoes = talhaoService.findAllWithMapFile(user);
        return new ResponseEntity<>(talhoes, OK);
    }

    @GetMapping("/mapas-talhao/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename, HttpServletRequest request) throws Exception {
        // Load file as Resource
        Resource resource = storageService.loadAsResource(filename);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            throw new Exception("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping("/mapas-talhao/{filename:.+}/base64")
    public @ResponseBody ResponseEntity<String> getBase64File(@PathVariable String filename) throws IOException {
        // Load file as Resource
        Resource resource = storageService.loadAsResource(filename);
        java.io.File file = resource.getFile();
        byte[] bytes = loadFile(file);
        byte[] encoded = Base64.getEncoder().encode(bytes);
        String encodedString = new String(encoded);
        return new ResponseEntity<>(encodedString, OK);
    }

    @PostMapping("/mapas-talhao")
    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file,
            @RequestParam List<Integer> talhoes) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        String filePath = storageService.store(file, "mapas");
        UserFile f = new UserFile();
        f.setCreatedBy(user);
        f.setFileName(file.getOriginalFilename());
        f.setPath(filePath);
        fileService.save(f);
        for (Integer t : talhoes) {
            Talhao talhao = talhaoService.findById(Long.parseLong(String.valueOf(t)));
            talhao.setMapaPDF(f);
            talhaoService.save(talhao);
        }

        //return "redirect:/";
        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    private static byte[] loadFile(java.io.File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

}
