package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeAreaPreparoDTO;
import com.vas.campodigital.models.ApontamentoDeAreaPreparo;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeAreaPreparoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeAreaPreparoController {

    private final ApontamentoDeAreaPreparoService apontamentoDeAreaPreparoService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeAreaPreparoController(
            ApontamentoDeAreaPreparoService apontamentoDeAreaPreparoService,
            UserService userService
    ) {
        this.apontamentoDeAreaPreparoService = apontamentoDeAreaPreparoService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-area-preparo")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeAreaPreparo apontamentoDeAreaPreparo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeAreaPreparo.setCreatedBy(user);
        apontamentoDeAreaPreparo = apontamentoDeAreaPreparoService.save(apontamentoDeAreaPreparo);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeAreaPreparo.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-area-preparo")
    public @ResponseBody ResponseEntity<List<ApontamentoDeAreaPreparoDTO>> getAll() {
        List<ApontamentoDeAreaPreparoDTO> apontamentoDeAreaPreparos = apontamentoDeAreaPreparoService.findAll();
        return new ResponseEntity<>(apontamentoDeAreaPreparos, OK);
    }
    
    @GetMapping(value = "apontamentos-area-preparo/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeAreaPreparoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeAreaPreparo
        ApontamentoDeAreaPreparoDTO apontamentoDeAreaPreparo = apontamentoDeAreaPreparoService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeAreaPreparo, OK);
    }

    @PatchMapping(value = "/apontamentos-area-preparo/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDeAreaPreparo apontamentoDeAreaPreparo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeAreaPreparo apontamentoDeAreaPreparoBD = apontamentoDeAreaPreparoService.findById(id);
        apontamentoDeAreaPreparoBD.setUsuarioDigitouPims(user);
        apontamentoDeAreaPreparoBD.setDigitadoPims(apontamentoDeAreaPreparo.isDigitadoPims());
        apontamentoDeAreaPreparoService.update(apontamentoDeAreaPreparoBD, user);
        return ResponseEntity.ok(apontamentoDeAreaPreparo.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-area-preparo/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeAreaPreparo apontamentoDeAreaPreparo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeAreaPreparo apontamentoDeAreaPreparoBD = apontamentoDeAreaPreparoService.findById(id);
        apontamentoDeAreaPreparoBD.setFichaEntregue(apontamentoDeAreaPreparo.isFichaEntregue());
        apontamentoDeAreaPreparoService.update(apontamentoDeAreaPreparoBD, user);
        return ResponseEntity.ok(apontamentoDeAreaPreparo.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
