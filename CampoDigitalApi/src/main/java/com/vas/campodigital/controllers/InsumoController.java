package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.InsumoDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.Insumo;
import com.vas.campodigital.services.InsumoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class InsumoController {

    private final InsumoService insumoService;
    private final UserService userService;

    @Autowired
    public InsumoController(
            InsumoService insumoService,
            UserService userService
    ) {
        this.insumoService = insumoService;
        this.userService = userService;
    }

    @PostMapping(value = "/insumos")
    public ResponseEntity<?> save(@RequestBody Insumo insumo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        insumo.setCreatedBy(user);
        insumo = insumoService.save(insumo);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(insumo.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/insumos/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Insumo insumo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Insumo insumoDB = insumoService.findById(id);
        insumoDB.setUpdatedBy(user);
        insumoDB.setAtivo(insumo.isAtivo());
        insumoDB.setDescricao(insumo.getDescricao());
        insumoDB.setNumero(insumo.getNumero());
        insumoService.save(insumoDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/insumos")
    public @ResponseBody ResponseEntity<List<InsumoDTO>> getAll() {
        List<InsumoDTO> insumos = insumoService.findAll();
        return new ResponseEntity<>(insumos, OK);
    }
    
    @GetMapping(value = "/insumos/{id}")
    public @ResponseBody ResponseEntity<InsumoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find insumo
        InsumoDTO insumo = insumoService.findByIdDTO(id);
        return new ResponseEntity<>(insumo, OK);
    }

}
