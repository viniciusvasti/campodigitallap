/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.CicloDeDesenvolvimentoDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.CicloDeDesenvolvimento;
import com.vas.campodigital.services.CicloDeDesenvolvimentoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class CicloDeDesenvolvimentoController {

    private final CicloDeDesenvolvimentoService cicloDeDesenvolvimentoService;
    private final UserService userService;

    @Autowired
    public CicloDeDesenvolvimentoController(
            CicloDeDesenvolvimentoService cicloDeDesenvolvimentoService,
            UserService userService
    ) {
        this.cicloDeDesenvolvimentoService = cicloDeDesenvolvimentoService;
        this.userService = userService;
    }

    @PostMapping(value = "/ciclos-desenvolvimento")
    public ResponseEntity<?> save(@RequestBody CicloDeDesenvolvimento cicloDeDesenvolvimento) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        cicloDeDesenvolvimento.setCreatedBy(user);
        cicloDeDesenvolvimento = cicloDeDesenvolvimentoService.save(cicloDeDesenvolvimento);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(cicloDeDesenvolvimento.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/ciclos-desenvolvimento/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody CicloDeDesenvolvimento cicloDeDesenvolvimento) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        CicloDeDesenvolvimento cicloDeDesenvolvimentoDB = cicloDeDesenvolvimentoService.findById(id);
        cicloDeDesenvolvimentoDB.setUpdatedBy(user);
        cicloDeDesenvolvimentoDB.setAtivo(cicloDeDesenvolvimento.isAtivo());
        cicloDeDesenvolvimentoDB.setDescricao(cicloDeDesenvolvimento.getDescricao());
        cicloDeDesenvolvimentoDB.setNumero(cicloDeDesenvolvimento.getNumero());
        cicloDeDesenvolvimentoService.save(cicloDeDesenvolvimentoDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/ciclos-desenvolvimento")
    public @ResponseBody ResponseEntity<List<CicloDeDesenvolvimentoDTO>> getAll() {
        List<CicloDeDesenvolvimentoDTO> cicloDeDesenvolvimentos = cicloDeDesenvolvimentoService.findAll();
        return new ResponseEntity<>(cicloDeDesenvolvimentos, OK);
    }
    
    @GetMapping(value = "/ciclos-desenvolvimento/{id}")
    public @ResponseBody ResponseEntity<CicloDeDesenvolvimentoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find cicloDeDesenvolvimento
        CicloDeDesenvolvimentoDTO cicloDeDesenvolvimento = cicloDeDesenvolvimentoService.findByIdDTO(id);
        return new ResponseEntity<>(cicloDeDesenvolvimento, OK);
    }

}
