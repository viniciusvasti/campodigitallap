/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.AplicacaoDeInsumoDTO;
import com.vas.campodigital.models.AplicacaoDeInsumo;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.AplicacaoDeInsumoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class AplicacaoDeInsumoController {

    private final AplicacaoDeInsumoService aplicacaoDeInsumoService;
    private final UserService userService;

    @Autowired
    public AplicacaoDeInsumoController(
            AplicacaoDeInsumoService aplicacaoDeInsumoService,
            UserService userService
    ) {
        this.aplicacaoDeInsumoService = aplicacaoDeInsumoService;
        this.userService = userService;
    }

    @PostMapping(value = "/aplicacoes-insumo")
    public ResponseEntity<?> save(@RequestBody AplicacaoDeInsumo aplicacaoDeInsumo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        aplicacaoDeInsumo.setCreatedBy(user);
        aplicacaoDeInsumo = aplicacaoDeInsumoService.save(aplicacaoDeInsumo);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(aplicacaoDeInsumo.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/aplicacoes-insumo")
    public @ResponseBody ResponseEntity<List<AplicacaoDeInsumoDTO>> getAll() {
        List<AplicacaoDeInsumoDTO> aplicacaoDeInsumos = aplicacaoDeInsumoService.findAll();
        return new ResponseEntity<>(aplicacaoDeInsumos, OK);
    }
    
    @GetMapping(value = "/aplicacoes-insumo/{id}")
    public @ResponseBody ResponseEntity<AplicacaoDeInsumoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find aplicacaoDeInsumo
        AplicacaoDeInsumoDTO aplicacaoDeInsumo = aplicacaoDeInsumoService.findByIdDTO(id);
        return new ResponseEntity<>(aplicacaoDeInsumo, OK);
    }

    @PatchMapping(value = "/aplicacoes-insumo/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody AplicacaoDeInsumo aplicacaoDeInsumo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        AplicacaoDeInsumo aplicacaoDeInsumoBD = aplicacaoDeInsumoService.findById(id);
        aplicacaoDeInsumoBD.setUsuarioDigitouPims(user);
        aplicacaoDeInsumoBD.setDigitadoPims(aplicacaoDeInsumo.isDigitadoPims());
        aplicacaoDeInsumoService.update(aplicacaoDeInsumoBD, user);
        return ResponseEntity.ok(aplicacaoDeInsumo.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/aplicacoes-insumo/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody AplicacaoDeInsumo aplicacaoDeInsumo) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        AplicacaoDeInsumo aplicacaoDeInsumoBD = aplicacaoDeInsumoService.findById(id);
        aplicacaoDeInsumoBD.setFichaEntregue(aplicacaoDeInsumo.isFichaEntregue());
        aplicacaoDeInsumoService.update(aplicacaoDeInsumoBD, user);
        return ResponseEntity.ok(aplicacaoDeInsumo.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
