/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeAreaDTO;
import com.vas.campodigital.models.ApontamentoDeArea;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeAreaService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeAreaController {

    private final ApontamentoDeAreaService apontamentoDeAreaService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeAreaController(
            ApontamentoDeAreaService apontamentoDeAreaService,
            UserService userService
    ) {
        this.apontamentoDeAreaService = apontamentoDeAreaService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-area")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeArea apontamentoDeArea) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeArea.setCreatedBy(user);
        apontamentoDeArea = apontamentoDeAreaService.save(apontamentoDeArea);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeArea.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-area")
    public @ResponseBody ResponseEntity<List<ApontamentoDeAreaDTO>> getAll() {
        List<ApontamentoDeAreaDTO> apontamentoDeAreas = apontamentoDeAreaService.findAll();
        return new ResponseEntity<>(apontamentoDeAreas, OK);
    }
    
    @GetMapping(value = "apontamentos-area/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeAreaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeArea
        ApontamentoDeAreaDTO apontamentoDeArea = apontamentoDeAreaService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeArea, OK);
    }

    @PatchMapping(value = "/apontamentos-area/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDeArea apontamentoDeArea) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeArea apontamentoDeAreaBD = apontamentoDeAreaService.findById(id);
        apontamentoDeAreaBD.setUsuarioDigitouPims(user);
        apontamentoDeAreaBD.setDigitadoPims(apontamentoDeArea.isDigitadoPims());
        apontamentoDeAreaService.update(apontamentoDeAreaBD, user);
        return ResponseEntity.ok(apontamentoDeArea.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-area/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeArea apontamentoDeArea) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeArea apontamentoDeAreaBD = apontamentoDeAreaService.findById(id);
        apontamentoDeAreaBD.setFichaEntregue(apontamentoDeArea.isFichaEntregue());
        apontamentoDeAreaService.update(apontamentoDeAreaBD, user);
        return ResponseEntity.ok(apontamentoDeArea.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
