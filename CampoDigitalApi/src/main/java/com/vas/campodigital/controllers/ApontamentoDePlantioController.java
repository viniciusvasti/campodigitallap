/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDePlantioDTO;
import com.vas.campodigital.models.ApontamentoDePlantio;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDePlantioService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDePlantioController {

    private final ApontamentoDePlantioService apontamentoDePlantioService;
    private final UserService userService;

    @Autowired
    public ApontamentoDePlantioController(
            ApontamentoDePlantioService apontamentoDePlantioService,
            UserService userService
    ) {
        this.apontamentoDePlantioService = apontamentoDePlantioService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-plantio")
    public ResponseEntity<?> save(@RequestBody ApontamentoDePlantio apontamentoDePlantio) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDePlantio.setCreatedBy(user);
        apontamentoDePlantio = apontamentoDePlantioService.save(apontamentoDePlantio);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDePlantio.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-plantio")
    public @ResponseBody ResponseEntity<List<ApontamentoDePlantioDTO>> getAll() {
        List<ApontamentoDePlantioDTO> apontamentoDePlantios = apontamentoDePlantioService.findAll();
        return new ResponseEntity<>(apontamentoDePlantios, OK);
    }
    
    @GetMapping(value = "apontamentos-plantio/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDePlantioDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDePlantio
        ApontamentoDePlantioDTO apontamentoDePlantio = apontamentoDePlantioService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDePlantio, OK);
    }

    @PatchMapping(value = "/apontamentos-plantio/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDePlantio apontamentoDePlantio) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDePlantio apontamentoDePlantioBD = apontamentoDePlantioService.findById(id);
        apontamentoDePlantioBD.setUsuarioDigitouPims(user);
        apontamentoDePlantioBD.setDigitadoPims(apontamentoDePlantio.isDigitadoPims());
        apontamentoDePlantioService.update(apontamentoDePlantioBD, user);
        return ResponseEntity.ok(apontamentoDePlantio.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-plantio/{id}/sol")
    public ResponseEntity<?> updateSol(
            @PathVariable Long id,
            @RequestBody ApontamentoDePlantio apontamentoDePlantio) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDePlantio apontamentoDePlantioBD = apontamentoDePlantioService.findById(id);
        apontamentoDePlantioBD.setUsuarioDigitouSol(user);
        apontamentoDePlantioBD.setDigitadoSOL(apontamentoDePlantio.isDigitadoSOL());
        apontamentoDePlantioService.update(apontamentoDePlantioBD, user);
        return ResponseEntity.ok(apontamentoDePlantio.isDigitadoSOL() ? "Digitação do SOL confirmada" : "Digitação do SOL desfeita");
    }

    @PatchMapping(value = "/apontamentos-plantio/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDePlantio apontamentoDePlantio) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDePlantio apontamentoDePlantioBD = apontamentoDePlantioService.findById(id);
        apontamentoDePlantioBD.setFichaEntregue(apontamentoDePlantio.isFichaEntregue());
        apontamentoDePlantioService.update(apontamentoDePlantioBD, user);
        return ResponseEntity.ok(apontamentoDePlantio.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
