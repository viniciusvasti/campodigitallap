/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.CentroCustoDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.services.CentroCustoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class CentroCustoController {

    private final CentroCustoService centroCustoService;
    private final UserService userService;

    @Autowired
    public CentroCustoController(
            CentroCustoService centroCustoService,
            UserService userService
    ) {
        this.centroCustoService = centroCustoService;
        this.userService = userService;
    }

    @PostMapping(value = "/centros-custo")
    public ResponseEntity<?> save(@RequestBody CentroCusto centroCusto) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        centroCusto.setCreatedBy(user);
        centroCusto = centroCustoService.save(centroCusto);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(centroCusto.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/centros-custo/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody CentroCusto centroCusto) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        CentroCusto centroCustoDB = centroCustoService.findById(id);
        centroCustoDB.setUpdatedBy(user);
        centroCustoDB.setAtivo(centroCusto.isAtivo());
        centroCustoDB.setDescricao(centroCusto.getDescricao());
        centroCustoDB.setNumero(centroCusto.getNumero());
        centroCustoService.save(centroCustoDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/centros-custo")
    public @ResponseBody ResponseEntity<List<CentroCustoDTO>> getAll() {
        List<CentroCustoDTO> centroCustos = centroCustoService.findAll();
        return new ResponseEntity<>(centroCustos, OK);
    }
    
    @GetMapping(value = "/centros-custo/{id}")
    public @ResponseBody ResponseEntity<CentroCustoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find centroCusto
        CentroCustoDTO centroCusto = centroCustoService.findByIdDTO(id);
        return new ResponseEntity<>(centroCusto, OK);
    }

}
