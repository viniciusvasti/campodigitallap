/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.FechamentoDeCorteDTO;
import com.vas.campodigital.models.FechamentoDeCorte;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.FechamentoDeCorteService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class FechamentoDeCorteController {

    private final FechamentoDeCorteService fechamentoDeCorteService;
    private final UserService userService;

    @Autowired
    public FechamentoDeCorteController(
            FechamentoDeCorteService fechamentoDeCorteService,
            UserService userService
    ) {
        this.fechamentoDeCorteService = fechamentoDeCorteService;
        this.userService = userService;
    }

    @PostMapping(value = "/fechamentos-corte")
    public ResponseEntity<?> save(@RequestBody FechamentoDeCorte fechamentoDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        fechamentoDeCorte.setCreatedBy(user);
        fechamentoDeCorte = fechamentoDeCorteService.save(fechamentoDeCorte);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(fechamentoDeCorte.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/fechamentos-corte")
    public @ResponseBody ResponseEntity<List<FechamentoDeCorteDTO>> getAll() {
        List<FechamentoDeCorteDTO> fechamentoDeCortes = fechamentoDeCorteService.findAll();
        return new ResponseEntity<>(fechamentoDeCortes, OK);
    }
    
    @GetMapping(value = "/fechamentos-corte/{id}")
    public @ResponseBody ResponseEntity<FechamentoDeCorteDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find fechamentoDeCorte
        FechamentoDeCorteDTO fechamentoDeCorte = fechamentoDeCorteService.findByIdDTO(id);
        return new ResponseEntity<>(fechamentoDeCorte, OK);
    }

    @PatchMapping(value = "/fechamentos-corte/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody FechamentoDeCorte fechamentoDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        FechamentoDeCorte fechamentoDeCorteBD = fechamentoDeCorteService.findById(id);
        fechamentoDeCorteBD.setUsuarioDigitouPims(user);
        fechamentoDeCorteBD.setDigitadoPims(fechamentoDeCorte.isDigitadoPims());
        fechamentoDeCorteService.update(fechamentoDeCorteBD, user);
        return ResponseEntity.ok(fechamentoDeCorte.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/fechamentos-corte/{id}/sol")
    public ResponseEntity<?> updateSol(
            @PathVariable Long id,
            @RequestBody FechamentoDeCorte fechamentoDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        FechamentoDeCorte fechamentoDeCorteBD = fechamentoDeCorteService.findById(id);
        fechamentoDeCorteBD.setUsuarioDigitouSol(user);
        fechamentoDeCorteBD.setDigitadoSol(fechamentoDeCorte.isDigitadoSOL());
        fechamentoDeCorteService.update(fechamentoDeCorteBD, user);
        return ResponseEntity.ok(fechamentoDeCorte.isDigitadoSOL() ? "Digitação do SOL confirmada" : "Digitação do SOL desfeita");
    }

    @PatchMapping(value = "/fechamentos-corte/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody FechamentoDeCorte fechamentoDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        FechamentoDeCorte fechamentoDeCorteBD = fechamentoDeCorteService.findById(id);
        fechamentoDeCorteBD.setFichaEntregue(fechamentoDeCorte.isFichaEntregue());
        fechamentoDeCorteService.update(fechamentoDeCorteBD, user);
        return ResponseEntity.ok(fechamentoDeCorte.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
