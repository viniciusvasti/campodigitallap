package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.EscalaDTO;
import com.vas.campodigital.models.Escala;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.EscalaService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class EscalaController {

    private final EscalaService escalaService;
    private final UserService userService;

    @Autowired
    public EscalaController(
            EscalaService escalaService,
            UserService userService
    ) {
        this.escalaService = escalaService;
        this.userService = userService;
    }

    @PostMapping(value = "/escalas")
    public ResponseEntity<?> save(@RequestBody Escala escala) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        escala.setCreatedBy(user);
        escala = escalaService.save(escala);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(escala.getId()).toUri();
        return created(location).build();
    }

//    @PutMapping(value = "/escalas/{id}")
//    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Escala escala) throws Exception {
//        String username = (String) getContext().getAuthentication().getPrincipal();
//        User user = userService.findUserByUsername(username);
//        Escala escalaDB = escalaService.findByIdDTO(id);
//        escalaDB.setUpdatedBy(user);
//        escalaDB.setAtivo(escala.isAtivo());
//        escalaDB.setDescricao(escala.getDescricao());
//        escalaDB.setNumero(escala.getNumero());
//        escalaService.save(escalaDB);
//        URI location = fromCurrentRequest().path("")
//                .buildAndExpand("").toUri();
//        return created(location).build();
//    }

    @GetMapping(value = "/escalas")
    public @ResponseBody
    ResponseEntity<List<EscalaDTO>> getAll() {
        List<EscalaDTO> escalas = escalaService.findAllDTO();
        return new ResponseEntity<>(escalas, OK);
    }

    @GetMapping(value = "escalas/{id}")
    public @ResponseBody
    ResponseEntity<EscalaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find escala
        EscalaDTO escala = escalaService.findByIdDTO(id);
        return new ResponseEntity<>(escala, OK);
    }

}
