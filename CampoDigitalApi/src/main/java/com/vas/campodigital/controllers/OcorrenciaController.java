package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.OcorrenciaDTO;
import com.vas.campodigital.enums.NivelCritico;
import com.vas.campodigital.models.UserFile;
import com.vas.campodigital.models.Ocorrencia;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.FileService;
import com.vas.campodigital.services.OcorrenciaService;
import com.vas.campodigital.services.PermissaoService;
import com.vas.campodigital.services.TalhaoService;
import com.vas.campodigital.services.UserService;
import com.vas.campodigital.storage.StorageService;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class OcorrenciaController {

    private final OcorrenciaService ocorrenciaService;
    private final UserService userService;
    private final StorageService storageService;
    private final FileService fileService;
    private final PermissaoService permissaoService;
    private final TalhaoService talhaoService;

    @Autowired
    public OcorrenciaController(
            StorageService storageService,
            OcorrenciaService ocorrenciaService,
            FileService fileService,
            PermissaoService permissaoService,
            TalhaoService talhaoService,
            UserService userService
    ) {
        this.ocorrenciaService = ocorrenciaService;
        this.userService = userService;
        this.storageService = storageService;
        this.fileService = fileService;
        this.permissaoService = permissaoService;
        this.talhaoService = talhaoService;
    }

    @PostMapping(value = "/ocorrencias")
    public ResponseEntity<?> save(
            @RequestParam("files") List<MultipartFile> files,
            @RequestParam int nivelCritico,
            @RequestParam Long permissao,
            @RequestParam Long talhao,
            @RequestParam String texto,
            @RequestParam String location
    ) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Set<UserFile> fotos = new HashSet<>();
        for (MultipartFile file : files) {
            String filePath = storageService.store(file, "ocorrencias");
            UserFile f = new UserFile();
            f.setCreatedBy(user);
            f.setFileName(file.getOriginalFilename());
            f.setPath(filePath);
            fileService.save(f);
            fotos.add(f);
        }
        Ocorrencia ocorrencia = new Ocorrencia();
        if (!location.isEmpty()) {
            ocorrencia.setLocation(location);
        }
        ocorrencia.setNivelCritico(NivelCritico.values()[nivelCritico]);
        ocorrencia.setPermissao(permissaoService.findById(permissao));
        ocorrencia.setTalhao(talhaoService.findById(talhao));
        ocorrencia.setTexto(texto);
        ocorrencia.setCreatedBy(user);
        ocorrencia.setFotos(fotos);
        ocorrencia = ocorrenciaService.save(ocorrencia);
        URI urlLocation = fromCurrentRequest().path("/{id}")
                .buildAndExpand(ocorrencia.getId()).toUri();
        return created(urlLocation).build();
    }

    @PutMapping(value = "/ocorrencias/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Ocorrencia ocorrencia) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Ocorrencia ocorrenciaDB = ocorrenciaService.findById(id);
        ocorrenciaDB.setUpdatedBy(user);
        ocorrenciaDB.setAtivo(ocorrencia.isAtivo());
        ocorrenciaDB.setLocation(ocorrenciaDB.getLocation());
        ocorrenciaDB.setNivelCritico(ocorrenciaDB.getNivelCritico());
        ocorrenciaDB.setPermissao(ocorrenciaDB.getPermissao());
        ocorrenciaDB.setTalhao(ocorrenciaDB.getTalhao());
        ocorrenciaDB.setTexto(ocorrenciaDB.getTexto());
        ocorrenciaService.save(ocorrenciaDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/ocorrencias")
    public @ResponseBody
    ResponseEntity<List<OcorrenciaDTO>> getAll() {
        List<OcorrenciaDTO> ocorrencias = ocorrenciaService.findAll();
        return new ResponseEntity<>(ocorrencias, OK);
    }

    @GetMapping(value = "/ocorrencias/{id}")
    public @ResponseBody
    ResponseEntity<OcorrenciaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find ocorrencia
        OcorrenciaDTO ocorrencia = ocorrenciaService.findByIdDTO(id);
        return new ResponseEntity<>(ocorrencia, OK);
    }

    @GetMapping("/ocorrencias/foto/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename, HttpServletRequest request) throws Exception {
        // Load file as Resource
        Resource resource = storageService.loadAsResource("ocorrencias/"+filename);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            throw new Exception("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @PatchMapping(value = "/ocorrencias/{id}/concluido")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody Ocorrencia ocorrencia) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Ocorrencia ocorrenciaBD = ocorrenciaService.findById(id);
        ocorrenciaBD.setConcluido(true);
        ocorrenciaService.update(ocorrenciaBD, user);
        return ResponseEntity.ok("Ocorrência concluída.");
    }

}
