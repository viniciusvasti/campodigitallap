/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.OrdemDeCorteDTO;
import com.vas.campodigital.models.OrdemDeCorte;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.OrdemDeCorteService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class OrdemDeCorteController {

    private final OrdemDeCorteService ordemDeCorteService;
    private final UserService userService;

    @Autowired
    public OrdemDeCorteController(
            OrdemDeCorteService ordemDeCorteService,
            UserService userService
    ) {
        this.ordemDeCorteService = ordemDeCorteService;
        this.userService = userService;
    }

    @PostMapping(value = "/ordens-corte")
    public ResponseEntity<?> save(@RequestBody OrdemDeCorte ordemDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ordemDeCorte.setCreatedBy(user);
        ordemDeCorte = ordemDeCorteService.save(ordemDeCorte);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(ordemDeCorte.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/ordens-corte")
    public @ResponseBody ResponseEntity<List<OrdemDeCorteDTO>> getAll() {
        List<OrdemDeCorteDTO> ordemDeCortes = ordemDeCorteService.findAll();
        return new ResponseEntity<>(ordemDeCortes, OK);
    }
    
    @GetMapping(value = "ordens-corte/{id}")
    public @ResponseBody ResponseEntity<OrdemDeCorteDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find ordemDeCorte
        OrdemDeCorteDTO ordemDeCorte = ordemDeCorteService.findByIdDTO(id);
        return new ResponseEntity<>(ordemDeCorte, OK);
    }

    @PatchMapping(value = "/ordens-corte/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody OrdemDeCorte ordemDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        OrdemDeCorte ordemDeCorteBD = ordemDeCorteService.findById(id);
        ordemDeCorteBD.setUsuarioDigitouPims(user);
        ordemDeCorteBD.setDigitadoPims(ordemDeCorte.isDigitadoPims());
        ordemDeCorteService.update(ordemDeCorteBD, user);
        return ResponseEntity.ok(ordemDeCorte.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/ordens-corte/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody OrdemDeCorte ordemDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        OrdemDeCorte ordemDeCorteBD = ordemDeCorteService.findById(id);
        ordemDeCorteBD.setFichaEntregue(ordemDeCorte.isFichaEntregue());
        ordemDeCorteService.update(ordemDeCorteBD, user);
        return ResponseEntity.ok(ordemDeCorte.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
