/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.EquipamentoDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.Equipamento;
import com.vas.campodigital.services.EquipamentoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class EquipamentoController {

    private final EquipamentoService equipamentoService;
    private final UserService userService;

    @Autowired
    public EquipamentoController(
            EquipamentoService equipamentoService,
            UserService userService
    ) {
        this.equipamentoService = equipamentoService;
        this.userService = userService;
    }

    @PostMapping(value = "/equipamentos")
    public ResponseEntity<?> save(@RequestBody Equipamento equipamento) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        equipamento.setCreatedBy(user);
        equipamento = equipamentoService.save(equipamento);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(equipamento.getId()).toUri();
        return created(location).build();
    }

//    @PutMapping(value = "/equipamentos/{id}")
//    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Equipamento equipamento) throws Exception {
//        String username = (String) getContext().getAuthentication().getPrincipal();
//        User user = userService.findUserByUsername(username);
//        Equipamento equipamentoDB = equipamentoService.findByIdDTO(id);
//        equipamentoDB.setUpdatedBy(user);
//        equipamentoDB.setAtivo(equipamento.isAtivo());
//        equipamentoDB.setDescricao(equipamento.getDescricao());
//        equipamentoDB.setNumero(equipamento.getNumero());
//        equipamentoService.save(equipamentoDB);
//        URI location = fromCurrentRequest().path("")
//                .buildAndExpand("").toUri();
//        return created(location).build();
//    }

    
    @GetMapping(value = "/equipamentos")
    public @ResponseBody
    ResponseEntity<List<EquipamentoDTO>> getAll() {
        List<EquipamentoDTO> equipamentos = equipamentoService.findAll();
        return new ResponseEntity<>(equipamentos, OK);
    }
    
    // TODO retornar apenas equipamentos da filial do usuário
    @GetMapping(value = "/equipamentos/to-escala")
    public @ResponseBody
    ResponseEntity<List<EquipamentoDTO>> getAllSemEscala() {
        List<EquipamentoDTO> equipamentos = equipamentoService.findAllNoEscala();
        return new ResponseEntity<>(equipamentos, OK);
    }

    @GetMapping(value = "equipamentos/{id}")
    public @ResponseBody
    ResponseEntity<EquipamentoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find equipamento
        EquipamentoDTO equipamento = equipamentoService.findByIdDTO(id);
        return new ResponseEntity<>(equipamento, OK);
    }

}
