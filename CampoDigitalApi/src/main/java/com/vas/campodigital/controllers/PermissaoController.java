package com.vas.campodigital.controllers;

import com.vas.campodigital.models.secondary.Permissao;
import com.vas.campodigital.services.PermissaoService;
import com.vas.campodigital.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Vinicius
 */
@RestController
public class PermissaoController {

    private final PermissaoService permissaoService;

    @Autowired
    public PermissaoController(
            PermissaoService permissaoService,
            UserService userService
    ) {
        this.permissaoService = permissaoService;
    }

    @GetMapping(value = "/permissoes")
    public @ResponseBody
    ResponseEntity<List<Permissao>> getAll() {
        List<Permissao> permissaos = permissaoService.findAll();
        return new ResponseEntity<>(permissaos, OK);
    }

    @GetMapping(value = "permissoes/{id}")
    public @ResponseBody
    ResponseEntity<Permissao> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find permissao
        Permissao permissao = permissaoService.findById(id);
        return new ResponseEntity<>(permissao, OK);
    }

}
