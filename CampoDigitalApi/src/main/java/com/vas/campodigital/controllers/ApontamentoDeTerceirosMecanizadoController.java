/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeTerceirosMecanizadoDTO;
import com.vas.campodigital.models.ApontamentoDeTerceirosMecanizado;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeTerceirosMecanizadoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeTerceirosMecanizadoController {

    private final ApontamentoDeTerceirosMecanizadoService apontamentoDeTerceirosMecanizadoService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeTerceirosMecanizadoController(
            ApontamentoDeTerceirosMecanizadoService apontamentoDeTerceirosMecanizadoService,
            UserService userService
    ) {
        this.apontamentoDeTerceirosMecanizadoService = apontamentoDeTerceirosMecanizadoService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-terceiros-mecanizado")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizado) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeTerceirosMecanizado.setCreatedBy(user);
        apontamentoDeTerceirosMecanizado = apontamentoDeTerceirosMecanizadoService.save(apontamentoDeTerceirosMecanizado);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeTerceirosMecanizado.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-terceiros-mecanizado")
    public @ResponseBody ResponseEntity<List<ApontamentoDeTerceirosMecanizadoDTO>> getAll() {
        List<ApontamentoDeTerceirosMecanizadoDTO> apontamentoDeTerceirosMecanizados = apontamentoDeTerceirosMecanizadoService.findAll();
        return new ResponseEntity<>(apontamentoDeTerceirosMecanizados, OK);
    }
    
    @GetMapping(value = "apontamentos-terceiros-mecanizado/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeTerceirosMecanizadoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeTerceirosMecanizado
        ApontamentoDeTerceirosMecanizadoDTO apontamentoDeTerceirosMecanizado = apontamentoDeTerceirosMecanizadoService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeTerceirosMecanizado, OK);
    }

    @PatchMapping(value = "/apontamentos-terceiros-mecanizado/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizado) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizadoBD = apontamentoDeTerceirosMecanizadoService.findById(id);
        apontamentoDeTerceirosMecanizadoBD.setUsuarioDigitouPims(user);
        apontamentoDeTerceirosMecanizadoBD.setDigitadoPims(apontamentoDeTerceirosMecanizado.isDigitadoPims());
        apontamentoDeTerceirosMecanizadoService.update(apontamentoDeTerceirosMecanizadoBD, user);
        return ResponseEntity.ok(apontamentoDeTerceirosMecanizado.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-terceiros-mecanizado/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizado) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizadoBD = apontamentoDeTerceirosMecanizadoService.findById(id);
        apontamentoDeTerceirosMecanizadoBD.setFichaEntregue(apontamentoDeTerceirosMecanizado.isFichaEntregue());
        apontamentoDeTerceirosMecanizadoService.update(apontamentoDeTerceirosMecanizadoBD, user);
        return ResponseEntity.ok(apontamentoDeTerceirosMecanizado.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
