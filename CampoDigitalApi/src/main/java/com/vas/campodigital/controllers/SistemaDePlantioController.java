/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.SistemaDePlantioDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.SistemaDePlantio;
import com.vas.campodigital.services.SistemaDePlantioService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class SistemaDePlantioController {

    private final SistemaDePlantioService sistemaDePlantioService;
    private final UserService userService;

    @Autowired
    public SistemaDePlantioController(
            SistemaDePlantioService sistemaDePlantioService,
            UserService userService
    ) {
        this.sistemaDePlantioService = sistemaDePlantioService;
        this.userService = userService;
    }

    @PostMapping(value = "/sistemas-plantio")
    public ResponseEntity<?> save(@RequestBody SistemaDePlantio sistemaDePlantio) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        sistemaDePlantio.setCreatedBy(user);
        sistemaDePlantio = sistemaDePlantioService.save(sistemaDePlantio);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(sistemaDePlantio.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/sistemas-plantio/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody SistemaDePlantio sistemaDePlantio) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        SistemaDePlantio sistemaDePlantioDB = sistemaDePlantioService.findById(id);
        sistemaDePlantioDB.setUpdatedBy(user);
        sistemaDePlantioDB.setAtivo(sistemaDePlantio.isAtivo());
        sistemaDePlantioDB.setDescricao(sistemaDePlantio.getDescricao());
        sistemaDePlantioDB.setNumero(sistemaDePlantio.getNumero());
        sistemaDePlantioService.save(sistemaDePlantioDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/sistemas-plantio")
    public @ResponseBody ResponseEntity<List<SistemaDePlantioDTO>> getAll() {
        List<SistemaDePlantioDTO> sistemaDePlantios = sistemaDePlantioService.findAll();
        return new ResponseEntity<>(sistemaDePlantios, OK);
    }
    
    @GetMapping(value = "/sistemas-plantio/{id}")
    public @ResponseBody ResponseEntity<SistemaDePlantioDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find sistemaDePlantio
        SistemaDePlantioDTO sistemaDePlantio = sistemaDePlantioService.findByIdDTO(id);
        return new ResponseEntity<>(sistemaDePlantio, OK);
    }

}
