/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.SistemaDeCorteDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.SistemaDeCorte;
import com.vas.campodigital.services.SistemaDeCorteService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class SistemaDeCorteController {

    private final SistemaDeCorteService sistemaDeCorteService;
    private final UserService userService;

    @Autowired
    public SistemaDeCorteController(
            SistemaDeCorteService sistemaDeCorteService,
            UserService userService
    ) {
        this.sistemaDeCorteService = sistemaDeCorteService;
        this.userService = userService;
    }

    @PostMapping(value = "/sistemas-corte")
    public ResponseEntity<?> save(@RequestBody SistemaDeCorte sistemaDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        sistemaDeCorte.setCreatedBy(user);
        sistemaDeCorte = sistemaDeCorteService.save(sistemaDeCorte);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(sistemaDeCorte.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/sistemas-corte/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody SistemaDeCorte sistemaDeCorte) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        SistemaDeCorte sistemaDeCorteDB = sistemaDeCorteService.findById(id);
        sistemaDeCorteDB.setUpdatedBy(user);
        sistemaDeCorteDB.setAtivo(sistemaDeCorte.isAtivo());
        sistemaDeCorteDB.setDescricao(sistemaDeCorte.getDescricao());
        sistemaDeCorteDB.setNumero(sistemaDeCorte.getNumero());
        sistemaDeCorteService.save(sistemaDeCorteDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/sistemas-corte")
    public @ResponseBody ResponseEntity<List<SistemaDeCorteDTO>> getAll() {
        List<SistemaDeCorteDTO> sistemaDeCortes = sistemaDeCorteService.findAll();
        return new ResponseEntity<>(sistemaDeCortes, OK);
    }
    
    @GetMapping(value = "/sistemas-corte/{id}")
    public @ResponseBody ResponseEntity<SistemaDeCorteDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find sistemaDeCorte
        SistemaDeCorteDTO sistemaDeCorte = sistemaDeCorteService.findByIdDTO(id);
        return new ResponseEntity<>(sistemaDeCorte, OK);
    }

}
