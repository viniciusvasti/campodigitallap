/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeLubrificanteDTO;
import com.vas.campodigital.models.ApontamentoDeLubrificante;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeLubrificanteService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeLubrificanteController {

    private final ApontamentoDeLubrificanteService apontamentoDeLubrificanteService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeLubrificanteController(
            ApontamentoDeLubrificanteService apontamentoDeLubrificanteService,
            UserService userService
    ) {
        this.apontamentoDeLubrificanteService = apontamentoDeLubrificanteService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-lubrificante")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeLubrificante apontamentoDeLubrificante) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeLubrificante.setCreatedBy(user);
        apontamentoDeLubrificante = apontamentoDeLubrificanteService.save(apontamentoDeLubrificante);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeLubrificante.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-lubrificante")
    public @ResponseBody ResponseEntity<List<ApontamentoDeLubrificanteDTO>> getAll() {
        List<ApontamentoDeLubrificanteDTO> apontamentoDeLubrificantes = apontamentoDeLubrificanteService.findAll();
        return new ResponseEntity<>(apontamentoDeLubrificantes, OK);
    }
    
    @GetMapping(value = "apontamentos-lubrificante/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeLubrificanteDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeLubrificante
        ApontamentoDeLubrificanteDTO apontamentoDeLubrificante = apontamentoDeLubrificanteService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeLubrificante, OK);
    }

    @PatchMapping(value = "/apontamentos-lubrificante/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDeLubrificante apontamentoDeLubrificante) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeLubrificante apontamentoDeLubrificanteBD = apontamentoDeLubrificanteService.findById(id);
        apontamentoDeLubrificanteBD.setUsuarioDigitouPims(user);
        apontamentoDeLubrificanteBD.setDigitadoPims(apontamentoDeLubrificante.isDigitadoPims());
        apontamentoDeLubrificanteService.update(apontamentoDeLubrificanteBD, user);
        return ResponseEntity.ok(apontamentoDeLubrificante.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-lubrificante/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeLubrificante apontamentoDeLubrificante) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeLubrificante apontamentoDeLubrificanteBD = apontamentoDeLubrificanteService.findById(id);
        apontamentoDeLubrificanteBD.setFichaEntregue(apontamentoDeLubrificante.isFichaEntregue());
        apontamentoDeLubrificanteService.update(apontamentoDeLubrificanteBD, user);
        return ResponseEntity.ok(apontamentoDeLubrificante.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
