/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.EmpresaDTO;
import com.vas.campodigital.models.Empresa;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.EmpresaService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class EmpresaController {

    private final EmpresaService empresaService;
    private final UserService userService;

    @Autowired
    public EmpresaController(
            EmpresaService empresaService,
            UserService userService
    ) {
        this.empresaService = empresaService;
        this.userService = userService;
    }

    @PostMapping(value = "/empresas")
    public ResponseEntity<?> save(@RequestBody Empresa empresa) {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        empresa.setCreatedBy(user);
        empresa = empresaService.save(empresa);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(empresa.getId()).toUri();
        return created(location).build();
    }

    @CrossOrigin
    @PutMapping(value = "/empresas/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Empresa empresa) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Empresa empresaDB = empresaService.findById(id);
        empresaDB.setUpdatedBy(user);
        empresaDB.setAtivo(empresa.isAtivo());
        empresaDB.setCnpj(empresa.getCnpj());
        empresaDB.setNomeFantasia(empresa.getNomeFantasia());
        empresaDB.setRazaoSocial(empresa.getRazaoSocial());
        empresaService.save(empresaDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/empresas")
    public @ResponseBody
    ResponseEntity<List<EmpresaDTO>> getAll() {
        List<EmpresaDTO> empresas = empresaService.findAll();
        return new ResponseEntity<>(empresas, OK);
    }

    @GetMapping(value = "empresas/{id}")
    public @ResponseBody
    ResponseEntity<EmpresaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find empresa
        EmpresaDTO empresa = empresaService.findByIdDTO(id);
        return new ResponseEntity<>(empresa, OK);
    }

}
