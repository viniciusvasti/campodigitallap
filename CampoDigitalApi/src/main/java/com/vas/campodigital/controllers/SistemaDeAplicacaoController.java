/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.SistemaDeAplicacaoDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.SistemaDeAplicacao;
import com.vas.campodigital.services.SistemaDeAplicacaoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class SistemaDeAplicacaoController {

    private final SistemaDeAplicacaoService sistemaDeAplicacaoService;
    private final UserService userService;

    @Autowired
    public SistemaDeAplicacaoController(
            SistemaDeAplicacaoService sistemaDeAplicacaoService,
            UserService userService
    ) {
        this.sistemaDeAplicacaoService = sistemaDeAplicacaoService;
        this.userService = userService;
    }

    @PostMapping(value = "/sistemas-aplicacao")
    public ResponseEntity<?> save(@RequestBody SistemaDeAplicacao sistemaDeAplicacao) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        sistemaDeAplicacao.setCreatedBy(user);
        sistemaDeAplicacao = sistemaDeAplicacaoService.save(sistemaDeAplicacao);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(sistemaDeAplicacao.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/sistemas-aplicacao/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody SistemaDeAplicacao sistemaDeAplicacao) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        SistemaDeAplicacao sistemaDeAplicacaoDB = sistemaDeAplicacaoService.findById(id);
        sistemaDeAplicacaoDB.setUpdatedBy(user);
        sistemaDeAplicacaoDB.setAtivo(sistemaDeAplicacao.isAtivo());
        sistemaDeAplicacaoDB.setDescricao(sistemaDeAplicacao.getDescricao());
        sistemaDeAplicacaoDB.setNumero(sistemaDeAplicacao.getNumero());
        sistemaDeAplicacaoService.save(sistemaDeAplicacaoDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/sistemas-aplicacao")
    public @ResponseBody ResponseEntity<List<SistemaDeAplicacaoDTO>> getAll() {
        List<SistemaDeAplicacaoDTO> sistemaDeAplicacaos = sistemaDeAplicacaoService.findAll();
        return new ResponseEntity<>(sistemaDeAplicacaos, OK);
    }
    
    @GetMapping(value = "/sistemas-aplicacao/{id}")
    public @ResponseBody ResponseEntity<SistemaDeAplicacaoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find sistemaDeAplicacao
        SistemaDeAplicacaoDTO sistemaDeAplicacao = sistemaDeAplicacaoService.findByIdDTO(id);
        return new ResponseEntity<>(sistemaDeAplicacao, OK);
    }

}
