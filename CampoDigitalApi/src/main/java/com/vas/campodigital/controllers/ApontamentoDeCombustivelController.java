/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeCombustivelDTO;
import com.vas.campodigital.models.ApontamentoDeCombustivel;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeCombustivelService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeCombustivelController {

    private final ApontamentoDeCombustivelService apontamentoDeCombustivelService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeCombustivelController(
            ApontamentoDeCombustivelService apontamentoDeCombustivelService,
            UserService userService
    ) {
        this.apontamentoDeCombustivelService = apontamentoDeCombustivelService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-combustivel")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeCombustivel apontamentoDeCombustivel) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeCombustivel.setCreatedBy(user);
        apontamentoDeCombustivel = apontamentoDeCombustivelService.save(apontamentoDeCombustivel);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeCombustivel.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-combustivel")
    public @ResponseBody ResponseEntity<List<ApontamentoDeCombustivelDTO>> getAll() {
        List<ApontamentoDeCombustivelDTO> apontamentoDeCombustivels = apontamentoDeCombustivelService.findAll();
        return new ResponseEntity<>(apontamentoDeCombustivels, OK);
    }
    
    @GetMapping(value = "apontamentos-combustivel/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeCombustivelDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeCombustivel
        ApontamentoDeCombustivelDTO apontamentoDeCombustivel = apontamentoDeCombustivelService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeCombustivel, OK);
    }

    @PatchMapping(value = "/apontamentos-combustivel/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDeCombustivel apontamentoDeCombustivel) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeCombustivel apontamentoDeCombustivelBD = apontamentoDeCombustivelService.findById(id);
        apontamentoDeCombustivelBD.setUsuarioDigitouPims(user);
        apontamentoDeCombustivelBD.setDigitadoPims(apontamentoDeCombustivel.isDigitadoPims());
        apontamentoDeCombustivelService.update(apontamentoDeCombustivelBD, user);
        return ResponseEntity.ok(apontamentoDeCombustivel.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-combustivel/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeCombustivel apontamentoDeCombustivel) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeCombustivel apontamentoDeCombustivelBD = apontamentoDeCombustivelService.findById(id);
        apontamentoDeCombustivelBD.setFichaEntregue(apontamentoDeCombustivel.isFichaEntregue());
        apontamentoDeCombustivelService.update(apontamentoDeCombustivelBD, user);
        return ResponseEntity.ok(apontamentoDeCombustivel.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
