/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.BloqueioCadastroDTO;
import com.vas.campodigital.models.BloqueioCadastro;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.BloqueioCadastroService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class BloqueioCadastroController {

    private final BloqueioCadastroService bloqueioCadastroService;
    private final UserService userService;

    @Autowired
    public BloqueioCadastroController(
            BloqueioCadastroService bloqueioCadastroService,
            UserService userService
    ) {
        this.bloqueioCadastroService = bloqueioCadastroService;
        this.userService = userService;
    }

    @PostMapping(value = "/bloqueios-cadastro")
    public ResponseEntity<?> save(@RequestBody BloqueioCadastro bloqueioCadastro) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        bloqueioCadastro.setCreatedBy(user);
        bloqueioCadastro = bloqueioCadastroService.save(bloqueioCadastro);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(bloqueioCadastro.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/bloqueios-cadastro")
    public @ResponseBody ResponseEntity<List<BloqueioCadastroDTO>> getAll() {
        List<BloqueioCadastroDTO> bloqueioCadastros = bloqueioCadastroService.findAll();
        return new ResponseEntity<>(bloqueioCadastros, OK);
    }
    
    @GetMapping(value = "bloqueios-cadastro/{id}")
    public @ResponseBody ResponseEntity<BloqueioCadastroDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find bloqueioCadastro
        BloqueioCadastroDTO bloqueioCadastro = bloqueioCadastroService.findByIdDTO(id);
        return new ResponseEntity<>(bloqueioCadastro, OK);
    }

}
