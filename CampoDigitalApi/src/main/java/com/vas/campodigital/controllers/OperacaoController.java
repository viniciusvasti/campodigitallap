package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.OperacaoDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.secondary.Operacao;
import com.vas.campodigital.services.OperacaoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class OperacaoController {

    private final OperacaoService operacaoService;
    private final UserService userService;

    @Autowired
    public OperacaoController(
            OperacaoService operacaoService,
            UserService userService
    ) {
        this.operacaoService = operacaoService;
        this.userService = userService;
    }

    @PostMapping(value = "/operacaos")
    public ResponseEntity<?> save(@RequestBody Operacao operacao) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        operacao.setCreatedBy(user);
        operacao = operacaoService.save(operacao);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(operacao.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/operacaos/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Operacao operacao) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Operacao operacaoDB = operacaoService.findById(id);
        operacaoDB.setUpdatedBy(user);
        operacaoDB.setAtivo(operacao.isAtivo());
        operacaoDB.setDescricao(operacao.getDescricao());
        operacaoDB.setNumero(operacao.getNumero());
        operacaoService.save(operacaoDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/operacaos")
    public @ResponseBody ResponseEntity<List<OperacaoDTO>> getAll() {
        List<OperacaoDTO> operacaos = operacaoService.findAll();
        return new ResponseEntity<>(operacaos, OK);
    }
    
    @GetMapping(value = "/operacaos/{id}")
    public @ResponseBody ResponseEntity<OperacaoDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find operacao
        OperacaoDTO operacao = operacaoService.findByIdDTO(id);
        return new ResponseEntity<>(operacao, OK);
    }

}
