/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.FazendaDTO;
import com.vas.campodigital.models.Fazenda;
import com.vas.campodigital.models.Filial;
import com.vas.campodigital.models.Secao;
import com.vas.campodigital.models.Talhao;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.FazendaService;
import com.vas.campodigital.services.SecaoService;
import com.vas.campodigital.services.TalhaoService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class FazendaController {

    private final FazendaService fazendaService;
    private final SecaoService secaoService;
    private final TalhaoService talhaoService;
    private final UserService userService;

    @Autowired
    public FazendaController(
            FazendaService fazendaService,
            SecaoService secaoService,
            TalhaoService talhaoService,
            UserService userService
    ) {
        this.fazendaService = fazendaService;
        this.secaoService = secaoService;
        this.talhaoService = talhaoService;
        this.userService = userService;
    }

    @PostMapping(value = "/fazendas")
    public ResponseEntity<?> save(@RequestBody Fazenda fazenda) {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        fazenda.setCreatedBy(user);
        fazenda = fazendaService.save(fazenda);
        for (Secao secao : fazenda.getSecoes()) {
            secao.setFazenda(fazenda);
            secao.setCreatedBy(user);
            secao.setAtivo(fazenda.isAtivo());
            secaoService.save(secao);
            for (Talhao talhao : secao.getTalhoes()) {
                talhao.setSecao(secao);
                talhao.setCreatedBy(user);
                talhao.setAtivo(fazenda.isAtivo());
                talhaoService.save(talhao);
            }
        }
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(fazenda.getId()).toUri();
        return created(location).build();
    }

    @PutMapping(value = "/fazendas/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Fazenda fazendaNew) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        Fazenda fazendaDB = fazendaService.findById(id);
        Secao secaoDB;
        Talhao talhaoDB;
        Filial filial = new Filial();
        filial.setId(fazendaNew.getFilial().getId());
        fazendaDB.setFilial(filial);
        fazendaDB.setUpdatedBy(user);
        fazendaDB.setAtivo(fazendaNew.isAtivo());
        for (Secao secao : fazendaDB.getSecoes()) {
            secaoDB = secaoService.findById(secao.getId());
            secaoDB.setAtivo(fazendaNew.getSecoes().contains(secao));
            secaoDB.setUpdatedBy(user);
            secaoService.save(secaoDB);
            for (Talhao talhao : secao.getTalhoes()) {
                talhaoDB = talhaoService.findById(talhao.getId());
                Secao secaoNew;
                for (Iterator<Secao> it = fazendaNew.getSecoes().iterator(); it.hasNext();) {
                    secaoNew = it.next();
                    if (secao.equals(secaoNew)) {
                        talhao.setAtivo(secaoNew.getTalhoes().contains(talhao));
                        talhaoDB.setUpdatedBy(user);
                        talhaoService.save(talhaoDB);
                    }
                }
            }
        }

        for (Secao secao : fazendaNew.getSecoes()) {
            if (!fazendaDB.getSecoes().contains(secao)) {
                secao.setCreatedBy(user);
                secao.setFazenda(fazendaDB);
                secaoService.save(secao);
            }
            for (Talhao talhao : secao.getTalhoes()) {
                talhao.setCreatedBy(user);
                talhao.setSecao(secao);
                talhaoService.save(talhao);
            }
        }
        fazendaDB.setNome(fazendaNew.getNome());
        fazendaDB.setNumero(fazendaNew.getNumero());
        fazendaDB.setSecoes(fazendaNew.getSecoes());
        fazendaService.save(fazendaDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

    @GetMapping(value = "/fazendas")
    public @ResponseBody
    ResponseEntity<List<FazendaDTO>> getAll() {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        List<FazendaDTO> fazendas = fazendaService.findAll(user);
        return new ResponseEntity<>(fazendas, OK);
    }

    @GetMapping(value = "fazendas/{id}")
    public @ResponseBody
    ResponseEntity<FazendaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find fazenda
        FazendaDTO fazenda = fazendaService.findByIdDTO(id);
        return new ResponseEntity<>(fazenda, OK);
    }

}
