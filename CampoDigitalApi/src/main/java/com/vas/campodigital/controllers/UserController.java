/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.UserDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.UserService;
import com.vas.campodigital.exceptions.UserAlreadyExistsException;
import com.vas.campodigital.mappers.Mapper;
import com.vas.campodigital.models.Funcionario;
import com.vas.campodigital.services.FuncionarioService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinícius
 */
@RestController
public class UserController {

    private final UserService userService;
    private final FuncionarioService funcionarioService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserController(UserService userService,
            FuncionarioService funcionarioService,
            BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.funcionarioService = funcionarioService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping(value = "/users")
    public ResponseEntity<?> register(@RequestBody User userNew) throws Exception {
        if (this.userService.findUserByUsername(userNew.getUsername()) != null) {
            throw new UserAlreadyExistsException(userNew.getUsername());
        }
        String username = (String) getContext().getAuthentication().getPrincipal();
        User userCreating = userService.findUserByUsername(username);
        Funcionario funcionario = userNew.getFuncionario();
        userNew.setPassword(this.bCryptPasswordEncoder.encode(userNew.getPassword()));
        userNew.setCreatedBy(userCreating);
        userNew.setFuncionario(null);
        this.userService.save(userNew);
        if (funcionario != null) {
            funcionario.setCreatedBy(userCreating);
            funcionario.setUser(userNew);
            funcionario.setAtivo(userNew.isAtivo());
            this.funcionarioService.save(funcionario);
        }
        return status(CREATED).build();
    }

    @PutMapping(value = "/users/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody User newUser) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User userUpdating = userService.findUserByUsername(username);
        Funcionario funcionario = newUser.getFuncionario();
        User userDB = userService.findById(id);
        Funcionario funcionarioDB = userDB.getFuncionario();
        userDB.setUpdatedBy(userUpdating);
        userDB.setAtivo(newUser.isAtivo());
        userDB.setName(newUser.getName());
        userDB.setUsername(newUser.getUsername());
        userDB.setPermissoes(newUser.getPermissoes());
        if (funcionario != null) {
            if (funcionarioDB == null) {
                funcionarioDB = new Funcionario();
            }
            funcionarioDB.setCreatedBy(newUser);
            funcionarioDB.setUser(newUser);
            funcionarioDB.setAtivo(newUser.isAtivo());
            funcionarioDB.setCargo(funcionario.getCargo());
            funcionarioDB.setFilial(funcionario.getFilial());
            funcionarioDB.setMatricula(funcionario.getMatricula());
        } else {
            if (funcionarioDB != null) {
                funcionarioDB.setAtivo(false);
            }
        }
        if (funcionarioDB != null) {
            funcionarioService.save(funcionarioDB);
            userDB.setFuncionario(funcionarioDB);
        }
        userService.save(userDB);
        URI location = fromCurrentRequest().path("")
                .buildAndExpand("").toUri();
        return created(location).build();
    }

//    @GetMapping(value = "/users")
//    public ResponseEntity<UserDTO> userByToken(@RequestHeader String authorization) throws Exception {
//        String username = (String) getContext().getAuthentication().getPrincipal();
//        UserDTO userDto = userService.findUserDTOByUsername(username);
//        return new ResponseEntity<>(userDto, OK);
//    }
    @GetMapping(value = "/users")
    public @ResponseBody
    ResponseEntity<List<UserDTO>> getAll() {
        List<UserDTO> users = userService.findAll();
        return new ResponseEntity<>(users, OK);
    }

    @GetMapping(value = "users/{id}")
    public @ResponseBody
    ResponseEntity<UserDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find filial
        UserDTO user = Mapper.mapUserToDto(userService.findById(id));
        return new ResponseEntity<>(user, OK);
    }

}
