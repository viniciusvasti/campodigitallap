/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoDeAtividadeMecanizadaDTO;
import com.vas.campodigital.models.ApontamentoDeAtividadeMecanizada;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoDeAtividadeMecanizadaService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoDeAtividadeMecanizadaController {

    private final ApontamentoDeAtividadeMecanizadaService apontamentoDeAtividadeMecanizadaService;
    private final UserService userService;

    @Autowired
    public ApontamentoDeAtividadeMecanizadaController(
            ApontamentoDeAtividadeMecanizadaService apontamentoDeAtividadeMecanizadaService,
            UserService userService
    ) {
        this.apontamentoDeAtividadeMecanizadaService = apontamentoDeAtividadeMecanizadaService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-atividade-mecanizada")
    public ResponseEntity<?> save(@RequestBody ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizada) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoDeAtividadeMecanizada.setCreatedBy(user);
        apontamentoDeAtividadeMecanizada = apontamentoDeAtividadeMecanizadaService.save(apontamentoDeAtividadeMecanizada);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoDeAtividadeMecanizada.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-atividade-mecanizada")
    public @ResponseBody ResponseEntity<List<ApontamentoDeAtividadeMecanizadaDTO>> getAll() {
        List<ApontamentoDeAtividadeMecanizadaDTO> apontamentoDeAtividadeMecanizadas = apontamentoDeAtividadeMecanizadaService.findAll();
        return new ResponseEntity<>(apontamentoDeAtividadeMecanizadas, OK);
    }
    
    @GetMapping(value = "apontamentos-atividade-mecanizada/{id}")
    public @ResponseBody ResponseEntity<ApontamentoDeAtividadeMecanizadaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoDeAtividadeMecanizada
        ApontamentoDeAtividadeMecanizadaDTO apontamentoDeAtividadeMecanizada = apontamentoDeAtividadeMecanizadaService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoDeAtividadeMecanizada, OK);
    }

    @PatchMapping(value = "/apontamentos-atividade-mecanizada/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizada) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizadaBD = apontamentoDeAtividadeMecanizadaService.findById(id);
        apontamentoDeAtividadeMecanizadaBD.setUsuarioDigitouPims(user);
        apontamentoDeAtividadeMecanizadaBD.setDigitadoPims(apontamentoDeAtividadeMecanizada.isDigitadoPims());
        apontamentoDeAtividadeMecanizadaService.update(apontamentoDeAtividadeMecanizadaBD, user);
        return ResponseEntity.ok(apontamentoDeAtividadeMecanizada.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-atividade-mecanizada/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizada) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizadaBD = apontamentoDeAtividadeMecanizadaService.findById(id);
        apontamentoDeAtividadeMecanizadaBD.setFichaEntregue(apontamentoDeAtividadeMecanizada.isFichaEntregue());
        apontamentoDeAtividadeMecanizadaService.update(apontamentoDeAtividadeMecanizadaBD, user);
        return ResponseEntity.ok(apontamentoDeAtividadeMecanizada.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
