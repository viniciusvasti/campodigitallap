/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.controllers;

import com.vas.campodigital.dtos.ApontamentoMoagemDiariaDTO;
import com.vas.campodigital.models.ApontamentoMoagemDiaria;
import com.vas.campodigital.models.User;
import com.vas.campodigital.services.ApontamentoMoagemDiariaService;
import com.vas.campodigital.services.UserService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 *
 * @author Vinicius
 */
@RestController
public class ApontamentoMoagemDiariaController {

    private final ApontamentoMoagemDiariaService apontamentoMoagemDiariaService;
    private final UserService userService;

    @Autowired
    public ApontamentoMoagemDiariaController(
            ApontamentoMoagemDiariaService apontamentoMoagemDiariaService,
            UserService userService
    ) {
        this.apontamentoMoagemDiariaService = apontamentoMoagemDiariaService;
        this.userService = userService;
    }

    @PostMapping(value = "/apontamentos-moagem-diaria")
    public ResponseEntity<?> save(@RequestBody ApontamentoMoagemDiaria apontamentoMoagemDiaria) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        apontamentoMoagemDiaria.setCreatedBy(user);
        apontamentoMoagemDiaria = apontamentoMoagemDiariaService.save(apontamentoMoagemDiaria);
        URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(apontamentoMoagemDiaria.getId()).toUri();
        return created(location).build();
    }

    @GetMapping(value = "/apontamentos-moagem-diaria")
    public @ResponseBody ResponseEntity<List<ApontamentoMoagemDiariaDTO>> getAll() {
        List<ApontamentoMoagemDiariaDTO> apontamentoMoagemDiarias = apontamentoMoagemDiariaService.findAll();
        return new ResponseEntity<>(apontamentoMoagemDiarias, OK);
    }
    
    @GetMapping(value = "apontamentos-moagem-diaria/{id}")
    public @ResponseBody ResponseEntity<ApontamentoMoagemDiariaDTO> get(@PathVariable Long id) {
        // TODO throw an exception if it can't find apontamentoMoagemDiaria
        ApontamentoMoagemDiariaDTO apontamentoMoagemDiaria = apontamentoMoagemDiariaService.findByIdDTO(id);
        return new ResponseEntity<>(apontamentoMoagemDiaria, OK);
    }

    @PatchMapping(value = "/apontamentos-moagem-diaria/{id}/pims")
    public ResponseEntity<?> updatePims(
            @PathVariable Long id,
            @RequestBody ApontamentoMoagemDiaria apontamentoMoagemDiaria) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoMoagemDiaria apontamentoMoagemDiariaBD = apontamentoMoagemDiariaService.findById(id);
        apontamentoMoagemDiariaBD.setUsuarioDigitouPims(user);
        apontamentoMoagemDiariaBD.setDigitadoPims(apontamentoMoagemDiaria.isDigitadoPims());
        apontamentoMoagemDiariaService.update(apontamentoMoagemDiariaBD, user);
        return ResponseEntity.ok(apontamentoMoagemDiaria.isDigitadoPims() ? "Digitação do PIMs confirmada" : "Digitação do PIMs desfeita");
    }

    @PatchMapping(value = "/apontamentos-moagem-diaria/{id}/ficha-entregue")
    public ResponseEntity<?> updateFicha(
            @PathVariable Long id,
            @RequestBody ApontamentoMoagemDiaria apontamentoMoagemDiaria) throws Exception {
        String username = (String) getContext().getAuthentication().getPrincipal();
        User user = userService.findUserByUsername(username);
        ApontamentoMoagemDiaria apontamentoMoagemDiariaBD = apontamentoMoagemDiariaService.findById(id);
        apontamentoMoagemDiariaBD.setFichaEntregue(apontamentoMoagemDiaria.isFichaEntregue());
        apontamentoMoagemDiariaService.update(apontamentoMoagemDiariaBD, user);
        return ResponseEntity.ok(apontamentoMoagemDiaria.isFichaEntregue() ? "Entrega da ficha confirmada" : "Entrega da ficha desfeita");
    }

}
