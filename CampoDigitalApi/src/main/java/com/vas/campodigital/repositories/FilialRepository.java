package com.vas.campodigital.repositories;

import com.vas.campodigital.models.Filial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(exported = false)
public interface FilialRepository extends JpaRepository<Filial, Long> {

}
