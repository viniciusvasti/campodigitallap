package com.vas.campodigital.repositories;

import com.vas.campodigital.models.secondary.SistemaDeCorte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "sistema-de-corte", path = "sistema-de-corte", exported = false)
public interface SistemaDeCorteRepository extends JpaRepository<SistemaDeCorte, Long> {
    
}
