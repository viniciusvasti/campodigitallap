package com.vas.campodigital.repositories;

import com.vas.campodigital.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "user", path = "user", exported = false)
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.permissoes WHERE LOWER(u.username) = LOWER(:username)")
    public Optional<User> findUserByUsername(@Param("username") String username);

}
