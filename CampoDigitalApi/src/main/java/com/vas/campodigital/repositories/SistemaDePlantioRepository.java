package com.vas.campodigital.repositories;

import com.vas.campodigital.models.secondary.SistemaDePlantio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "sistema-de-plantio", path = "sistema-de-plantio", exported = false)
public interface SistemaDePlantioRepository extends JpaRepository<SistemaDePlantio, Long> {
    
}
