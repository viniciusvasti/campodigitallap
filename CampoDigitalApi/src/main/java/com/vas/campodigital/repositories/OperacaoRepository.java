package com.vas.campodigital.repositories;

import com.vas.campodigital.models.secondary.Operacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "operacao", path = "operacao", exported = false)
public interface OperacaoRepository extends JpaRepository<Operacao, Long> {
    
}
