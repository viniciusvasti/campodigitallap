package com.vas.campodigital.repositories;

import com.vas.campodigital.models.ApontamentoChuva;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(exported = false)
public interface ApontamentoChuvaRepository extends JpaRepository<ApontamentoChuva, Long> {
    
}
