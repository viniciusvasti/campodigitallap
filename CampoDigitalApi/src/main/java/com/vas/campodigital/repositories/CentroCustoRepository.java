package com.vas.campodigital.repositories;

import com.vas.campodigital.models.secondary.CentroCusto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "centro-custo", path = "centro-custo", exported = false)
public interface CentroCustoRepository extends JpaRepository<CentroCusto, Long> {
    
}
