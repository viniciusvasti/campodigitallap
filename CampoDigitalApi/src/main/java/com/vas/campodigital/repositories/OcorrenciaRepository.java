package com.vas.campodigital.repositories;

import com.vas.campodigital.models.Ocorrencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "ocorrencia", path = "ocorrencia", exported = false)
public interface OcorrenciaRepository extends JpaRepository<Ocorrencia, Long> {
    
}
