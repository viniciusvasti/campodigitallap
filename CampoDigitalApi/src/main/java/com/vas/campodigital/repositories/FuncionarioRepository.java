package com.vas.campodigital.repositories;

import com.vas.campodigital.models.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "funcionario", path = "funcionario", exported = false)
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

}
