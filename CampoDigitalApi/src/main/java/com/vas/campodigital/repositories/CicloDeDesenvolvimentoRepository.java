package com.vas.campodigital.repositories;

import com.vas.campodigital.models.secondary.CicloDeDesenvolvimento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "ciclo-de-desenvolvimento", path = "ciclo-de-desenvolvimento", exported = false)
public interface CicloDeDesenvolvimentoRepository extends JpaRepository<CicloDeDesenvolvimento, Long> {
    
}
