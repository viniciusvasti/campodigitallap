package com.vas.campodigital.repositories;

import com.vas.campodigital.models.OrdemDeCorte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(exported = false)
public interface OrdemDeCorteRepository extends JpaRepository<OrdemDeCorte, Long> {
    
}
