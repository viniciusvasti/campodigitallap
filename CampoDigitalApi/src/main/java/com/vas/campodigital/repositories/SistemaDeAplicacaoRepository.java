package com.vas.campodigital.repositories;

import com.vas.campodigital.models.secondary.SistemaDeAplicacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(collectionResourceRel = "sistema-de-aplicacao", path = "sistema-de-aplicacao", exported = false)
public interface SistemaDeAplicacaoRepository extends JpaRepository<SistemaDeAplicacao, Long> {
    
}
