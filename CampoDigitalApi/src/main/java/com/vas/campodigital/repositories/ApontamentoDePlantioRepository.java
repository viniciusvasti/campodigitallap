package com.vas.campodigital.repositories;

import com.vas.campodigital.models.ApontamentoDePlantio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Vinicius
 */
@RepositoryRestResource(exported = false)
public interface ApontamentoDePlantioRepository extends JpaRepository<ApontamentoDePlantio, Long> {
    
}
