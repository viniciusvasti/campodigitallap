/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.FechamentoDeCorteDTO;
import com.vas.campodigital.models.FechamentoDeCorte;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.FechamentoDeCorteRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class FechamentoDeCorteService {

    private final FechamentoDeCorteRepository fechamentoDeCorteRepository;

    @Autowired
    public FechamentoDeCorteService(FechamentoDeCorteRepository fechamentoDeCorteRepository) {
        this.fechamentoDeCorteRepository = fechamentoDeCorteRepository;
    }

    public FechamentoDeCorte save(FechamentoDeCorte fechamentoDeCorte) throws Exception {
        // TODO tratar exceção corretamente
        if (fechamentoDeCorte.getId() == null && fechamentoDeCorte.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (fechamentoDeCorte.getId() != null && fechamentoDeCorte.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return fechamentoDeCorteRepository.save(fechamentoDeCorte);
    }

    public FechamentoDeCorte update(FechamentoDeCorte fechamentoDeCorte, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (fechamentoDeCorte.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return fechamentoDeCorteRepository.save(fechamentoDeCorte);
    }

    public List<FechamentoDeCorteDTO> findAll() {
        List<FechamentoDeCorteDTO> dtos = new ArrayList<>();
        List<FechamentoDeCorte> users = fechamentoDeCorteRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapFechamentoDeCorteToDto(u))
        );
        
        return dtos;
    }

    public FechamentoDeCorteDTO findByIdDTO(Long id) {
        return Mapper.mapFechamentoDeCorteToDto(fechamentoDeCorteRepository.findById(id).orElse(null));
    }

    public FechamentoDeCorte findById(Long id) {
        return fechamentoDeCorteRepository.findById(id).orElse(null);
    }
    
}
