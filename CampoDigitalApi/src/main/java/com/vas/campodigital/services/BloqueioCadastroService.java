/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.BloqueioCadastroDTO;
import com.vas.campodigital.models.BloqueioCadastro;
import com.vas.campodigital.repositories.BloqueioCadastroRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class BloqueioCadastroService {

    private final BloqueioCadastroRepository bloqueioCadastroRepository;

    @Autowired
    public BloqueioCadastroService(BloqueioCadastroRepository bloqueioCadastroRepository) {
        this.bloqueioCadastroRepository = bloqueioCadastroRepository;
    }

    public BloqueioCadastro save(BloqueioCadastro bloqueioCadastro) throws Exception {
        return bloqueioCadastroRepository.save(bloqueioCadastro);
    }

    public List<BloqueioCadastroDTO> findAll() {
        List<BloqueioCadastroDTO> dtos = new ArrayList<>();
        List<BloqueioCadastro> users = bloqueioCadastroRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapBloqueioCadastroToDto(u))
        );
        
        return dtos;
    }

    public BloqueioCadastroDTO findByIdDTO(Long id) {
        return Mapper.mapBloqueioCadastroToDto(bloqueioCadastroRepository.findById(id).orElse(null));
    }

    public BloqueioCadastro findById(Long id) {
        return bloqueioCadastroRepository.findById(id).orElse(null);
    }
    
}
