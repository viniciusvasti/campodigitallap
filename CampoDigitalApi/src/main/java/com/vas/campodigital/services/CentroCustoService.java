/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.CentroCustoDTO;
import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.repositories.CentroCustoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class CentroCustoService {

    private final CentroCustoRepository centroCustoRepository;

    @Autowired
    public CentroCustoService(CentroCustoRepository centroCustoRepository) {
        this.centroCustoRepository = centroCustoRepository;
    }

    public CentroCusto save(CentroCusto centroCusto) throws Exception {
        return centroCustoRepository.save(centroCusto);
    }

    public List<CentroCustoDTO> findAll() {
        List<CentroCustoDTO> dtos = new ArrayList<>();
        List<CentroCusto> users = centroCustoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapCentroCustoToDto(u))
        );
        
        return dtos;
    }

    public CentroCustoDTO findByIdDTO(Long id) {
        return Mapper.mapCentroCustoToDto(centroCustoRepository.findById(id).orElse(null));
    }

    public CentroCusto findById(Long id) {
        return centroCustoRepository.findById(id).orElse(null);
    }
    
}
