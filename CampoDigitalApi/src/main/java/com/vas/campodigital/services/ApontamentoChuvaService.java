/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoChuvaDTO;
import com.vas.campodigital.models.ApontamentoChuva;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoChuvaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoChuvaService {

    private final ApontamentoChuvaRepository apontamentoChuvaRepository;

    @Autowired
    public ApontamentoChuvaService(ApontamentoChuvaRepository apontamentoChuvaRepository) {
        this.apontamentoChuvaRepository = apontamentoChuvaRepository;
    }

    public ApontamentoChuva save(ApontamentoChuva apontamentoChuva) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoChuva.getId() == null && apontamentoChuva.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoChuva.getId() != null && apontamentoChuva.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        apontamentoChuva.setFilial(apontamentoChuva.getCreatedBy().getFuncionario().getFilial());
        return apontamentoChuvaRepository.save(apontamentoChuva);
    }

    public ApontamentoChuva update(ApontamentoChuva apontamentoChuva, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoChuva.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoChuvaRepository.save(apontamentoChuva);
    }

    public List<ApontamentoChuvaDTO> findAll() {
        List<ApontamentoChuvaDTO> dtos = new ArrayList<>();
        List<ApontamentoChuva> users = apontamentoChuvaRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoChuvaToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoChuvaDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoChuvaToDto(apontamentoChuvaRepository.findById(id).orElse(null));
    }

    public ApontamentoChuva findById(Long id) {
        return apontamentoChuvaRepository.findById(id).orElse(null);
    }
    
}
