/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.EquipamentoDTO;
import com.vas.campodigital.dtos.EscalaDTO;
import com.vas.campodigital.models.secondary.Equipamento;
import com.vas.campodigital.repositories.EquipamentoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class EquipamentoService {

    private final EquipamentoRepository equipamentoRepository;
    private final EscalaService escalaService;

    @Autowired
    public EquipamentoService(
            EquipamentoRepository equipamentoRepository,
            EscalaService escalaService
    ) {
        this.equipamentoRepository = equipamentoRepository;
        this.escalaService = escalaService;
    }

    public Equipamento save(Equipamento equipamento) throws Exception {
        // TODO tratar exceção corretamente
        if (equipamento.getId() == null && equipamento.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (equipamento.getId() != null && equipamento.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return equipamentoRepository.save(equipamento);
    }

    public List<EquipamentoDTO> findAll() {
        List<EquipamentoDTO> dtos = new ArrayList<>();
        List<Equipamento> users = equipamentoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapEquipamentoToDto(u))
        );
        
        return dtos;
    }

    // TODO refatorar
    public List<EquipamentoDTO> findAllNoEscala() {
        List<EquipamentoDTO> dtos = new ArrayList<>();
        ArrayList<Equipamento> equipamentos = (ArrayList<Equipamento>) equipamentoRepository.findAll();
        for (EscalaDTO escala : escalaService.findAllDTO()) {
            if (escala.isAtivo()) {
                equipamentos.removeAll(escala.getEquipamentos());
            }
        }
        equipamentos.forEach(
                (u) -> dtos.add(Mapper.mapEquipamentoToDto(u))
        );
        return dtos;
    }

    public EquipamentoDTO findByIdDTO(Long id) {
        return Mapper.mapEquipamentoToDto(equipamentoRepository.findById(id).orElse(null));
    }

    public Equipamento findById(Long id) {
        return equipamentoRepository.findById(id).orElse(null);
    }
    
}
