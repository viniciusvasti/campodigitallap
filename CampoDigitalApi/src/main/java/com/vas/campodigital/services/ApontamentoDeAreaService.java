/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeAreaDTO;
import com.vas.campodigital.models.ApontamentoDeArea;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeAreaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeAreaService {

    private final ApontamentoDeAreaRepository apontamentoDeAreaRepository;

    @Autowired
    public ApontamentoDeAreaService(ApontamentoDeAreaRepository apontamentoDeAreaRepository) {
        this.apontamentoDeAreaRepository = apontamentoDeAreaRepository;
    }

    public ApontamentoDeArea save(ApontamentoDeArea apontamentoDeArea) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeArea.getId() == null && apontamentoDeArea.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeArea.getId() != null && apontamentoDeArea.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAreaRepository.save(apontamentoDeArea);
    }

    public ApontamentoDeArea update(ApontamentoDeArea apontamentoDeArea, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeArea.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAreaRepository.save(apontamentoDeArea);
    }

    public List<ApontamentoDeAreaDTO> findAll() {
        List<ApontamentoDeAreaDTO> dtos = new ArrayList<>();
        List<ApontamentoDeArea> users = apontamentoDeAreaRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeAreaToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDeAreaDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeAreaToDto(apontamentoDeAreaRepository.findById(id).orElse(null));
    }

    public ApontamentoDeArea findById(Long id) {
        return apontamentoDeAreaRepository.findById(id).orElse(null);
    }
    
}
