/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeAtividadesManuaisDTO;
import com.vas.campodigital.models.ApontamentoDeAtividadesManuais;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeAtividadesManuaisRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeAtividadesManuaisService {

    private final ApontamentoDeAtividadesManuaisRepository apontamentoDeAtividadesManuaisRepository;

    @Autowired
    public ApontamentoDeAtividadesManuaisService(ApontamentoDeAtividadesManuaisRepository apontamentoDeAtividadesManuaisRepository) {
        this.apontamentoDeAtividadesManuaisRepository = apontamentoDeAtividadesManuaisRepository;
    }

    public ApontamentoDeAtividadesManuais save(ApontamentoDeAtividadesManuais apontamentoDeAtividadesManuais) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeAtividadesManuais.getId() == null && apontamentoDeAtividadesManuais.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeAtividadesManuais.getId() != null && apontamentoDeAtividadesManuais.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAtividadesManuaisRepository.save(apontamentoDeAtividadesManuais);
    }

    public ApontamentoDeAtividadesManuais update(ApontamentoDeAtividadesManuais apontamentoDeAtividadesManuais, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeAtividadesManuais.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAtividadesManuaisRepository.save(apontamentoDeAtividadesManuais);
    }

    public List<ApontamentoDeAtividadesManuaisDTO> findAll() {
        List<ApontamentoDeAtividadesManuaisDTO> dtos = new ArrayList<>();
        List<ApontamentoDeAtividadesManuais> users = apontamentoDeAtividadesManuaisRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeAtividadesManuaisToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDeAtividadesManuaisDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeAtividadesManuaisToDto(apontamentoDeAtividadesManuaisRepository.findById(id).orElse(null));
    }

    public ApontamentoDeAtividadesManuais findById(Long id) {
        return apontamentoDeAtividadesManuaisRepository.findById(id).orElse(null);
    }
    
}
