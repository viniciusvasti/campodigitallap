/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeCombustivelDTO;
import com.vas.campodigital.models.ApontamentoDeCombustivel;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeCombustivelRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeCombustivelService {

    private final ApontamentoDeCombustivelRepository apontamentoDeCombustivelRepository;

    @Autowired
    public ApontamentoDeCombustivelService(ApontamentoDeCombustivelRepository apontamentoDeCombustivelRepository) {
        this.apontamentoDeCombustivelRepository = apontamentoDeCombustivelRepository;
    }

    public ApontamentoDeCombustivel save(ApontamentoDeCombustivel apontamentoDeCombustivel) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeCombustivel.getId() == null && apontamentoDeCombustivel.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeCombustivel.getId() != null && apontamentoDeCombustivel.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeCombustivelRepository.save(apontamentoDeCombustivel);
    }

    public ApontamentoDeCombustivel update(ApontamentoDeCombustivel apontamentoDeCombustivel, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeCombustivel.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeCombustivelRepository.save(apontamentoDeCombustivel);
    }

    public List<ApontamentoDeCombustivelDTO> findAll() {
        List<ApontamentoDeCombustivelDTO> dtos = new ArrayList<>();
        List<ApontamentoDeCombustivel> users = apontamentoDeCombustivelRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeCombustivelToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDeCombustivelDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeCombustivelToDto(apontamentoDeCombustivelRepository.findById(id).orElse(null));
    }

    public ApontamentoDeCombustivel findById(Long id) {
        return apontamentoDeCombustivelRepository.findById(id).orElse(null);
    }
    
}
