/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.OrdemDeServicoDTO;
import com.vas.campodigital.models.OrdemDeServico;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.OrdemDeServicoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class OrdemDeServicoService {

    private final OrdemDeServicoRepository ordemDeServicoRepository;

    @Autowired
    public OrdemDeServicoService(OrdemDeServicoRepository ordemDeServicoRepository) {
        this.ordemDeServicoRepository = ordemDeServicoRepository;
    }

    public OrdemDeServico save(OrdemDeServico ordemDeServico) throws Exception {
        // TODO tratar exceção corretamente
        if (ordemDeServico.getId() == null && ordemDeServico.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (ordemDeServico.getId() != null && ordemDeServico.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return ordemDeServicoRepository.save(ordemDeServico);
    }

    public OrdemDeServico update(OrdemDeServico ordemDeServico, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (ordemDeServico.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return ordemDeServicoRepository.save(ordemDeServico);
    }

    public List<OrdemDeServicoDTO> findAll() {
        List<OrdemDeServicoDTO> dtos = new ArrayList<>();
        List<OrdemDeServico> users = ordemDeServicoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapOrdemDeServicoToDto(u))
        );
        
        return dtos;
    }

    public OrdemDeServicoDTO findByIdDTO(Long id) {
        return Mapper.mapOrdemDeServicoToDto(ordemDeServicoRepository.findById(id).orElse(null));
    }

    public OrdemDeServico findById(Long id) {
        return ordemDeServicoRepository.findById(id).orElse(null);
    }
    
}
