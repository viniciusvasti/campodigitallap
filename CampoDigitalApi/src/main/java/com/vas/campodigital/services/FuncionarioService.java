/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.FuncionarioDTO;
import com.vas.campodigital.models.Funcionario;
import com.vas.campodigital.repositories.FuncionarioRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class FuncionarioService {

    private final FuncionarioRepository funcionarioRepository;

    @Autowired
    public FuncionarioService(FuncionarioRepository funcionarioRepository) {
        this.funcionarioRepository = funcionarioRepository;
    }

    public Funcionario save(Funcionario funcionario) {
        return funcionarioRepository.save(funcionario);
    }

    public List<FuncionarioDTO> findAll() {
        List<FuncionarioDTO> dtos = new ArrayList<>();
        List<Funcionario> users = funcionarioRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapFuncionarioToDto(u))
        );
        
        return dtos;
    }

    public FuncionarioDTO findByIdDTO(Long id) {
        return Mapper.mapFuncionarioToDto(funcionarioRepository.findById(id).orElse(null));
    }

    public Funcionario findById(Long id) {
        return funcionarioRepository.findById(id).orElse(null);
    }
    
}
