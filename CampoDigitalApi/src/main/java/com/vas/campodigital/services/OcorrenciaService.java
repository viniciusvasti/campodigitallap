/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.OcorrenciaDTO;
import com.vas.campodigital.mappers.Mapper;
import com.vas.campodigital.models.Ocorrencia;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.OcorrenciaRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class OcorrenciaService {

    private final OcorrenciaRepository ocorrenciaRepository;

    @Autowired
    public OcorrenciaService(OcorrenciaRepository ocorrenciaRepository) {
        this.ocorrenciaRepository = ocorrenciaRepository;
    }

    public Ocorrencia save(Ocorrencia ocorrencia) throws Exception {
        return ocorrenciaRepository.save(ocorrencia);
    }

    public List<OcorrenciaDTO> findAll() {
        List<OcorrenciaDTO> dtos = new ArrayList<>();
        List<Ocorrencia> users = ocorrenciaRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapOcorrenciaToDto(u))
        );

        return dtos;
    }

    public Ocorrencia findById(Long id) {
        return ocorrenciaRepository.findById(id).orElse(null);
    }

    public OcorrenciaDTO findByIdDTO(Long id) {
        return Mapper.mapOcorrenciaToDto(ocorrenciaRepository.findById(id).orElse(null));
    }

    public Ocorrencia update(Ocorrencia ocorrencia, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (ocorrencia.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return ocorrenciaRepository.save(ocorrencia);
    }

}
