/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.models.secondary.Permissao;
import com.vas.campodigital.repositories.PermissaoRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class PermissaoService {

    private final PermissaoRepository permissaoRepository;

    @Autowired
    public PermissaoService(PermissaoRepository permissaoRepository) {
        this.permissaoRepository = permissaoRepository;
    }

    public Permissao save(Permissao permissao) throws Exception {
        return permissaoRepository.save(permissao);
    }

    public List<Permissao> findAll() {
        return permissaoRepository.findAll();
    }

    public Permissao findById(Long id) {
        return permissaoRepository.findById(id).orElse(null);
    }

    public List<String> endpointsPermissao() {
        return findAll().stream().map(p -> p.getEndpoint()).collect(Collectors.toList());
    }

}
