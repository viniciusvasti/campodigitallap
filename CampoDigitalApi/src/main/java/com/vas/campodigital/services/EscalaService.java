/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.EscalaDTO;
import com.vas.campodigital.models.Escala;
import com.vas.campodigital.repositories.EscalaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class EscalaService {

    private final EscalaRepository escalaRepository;

    @Autowired
    public EscalaService(EscalaRepository escalaRepository) {
        this.escalaRepository = escalaRepository;
    }

    public Escala save(Escala escala) throws Exception {
        // TODO tratar exceção corretamente
        if (escala.getId() == null && escala.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (escala.getId() != null && escala.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return escalaRepository.save(escala);
    }

    public List<EscalaDTO> findAllDTO() {
        List<Escala> escalas = escalaRepository.findAll();
        List<EscalaDTO> escalaDTOs = new ArrayList<>();
        escalas.forEach((u) -> {
            escalaDTOs.add(Mapper.mapEscalaToDto(u));
        });

        return escalaDTOs;
    }

    public EscalaDTO findByIdDTO(Long id) {
        return Mapper.mapEscalaToDto(escalaRepository.findById(id).orElse(null));
    }

    public Escala findById(Long id) {
        return escalaRepository.findById(id).orElse(null);
    }

}
