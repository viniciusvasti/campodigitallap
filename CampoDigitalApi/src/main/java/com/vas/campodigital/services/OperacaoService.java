/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.OperacaoDTO;
import com.vas.campodigital.models.secondary.Operacao;
import com.vas.campodigital.repositories.OperacaoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class OperacaoService {

    private final OperacaoRepository operacaoRepository;

    @Autowired
    public OperacaoService(OperacaoRepository operacaoRepository) {
        this.operacaoRepository = operacaoRepository;
    }

    public Operacao save(Operacao operacao) throws Exception {
        return operacaoRepository.save(operacao);
    }

    public List<OperacaoDTO> findAll() {
        List<OperacaoDTO> dtos = new ArrayList<>();
        List<Operacao> users = operacaoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapOperacaoToDto(u))
        );
        
        return dtos;
    }

    public OperacaoDTO findByIdDTO(Long id) {
        return Mapper.mapOperacaoToDto(operacaoRepository.findById(id).orElse(null));
    }

    public Operacao findById(Long id) {
        return operacaoRepository.findById(id).orElse(null);
    }
    
}
