/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeTerceirosMecanizadoDTO;
import com.vas.campodigital.models.ApontamentoDeTerceirosMecanizado;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeTerceirosMecanizadoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeTerceirosMecanizadoService {

    private final ApontamentoDeTerceirosMecanizadoRepository apontamentoDeTerceirosMecanizadoRepository;

    @Autowired
    public ApontamentoDeTerceirosMecanizadoService(ApontamentoDeTerceirosMecanizadoRepository apontamentoDeTerceirosMecanizadoRepository) {
        this.apontamentoDeTerceirosMecanizadoRepository = apontamentoDeTerceirosMecanizadoRepository;
    }

    public ApontamentoDeTerceirosMecanizado save(ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizado) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeTerceirosMecanizado.getId() == null && apontamentoDeTerceirosMecanizado.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeTerceirosMecanizado.getId() != null && apontamentoDeTerceirosMecanizado.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeTerceirosMecanizadoRepository.save(apontamentoDeTerceirosMecanizado);
    }

    public ApontamentoDeTerceirosMecanizado update(ApontamentoDeTerceirosMecanizado apontamentoDeTerceirosMecanizado, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeTerceirosMecanizado.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeTerceirosMecanizadoRepository.save(apontamentoDeTerceirosMecanizado);
    }

    public List<ApontamentoDeTerceirosMecanizadoDTO> findAll() {
        List<ApontamentoDeTerceirosMecanizadoDTO> dtos = new ArrayList<>();
        List<ApontamentoDeTerceirosMecanizado> users = apontamentoDeTerceirosMecanizadoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeTerceirosMecanizadoToDto(u))
        );

        return dtos;
    }

    public ApontamentoDeTerceirosMecanizadoDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeTerceirosMecanizadoToDto(apontamentoDeTerceirosMecanizadoRepository.findById(id).orElse(null));
    }

    public ApontamentoDeTerceirosMecanizado findById(Long id) {
        return apontamentoDeTerceirosMecanizadoRepository.findById(id).orElse(null);
    }

}
