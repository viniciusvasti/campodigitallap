/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.models.User;
import com.vas.campodigital.models.Activity;
import com.vas.campodigital.repositories.ActivityRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import static org.springframework.data.domain.Sort.Direction.DESC;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ActivityService {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Activity save(Activity activity) {
        if (activity.getId() == null) { // new activity (user logged in)
            Activity firstActivity = this.findFirst();
            if (firstActivity != null) {
                long total = firstActivity.getTotalVisitors();
                activity.setTotalVisitors(++total);
                firstActivity.setTotalVisitors(total);
                this.activityRepository.save(firstActivity);
            }
        }
        return activityRepository.save(activity);
    }

    public Activity findFirst() {
        return this.activityRepository.findFirstBy();
    }

    public Activity findLast(User user) {
        return activityRepository.findFirstByUserOrderByIdDesc(user);
    }

    public Page<Activity> findByUser(User user, int page, int size) {
        return this.activityRepository.findByUser(user, new PageRequest(page, size, DESC, "id"));
    }

    public Activity findOne(long id) {
        return this.activityRepository.findById(id).orElse(null);
    }

    public List<Activity> findAll() {
        return this.activityRepository.findAll();
    }

    public void delete(Long id) {
        this.activityRepository.deleteById(id);
    }

}
