/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.AplicacaoDeInsumoDTO;
import com.vas.campodigital.models.AplicacaoDeInsumo;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.AplicacaoDeInsumoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class AplicacaoDeInsumoService {

    private final AplicacaoDeInsumoRepository aplicacaoDeInsumoRepository;

    @Autowired
    public AplicacaoDeInsumoService(AplicacaoDeInsumoRepository aplicacaoDeInsumoRepository) {
        this.aplicacaoDeInsumoRepository = aplicacaoDeInsumoRepository;
    }

    public AplicacaoDeInsumo save(AplicacaoDeInsumo aplicacaoDeInsumo) throws Exception {
        // TODO tratar exceção corretamente
        if (aplicacaoDeInsumo.getId() == null && aplicacaoDeInsumo.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (aplicacaoDeInsumo.getId() != null && aplicacaoDeInsumo.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return aplicacaoDeInsumoRepository.save(aplicacaoDeInsumo);
    }

    public AplicacaoDeInsumo update(AplicacaoDeInsumo aplicacaoDeInsumo, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (aplicacaoDeInsumo.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return aplicacaoDeInsumoRepository.save(aplicacaoDeInsumo);
    }

    public List<AplicacaoDeInsumoDTO> findAll() {
        List<AplicacaoDeInsumoDTO> dtos = new ArrayList<>();
        List<AplicacaoDeInsumo> users = aplicacaoDeInsumoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapAplicacaoDeInsumoToDto(u))
        );
        
        return dtos;
    }

    public AplicacaoDeInsumoDTO findByIdDTO(Long id) {
        return Mapper.mapAplicacaoDeInsumoToDto(aplicacaoDeInsumoRepository.findById(id).orElse(null));
    }

    public AplicacaoDeInsumo findById(Long id) {
        return aplicacaoDeInsumoRepository.findById(id).orElse(null);
    }
    
}
