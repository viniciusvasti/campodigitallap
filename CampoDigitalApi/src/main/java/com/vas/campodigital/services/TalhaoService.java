/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.TalhaoDTO;
import com.vas.campodigital.dtos.TalhaoPlainDTO;
import com.vas.campodigital.mappers.Mapper;
import com.vas.campodigital.models.Talhao;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.TalhaoRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class TalhaoService {

    private final TalhaoRepository talhaoRepository;

    @Autowired
    public TalhaoService(TalhaoRepository talhaoRepository) {
        this.talhaoRepository = talhaoRepository;
    }

    public Talhao save(Talhao talhao) {
        return talhaoRepository.save(talhao);
    }

    public TalhaoDTO findByIdDTO(Long id) {
        return Mapper.mapTalhaoToDto(talhaoRepository.findById(id).orElse(null));
    }
    public Talhao findById(Long id) {
        return talhaoRepository.findById(id).orElse(null);
    }

    public List<TalhaoPlainDTO> findAllWithMapFile(User user) {
        List<Talhao> talhoes = talhaoRepository.findAll();
        List<TalhaoPlainDTO> dtos = new ArrayList<>(talhoes.size());   

        if (user.getFuncionario() != null) {
            for (Talhao talhao : new ArrayList<>(talhoes)) {
                if (talhao.getMapaPDF() == null
                        || !talhao
                            .getSecao()
                            .getFazenda()
                            .getFilial().equals(
                                    user.getFuncionario().getFilial()
                            )) {
                    talhoes.remove(talhao);
                }
            }
        } else {
            for (Talhao talhao : new ArrayList<>(talhoes)) {
                if (talhao.getMapaPDF() == null) {
                    talhoes.remove(talhao);
                }
            }
        }
        talhoes.forEach(
                (u) -> dtos.add(Mapper.mapTalhaoToPlainDto(u))
        );
        
        return dtos;
    }

}
