/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.CicloDeDesenvolvimentoDTO;
import com.vas.campodigital.models.secondary.CicloDeDesenvolvimento;
import com.vas.campodigital.repositories.CicloDeDesenvolvimentoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class CicloDeDesenvolvimentoService {

    private final CicloDeDesenvolvimentoRepository cicloDeDesenvolvimentoRepository;

    @Autowired
    public CicloDeDesenvolvimentoService(CicloDeDesenvolvimentoRepository cicloDeDesenvolvimentoRepository) {
        this.cicloDeDesenvolvimentoRepository = cicloDeDesenvolvimentoRepository;
    }

    public CicloDeDesenvolvimento save(CicloDeDesenvolvimento cicloDeDesenvolvimento) throws Exception {
        // TODO tratar exceção corretamente
        if (cicloDeDesenvolvimento.getId() == null && cicloDeDesenvolvimento.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (cicloDeDesenvolvimento.getId() != null && cicloDeDesenvolvimento.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return cicloDeDesenvolvimentoRepository.save(cicloDeDesenvolvimento);
    }

    public List<CicloDeDesenvolvimentoDTO> findAll() {
        List<CicloDeDesenvolvimentoDTO> dtos = new ArrayList<>();
        List<CicloDeDesenvolvimento> users = cicloDeDesenvolvimentoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapCicloDeDesenvolvimentoToDto(u))
        );
        
        return dtos;
    }

    public CicloDeDesenvolvimentoDTO findByIdDTO(Long id) {
        return Mapper.mapCicloDeDesenvolvimentoToDto(cicloDeDesenvolvimentoRepository.findById(id).orElse(null));
    }

    public CicloDeDesenvolvimento findById(Long id) {
        return cicloDeDesenvolvimentoRepository.findById(id).orElse(null);
    }
    
}
