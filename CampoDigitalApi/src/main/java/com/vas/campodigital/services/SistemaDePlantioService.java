/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.SistemaDePlantioDTO;
import com.vas.campodigital.models.secondary.SistemaDePlantio;
import com.vas.campodigital.repositories.SistemaDePlantioRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class SistemaDePlantioService {

    private final SistemaDePlantioRepository sistemaDePlantioRepository;

    @Autowired
    public SistemaDePlantioService(SistemaDePlantioRepository sistemaDePlantioRepository) {
        this.sistemaDePlantioRepository = sistemaDePlantioRepository;
    }

    public SistemaDePlantio save(SistemaDePlantio sistemaDePlantio) {
        return sistemaDePlantioRepository.save(sistemaDePlantio);
    }

    public List<SistemaDePlantioDTO> findAll() {
        List<SistemaDePlantioDTO> dtos = new ArrayList<>();
        List<SistemaDePlantio> users = sistemaDePlantioRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapSistemaDePlantioToDto(u))
        );
        
        return dtos;
    }

    public SistemaDePlantioDTO findByIdDTO(Long id) {
        return Mapper.mapSistemaDePlantioToDto(sistemaDePlantioRepository.findById(id).orElse(null));
    }

    public SistemaDePlantio findById(Long id) {
        return sistemaDePlantioRepository.findById(id).orElse(null);
    }
    
}
