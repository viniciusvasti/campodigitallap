/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.FazendaDTO;
import com.vas.campodigital.dtos.UserDTO;
import com.vas.campodigital.models.Fazenda;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.FazendaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class FazendaService {

    private final FazendaRepository fazendaRepository;

    @Autowired
    public FazendaService(FazendaRepository fazendaRepository) {
        this.fazendaRepository = fazendaRepository;
    }

    public Fazenda save(Fazenda fazenda) {
        return fazendaRepository.save(fazenda);
    }

    public List<FazendaDTO> findAll(User user) {
        List<Fazenda> fazendas = fazendaRepository.findAll();
        List<FazendaDTO> dtos = new ArrayList<>(fazendas.size());      
        fazendas.forEach(
                u -> dtos.add(Mapper.mapFazendaToDto(u))
        );
        
        UserDTO userDTO = Mapper.mapUserToDto(user);
        if (userDTO.getFuncionario() != null) {
            dtos.stream().filter(fazenda -> (!fazenda.getFilial().equals(userDTO.getFuncionario().getFilial()))).forEachOrdered((fazenda) -> {
                dtos.remove(fazenda);
            });
        }
        
        return dtos;
    }

    public FazendaDTO findByIdDTO(Long id) {
        return Mapper.mapFazendaToDto(fazendaRepository.findById(id).orElse(null));
    }

    public Fazenda findById(Long id) {
        return fazendaRepository.findById(id).orElse(null);
    }
    
}
