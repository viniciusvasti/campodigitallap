/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.EmpresaDTO;
import com.vas.campodigital.models.Empresa;
import com.vas.campodigital.repositories.EmpresaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class EmpresaService {

    private final EmpresaRepository empresaRepository;

    @Autowired
    public EmpresaService(EmpresaRepository empresaRepository) {
        this.empresaRepository = empresaRepository;
    }

    public Empresa save(Empresa empresa) {
        return empresaRepository.save(empresa);
    }

    public List<EmpresaDTO> findAll() {
        List<EmpresaDTO> dtos = new ArrayList<>();
        List<Empresa> users = empresaRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapEmpresaToDto(u))
        );
        
        return dtos;
    }

    public EmpresaDTO findByIdDTO(Long id) {
        return Mapper.mapEmpresaToDto(empresaRepository.findById(id).orElse(null));
    }
    
    public Empresa findById(Long id) {
        return empresaRepository.findById(id).orElse(null);
    }
    
}
