/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeAreaPreparoDTO;
import com.vas.campodigital.models.ApontamentoDeAreaPreparo;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeAreaPreparoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeAreaPreparoService {

    private final ApontamentoDeAreaPreparoRepository apontamentoDeAreaPreparoRepository;

    @Autowired
    public ApontamentoDeAreaPreparoService(ApontamentoDeAreaPreparoRepository apontamentoDeAreaPreparoRepository) {
        this.apontamentoDeAreaPreparoRepository = apontamentoDeAreaPreparoRepository;
    }

    public ApontamentoDeAreaPreparo save(ApontamentoDeAreaPreparo apontamentoDeAreaPreparo) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeAreaPreparo.getId() == null && apontamentoDeAreaPreparo.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeAreaPreparo.getId() != null && apontamentoDeAreaPreparo.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAreaPreparoRepository.save(apontamentoDeAreaPreparo);
    }

    public ApontamentoDeAreaPreparo update(ApontamentoDeAreaPreparo apontamentoDeAreaPreparo, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeAreaPreparo.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAreaPreparoRepository.save(apontamentoDeAreaPreparo);
    }

    public List<ApontamentoDeAreaPreparoDTO> findAll() {
        List<ApontamentoDeAreaPreparoDTO> dtos = new ArrayList<>();
        List<ApontamentoDeAreaPreparo> users = apontamentoDeAreaPreparoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeAreaPreparoToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDeAreaPreparoDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeAreaPreparoToDto(apontamentoDeAreaPreparoRepository.findById(id).orElse(null));
    }

    public ApontamentoDeAreaPreparo findById(Long id) {
        return apontamentoDeAreaPreparoRepository.findById(id).orElse(null);
    }
    
}
