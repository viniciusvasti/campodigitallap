/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.SistemaDeCorteDTO;
import com.vas.campodigital.models.secondary.SistemaDeCorte;
import com.vas.campodigital.repositories.SistemaDeCorteRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class SistemaDeCorteService {

    private final SistemaDeCorteRepository sistemaDeCorteRepository;

    @Autowired
    public SistemaDeCorteService(SistemaDeCorteRepository sistemaDeCorteRepository) {
        this.sistemaDeCorteRepository = sistemaDeCorteRepository;
    }

    public SistemaDeCorte save(SistemaDeCorte sistemaDeCorte) {
        return sistemaDeCorteRepository.save(sistemaDeCorte);
    }

    public List<SistemaDeCorteDTO> findAll() {
        List<SistemaDeCorteDTO> dtos = new ArrayList<>();
        List<SistemaDeCorte> users = sistemaDeCorteRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapSistemaDeCorteToDto(u))
        );
        
        return dtos;
    }

    public SistemaDeCorteDTO findByIdDTO(Long id) {
        return Mapper.mapSistemaDeCorteToDto(sistemaDeCorteRepository.findById(id).orElse(null));
    }

    public SistemaDeCorte findById(Long id) {
        return sistemaDeCorteRepository.findById(id).orElse(null);
    }
    
}
