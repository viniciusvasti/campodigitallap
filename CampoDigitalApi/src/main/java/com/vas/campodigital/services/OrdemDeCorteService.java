/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.OrdemDeCorteDTO;
import com.vas.campodigital.models.OrdemDeCorte;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.OrdemDeCorteRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class OrdemDeCorteService {

    private final OrdemDeCorteRepository ordemDeCorteRepository;

    @Autowired
    public OrdemDeCorteService(OrdemDeCorteRepository ordemDeCorteRepository) {
        this.ordemDeCorteRepository = ordemDeCorteRepository;
    }

    public OrdemDeCorte save(OrdemDeCorte ordemDeCorte) throws Exception {
        // TODO tratar exceção corretamente
        if (ordemDeCorte.getId() == null && ordemDeCorte.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (ordemDeCorte.getId() != null && ordemDeCorte.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return ordemDeCorteRepository.save(ordemDeCorte);
    }

    public OrdemDeCorte update(OrdemDeCorte ordemDeCorte, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (ordemDeCorte.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return ordemDeCorteRepository.save(ordemDeCorte);
    }

    public List<OrdemDeCorteDTO> findAll() {
        List<OrdemDeCorteDTO> dtos = new ArrayList<>();
        List<OrdemDeCorte> users = ordemDeCorteRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapOrdemDeCorteToDto(u))
        );
        
        return dtos;
    }

    public OrdemDeCorteDTO findByIdDTO(Long id) {
        return Mapper.mapOrdemDeCorteToDto(ordemDeCorteRepository.findById(id).orElse(null));
    }

    public OrdemDeCorte findById(Long id) {
        return ordemDeCorteRepository.findById(id).orElse(null);
    }
    
}
