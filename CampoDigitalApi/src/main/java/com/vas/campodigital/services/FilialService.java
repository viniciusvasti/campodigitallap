/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.FilialDTO;
import com.vas.campodigital.models.Filial;
import com.vas.campodigital.repositories.FilialRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class FilialService {

    private final FilialRepository filialRepository;

    @Autowired
    public FilialService(FilialRepository filialRepository) {
        this.filialRepository = filialRepository;
    }

    public Filial save(Filial filial) {
        return filialRepository.save(filial);
    }

    public List<FilialDTO> findAll() {
        List<FilialDTO> dtos = new ArrayList<>();
        List<Filial> users = filialRepository.findAll();
        users.forEach((u) -> dtos.add(Mapper.mapFilialToDto(u)));
        
        return dtos;
    }

    public FilialDTO findByIdDTO(Long id) {
        return Mapper.mapFilialToDto(filialRepository.findById(id).orElse(null));
    }

    public Filial findById(Long id) {
        return filialRepository.findById(id).orElse(null);
    }
    
}
