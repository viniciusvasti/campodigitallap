/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.UserDTO;
import com.vas.campodigital.models.User;
import com.vas.campodigital.exceptions.UserNotFoundException;
import com.vas.campodigital.mappers.Mapper;
import com.vas.campodigital.repositories.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class UserService {
    
    private final UserRepository userReporitory;

    @Autowired
    public UserService(UserRepository userReporitory) {
        this.userReporitory = userReporitory;
    }

    public UserDTO findUserDTOByUsername(String username) {
        User user = userReporitory.findUserByUsername(username).orElse(null);
        return user != null ? Mapper.mapUserToDto(user) : null;
    }

    public User findUserByUsername(String username) {
        User user = userReporitory.findUserByUsername(username).orElse(null);
        return user;
    }
    
    public User save(User user) {
        return userReporitory.save(user);
    }

    public User findById(Long userId) {
        return userReporitory.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("id", userId.toString()));
    }

    public void deleteAll() {
        userReporitory.deleteAll();
    }

    public List<UserDTO> findAll() {
        List<UserDTO> userDTOs = new ArrayList<>();
        List<User> users = userReporitory.findAll();
        users.forEach(
                (u) -> userDTOs.add(Mapper.mapUserToDto(u))
        );
        
        return userDTOs;
    }
    
}
