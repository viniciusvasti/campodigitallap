/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeAtividadeMecanizadaDTO;
import com.vas.campodigital.models.ApontamentoDeAtividadeMecanizada;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeAtividadeMecanizadaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeAtividadeMecanizadaService {

    private final ApontamentoDeAtividadeMecanizadaRepository apontamentoDeAtividadeMecanizadaRepository;

    @Autowired
    public ApontamentoDeAtividadeMecanizadaService(ApontamentoDeAtividadeMecanizadaRepository apontamentoDeAtividadeMecanizadaRepository) {
        this.apontamentoDeAtividadeMecanizadaRepository = apontamentoDeAtividadeMecanizadaRepository;
    }

    public ApontamentoDeAtividadeMecanizada save(ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizada) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeAtividadeMecanizada.getId() == null && apontamentoDeAtividadeMecanizada.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeAtividadeMecanizada.getId() != null && apontamentoDeAtividadeMecanizada.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAtividadeMecanizadaRepository.save(apontamentoDeAtividadeMecanizada);
    }

    public ApontamentoDeAtividadeMecanizada update(ApontamentoDeAtividadeMecanizada apontamentoDeAtividadeMecanizada, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeAtividadeMecanizada.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeAtividadeMecanizadaRepository.save(apontamentoDeAtividadeMecanizada);
    }

    public List<ApontamentoDeAtividadeMecanizadaDTO> findAll() {
        List<ApontamentoDeAtividadeMecanizadaDTO> dtos = new ArrayList<>();
        List<ApontamentoDeAtividadeMecanizada> users = apontamentoDeAtividadeMecanizadaRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeAtividadeMecanizadaToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDeAtividadeMecanizadaDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeAtividadeMecanizadaToDto(apontamentoDeAtividadeMecanizadaRepository.findById(id).orElse(null));
    }

    public ApontamentoDeAtividadeMecanizada findById(Long id) {
        return apontamentoDeAtividadeMecanizadaRepository.findById(id).orElse(null);
    }
    
}
