/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.InsumoDTO;
import com.vas.campodigital.models.secondary.Insumo;
import com.vas.campodigital.repositories.InsumoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class InsumoService {

    private final InsumoRepository insumoRepository;

    @Autowired
    public InsumoService(InsumoRepository insumoRepository) {
        this.insumoRepository = insumoRepository;
    }

    public Insumo save(Insumo insumo) throws Exception {
        return insumoRepository.save(insumo);
    }

    public List<InsumoDTO> findAll() {
        List<InsumoDTO> dtos = new ArrayList<>();
        List<Insumo> users = insumoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapInsumoToDto(u))
        );
        
        return dtos;
    }

    public InsumoDTO findByIdDTO(Long id) {
        return Mapper.mapInsumoToDto(insumoRepository.findById(id).orElse(null));
    }

    public Insumo findById(Long id) {
        return insumoRepository.findById(id).orElse(null);
    }
    
}
