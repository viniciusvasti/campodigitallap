/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.SistemaDeAplicacaoDTO;
import com.vas.campodigital.models.secondary.SistemaDeAplicacao;
import com.vas.campodigital.repositories.SistemaDeAplicacaoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class SistemaDeAplicacaoService {

    private final SistemaDeAplicacaoRepository sistemaDeAplicacaoRepository;

    @Autowired
    public SistemaDeAplicacaoService(SistemaDeAplicacaoRepository sistemaDeAplicacaoRepository) {
        this.sistemaDeAplicacaoRepository = sistemaDeAplicacaoRepository;
    }

    public SistemaDeAplicacao save(SistemaDeAplicacao sistemaDeAplicacao) {
        return sistemaDeAplicacaoRepository.save(sistemaDeAplicacao);
    }

    public List<SistemaDeAplicacaoDTO> findAll() {
        List<SistemaDeAplicacaoDTO> dtos = new ArrayList<>();
        List<SistemaDeAplicacao> users = sistemaDeAplicacaoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapSistemaDeAplicacaoToDto(u))
        );
        
        return dtos;
    }

    public SistemaDeAplicacaoDTO findByIdDTO(Long id) {
        return Mapper.mapSistemaDeAplicacaoToDto(sistemaDeAplicacaoRepository.findById(id).orElse(null));
    }

    public SistemaDeAplicacao findById(Long id) {
        return sistemaDeAplicacaoRepository.findById(id).orElse(null);
    }
    
}
