/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDePlantioDTO;
import com.vas.campodigital.models.ApontamentoDePlantio;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDePlantioRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDePlantioService {

    private final ApontamentoDePlantioRepository apontamentoDePlantioRepository;

    @Autowired
    public ApontamentoDePlantioService(ApontamentoDePlantioRepository apontamentoDePlantioRepository) {
        this.apontamentoDePlantioRepository = apontamentoDePlantioRepository;
    }

    public ApontamentoDePlantio save(ApontamentoDePlantio apontamentoDePlantio) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDePlantio.getId() == null && apontamentoDePlantio.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDePlantio.getId() != null && apontamentoDePlantio.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDePlantioRepository.save(apontamentoDePlantio);
    }

    public ApontamentoDePlantio update(ApontamentoDePlantio apontamentoDePlantio, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDePlantio.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDePlantioRepository.save(apontamentoDePlantio);
    }

    public List<ApontamentoDePlantioDTO> findAll() {
        List<ApontamentoDePlantioDTO> dtos = new ArrayList<>();
        List<ApontamentoDePlantio> users = apontamentoDePlantioRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDePlantioToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDePlantioDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDePlantioToDto(apontamentoDePlantioRepository.findById(id).orElse(null));
    }

    public ApontamentoDePlantio findById(Long id) {
        return apontamentoDePlantioRepository.findById(id).orElse(null);
    }
    
}
