/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.SecaoDTO;
import com.vas.campodigital.mappers.Mapper;
import com.vas.campodigital.models.Secao;
import com.vas.campodigital.repositories.SecaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class SecaoService {

    private final SecaoRepository secaoRepository;

    @Autowired
    public SecaoService(SecaoRepository secaoRepository) {
        this.secaoRepository = secaoRepository;
    }

    public Secao save(Secao secao) {
        return secaoRepository.save(secao);
    }

    public SecaoDTO findByIdDTO(Long id) {
        return Mapper.mapSecaoToDto(secaoRepository.findById(id).orElse(null));
    }

    public Secao findById(Long id) {
        return secaoRepository.findById(id).orElse(null);
    }
    
}
