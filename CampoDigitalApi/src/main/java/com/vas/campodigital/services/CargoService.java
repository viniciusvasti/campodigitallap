/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.CargoDTO;
import com.vas.campodigital.models.secondary.Cargo;
import com.vas.campodigital.repositories.CargoRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class CargoService {

    private final CargoRepository cargoRepository;

    @Autowired
    public CargoService(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    public Cargo save(Cargo cargo) {
        return cargoRepository.save(cargo);
    }

    public List<CargoDTO> findAll() {
        List<CargoDTO> dtos = new ArrayList<>();
        List<Cargo> users = cargoRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapCargoToDto(u))
        );
        
        return dtos;
    }

    public CargoDTO findByIdDTO(Long id) {
        return Mapper.mapCargoToDto(cargoRepository.findById(id).orElse(null));
    }

    public Cargo findById(Long id) {
        return cargoRepository.findById(id).orElse(null);
    }
    
}
