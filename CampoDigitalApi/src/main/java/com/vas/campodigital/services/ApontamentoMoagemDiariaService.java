/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoMoagemDiariaDTO;
import com.vas.campodigital.models.ApontamentoMoagemDiaria;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoMoagemDiariaRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoMoagemDiariaService {

    private final ApontamentoMoagemDiariaRepository apontamentoMoagemDiariaRepository;

    @Autowired
    public ApontamentoMoagemDiariaService(ApontamentoMoagemDiariaRepository apontamentoMoagemDiariaRepository) {
        this.apontamentoMoagemDiariaRepository = apontamentoMoagemDiariaRepository;
    }

    public ApontamentoMoagemDiaria save(ApontamentoMoagemDiaria apontamentoMoagemDiaria) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoMoagemDiaria.getId() == null && apontamentoMoagemDiaria.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoMoagemDiaria.getId() != null && apontamentoMoagemDiaria.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoMoagemDiariaRepository.save(apontamentoMoagemDiaria);
    }

    public ApontamentoMoagemDiaria update(ApontamentoMoagemDiaria apontamentoMoagemDiaria, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoMoagemDiaria.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoMoagemDiariaRepository.save(apontamentoMoagemDiaria);
    }

    public List<ApontamentoMoagemDiariaDTO> findAll() {
        List<ApontamentoMoagemDiariaDTO> dtos = new ArrayList<>();
        List<ApontamentoMoagemDiaria> users = apontamentoMoagemDiariaRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoMoagemDiariaToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoMoagemDiariaDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoMoagemDiariaToDto(apontamentoMoagemDiariaRepository.findById(id).orElse(null));
    }

    public ApontamentoMoagemDiaria findById(Long id) {
        return apontamentoMoagemDiariaRepository.findById(id).orElse(null);
    }
    
}
