/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.models.UserFile;
import com.vas.campodigital.repositories.FileRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class FileService {

    private final FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public UserFile save(UserFile file) throws Exception {
        return fileRepository.save(file);
    }

    public List<UserFile> findAll() {
        List<UserFile> users = fileRepository.findAll();
        return users;
    }

    public UserFile findById(Long id) {
        return fileRepository.findById(id).orElse(null);
    }
    
}
