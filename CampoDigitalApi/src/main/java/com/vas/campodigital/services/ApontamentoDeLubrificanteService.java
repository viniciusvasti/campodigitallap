/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.services;

import com.vas.campodigital.dtos.ApontamentoDeLubrificanteDTO;
import com.vas.campodigital.models.ApontamentoDeLubrificante;
import com.vas.campodigital.models.User;
import com.vas.campodigital.repositories.ApontamentoDeLubrificanteRepository;
import java.util.ArrayList;
import java.util.List;
import com.vas.campodigital.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vinícius
 */
@Service
public class ApontamentoDeLubrificanteService {

    private final ApontamentoDeLubrificanteRepository apontamentoDeLubrificanteRepository;

    @Autowired
    public ApontamentoDeLubrificanteService(ApontamentoDeLubrificanteRepository apontamentoDeLubrificanteRepository) {
        this.apontamentoDeLubrificanteRepository = apontamentoDeLubrificanteRepository;
    }

    public ApontamentoDeLubrificante save(ApontamentoDeLubrificante apontamentoDeLubrificante) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeLubrificante.getId() == null && apontamentoDeLubrificante.getCreatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        if (apontamentoDeLubrificante.getId() != null && apontamentoDeLubrificante.getUpdatedBy().getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeLubrificanteRepository.save(apontamentoDeLubrificante);
    }

    public ApontamentoDeLubrificante update(ApontamentoDeLubrificante apontamentoDeLubrificante, User user) throws Exception {
        // TODO tratar exceção corretamente
        if (apontamentoDeLubrificante.getId() == null && user.getFuncionario() == null) {
            throw new Exception("Só deve ser feito por funcionário");
        }
        return apontamentoDeLubrificanteRepository.save(apontamentoDeLubrificante);
    }

    public List<ApontamentoDeLubrificanteDTO> findAll() {
        List<ApontamentoDeLubrificanteDTO> dtos = new ArrayList<>();
        List<ApontamentoDeLubrificante> users = apontamentoDeLubrificanteRepository.findAll();
        users.forEach(
                (u) -> dtos.add(Mapper.mapApontamentoDeLubrificanteToDto(u))
        );
        
        return dtos;
    }

    public ApontamentoDeLubrificanteDTO findByIdDTO(Long id) {
        return Mapper.mapApontamentoDeLubrificanteToDto(apontamentoDeLubrificanteRepository.findById(id).orElse(null));
    }

    public ApontamentoDeLubrificante findById(Long id) {
        return (apontamentoDeLubrificanteRepository.findById(id).orElse(null));
    }
    
}
