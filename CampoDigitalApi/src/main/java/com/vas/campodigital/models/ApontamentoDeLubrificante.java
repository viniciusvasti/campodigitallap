package com.vas.campodigital.models;

import com.vas.campodigital.enums.FinalidadeLubrificante;
import com.vas.campodigital.models.secondary.Equipamento;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class ApontamentoDeLubrificante extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataApontamento;

    @Column(nullable = false)
    private String pontoDeLubrificacao;

    @Column(nullable = false)
    private String justificativaRemontagem;

    @Column(nullable = false)
    private int objeto;

    @Column(nullable = false)
    private int sistemaVeicular;

    @Column(nullable = false)
    private int subSistemaVeicular;

    @Column(nullable = false)
    private int lubrificante;

    @Column(nullable = false)
    private int quantidade;

    @Enumerated(STRING)
    private FinalidadeLubrificante finalidade;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @ManyToOne(optional = false, targetEntity = Equipamento.class)
    private Equipamento equipamento;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getPontoDeLubrificacao() {
        return pontoDeLubrificacao;
    }

    public void setPontoDeLubrificacao(String pontoDeLubrificacao) {
        this.pontoDeLubrificacao = pontoDeLubrificacao;
    }

    public String getJustificativaRemontagem() {
        return justificativaRemontagem;
    }

    public void setJustificativaRemontagem(String justificativaRemontagem) {
        this.justificativaRemontagem = justificativaRemontagem;
    }

    public int getObjeto() {
        return objeto;
    }

    public void setObjeto(int objeto) {
        this.objeto = objeto;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public int getSistemaVeicular() {
        return sistemaVeicular;
    }

    public void setSistemaVeicular(int sistemaVeicular) {
        this.sistemaVeicular = sistemaVeicular;
    }

    public int getSubSistemaVeicular() {
        return subSistemaVeicular;
    }

    public void setSubSistemaVeicular(int subSistemaVeicular) {
        this.subSistemaVeicular = subSistemaVeicular;
    }

    public int getLubrificante() {
        return lubrificante;
    }

    public void setLubrificante(int lubrificante) {
        this.lubrificante = lubrificante;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public FinalidadeLubrificante getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(FinalidadeLubrificante finalidade) {
        this.finalidade = finalidade;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeLubrificante other = (ApontamentoDeLubrificante) obj;
        return this.id.equals(other.id);
    }

}
