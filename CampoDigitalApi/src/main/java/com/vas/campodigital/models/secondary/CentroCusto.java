
package com.vas.campodigital.models.secondary;

import com.vas.campodigital.models.BaseEntity;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author vinicius
 */
@Entity
@Table(indexes = @Index(columnList = "numero"))
public class CentroCusto extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String numero;
    
    @Column(nullable = false)
    private String descricao;
    
    @ManyToMany(targetEntity = Operacao.class)
    private Set<Operacao> operacoes;

    public CentroCusto(String numero, String descricao, Set<Operacao> operacoes) {
        this.numero = numero;
        this.descricao = descricao;
        this.operacoes = operacoes;
    }

    public CentroCusto() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPermissoes(Set<Operacao> operacoes) {
        this.operacoes = operacoes;
    }

    public Set<Operacao> getPermissoes() {
        return operacoes;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CentroCusto other = (CentroCusto) obj;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return numero + " - " + descricao;
    }
    
}
