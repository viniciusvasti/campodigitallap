/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.models;

import com.vas.campodigital.models.secondary.Equipamento;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class OrdemDeServico extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String origem;
    @Column(nullable = false)
    private int objeto;
    @Column(nullable = false)
    private int classificacaoManutencao;
    @Column(nullable = false)
    private String motivo;
    @Column(nullable = false)
    private String saidaPrevista; // (data e hora)
    @Column(nullable = false)
    private double kmHEquipamento;
    @Column(nullable = false)
    private int pontoDeManutencao;
    @Column(nullable = false)
    private int modosDeOperacao;
    @Column(nullable = false)
    private int setoresDaOficina;
    @Column(nullable = false)
    private String observacao;
    @Column(nullable = false)
    private String servico;
    @Column(nullable = false)
    private boolean fechada = false;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @ManyToOne(optional = false, targetEntity = User.class)
    private User usuarioSolicitante;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioFechou;

    @ManyToOne(optional = false, targetEntity = Equipamento.class)
    private Equipamento equipamento;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public int getObjeto() {
        return objeto;
    }

    public void setObjeto(int objeto) {
        this.objeto = objeto;
    }

    public int getClassificacaoManutencao() {
        return classificacaoManutencao;
    }

    public void setClassificacaoManutencao(int classificacaoManutencao) {
        this.classificacaoManutencao = classificacaoManutencao;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public String getSaidaPrevista() {
        return saidaPrevista;
    }

    public void setSaidaPrevista(String saidaPrevista) {
        this.saidaPrevista = saidaPrevista;
    }

    public double getKmHEquipamento() {
        return kmHEquipamento;
    }

    public void setKmHEquipamento(double kmHEquipamento) {
        this.kmHEquipamento = kmHEquipamento;
    }

    public User getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    public void setUsuarioSolicitante(User usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }

    public int getPontoDeManutencao() {
        return pontoDeManutencao;
    }

    public void setPontoDeManutencao(int pontoDeManutencao) {
        this.pontoDeManutencao = pontoDeManutencao;
    }

    public int getModosDeOperacao() {
        return modosDeOperacao;
    }

    public void setModosDeOperacao(int modosDeOperacao) {
        this.modosDeOperacao = modosDeOperacao;
    }

    public int getSetoresDaOficina() {
        return setoresDaOficina;
    }

    public void setSetoresDaOficina(int setoresDaOficina) {
        this.setoresDaOficina = setoresDaOficina;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getServico() {
        return servico;
    }

    public void setServico(String servico) {
        this.servico = servico;
    }

    public boolean getFechada() {
        return fechada;
    }

    public void setFechada(boolean fechada) {
        this.fechada = fechada;
    }

    public User getUsuarioFechou() {
        return usuarioFechou;
    }

    public void setUsuarioFechou(User usuarioFechou) {
        this.usuarioFechou = usuarioFechou;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdemDeServico other = (OrdemDeServico) obj;
        return this.id.equals(other.id);
    }

}
