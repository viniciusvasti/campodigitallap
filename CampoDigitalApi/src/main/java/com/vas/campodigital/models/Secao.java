package com.vas.campodigital.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author vinicius
 */
@Entity
@Table(indexes = @Index(columnList = "numero"))
public class Secao extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String numero;

    @OneToMany(mappedBy = "secao", targetEntity = Talhao.class)
    private Set<Talhao> talhoes;
    
    @JsonIgnore
    @ManyToOne
    private Fazenda fazenda;

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setTalhoes(Set<Talhao> talhoes) {
        this.talhoes = talhoes;
    }

    public Set<Talhao> getTalhoes() {
        return talhoes;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Secao other = (Secao) obj;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return numero;
    }
    
}
