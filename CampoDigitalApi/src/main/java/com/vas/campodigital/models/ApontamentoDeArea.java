package com.vas.campodigital.models;

import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.models.secondary.Operacao;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class ApontamentoDeArea extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataApontamento;

    @Column(nullable = false)
    private boolean visualizado;

    @Column(nullable = false)
    private double producao;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @ManyToOne(optional = false, targetEntity = CentroCusto.class)
    private CentroCusto centroCusto;

    @ManyToOne(optional = false, targetEntity = Operacao.class)
    private Operacao operacao;

    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public Operacao getOperacao() {
        return operacao;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public double getProducao() {
        return producao;
    }

    public void setProducao(double producao) {
        this.producao = producao;
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeArea other = (ApontamentoDeArea) obj;
        return this.id.equals(other.id);
    }

}
