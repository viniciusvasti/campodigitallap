package com.vas.campodigital.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author vinicius
 */
@Entity
@Table(indexes = @Index(columnList = "numero"))
public class Talhao extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String numero;

    @JsonIgnore
    @ManyToOne
    private Secao secao;

    @JsonIgnore
    @OneToMany(mappedBy = "talhao", targetEntity = Escala.class)
    private Set<Escala> escalas;

    @ManyToOne
    private UserFile mapaPDF;
    
    @Transient
    private String fazenda;

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setSecao(Secao secao) {
        this.secao = secao;
    }

    public Secao getSecao() {
        return secao;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setEscalas(Set<Escala> escalas) {
        this.escalas = escalas;
    }

    public Set<Escala> getEscalas() {
        return escalas;
    }

    public void setMapaPDF(UserFile mapaPDF) {
        this.mapaPDF = mapaPDF;
    }

    public UserFile getMapaPDF() {
        return mapaPDF;
    }

    public String getFazenda() {
        return secao.getFazenda().toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Talhao other = (Talhao) obj;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return numero;
    }

}
