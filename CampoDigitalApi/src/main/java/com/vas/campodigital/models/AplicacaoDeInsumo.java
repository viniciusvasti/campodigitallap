package com.vas.campodigital.models;

import com.vas.campodigital.enums.CondicaoSolo;
import com.vas.campodigital.enums.CondicaoTempo;
import com.vas.campodigital.enums.RestoVegetacao;
import com.vas.campodigital.enums.TipoPreparacaoSolo;
import com.vas.campodigital.enums.Turno;
import com.vas.campodigital.models.secondary.SistemaDeAplicacao;
import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.models.secondary.Insumo;
import com.vas.campodigital.models.secondary.Operacao;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class AplicacaoDeInsumo extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataAplicacao;

    @Column(nullable = false)
    private boolean visualizado;

    @Enumerated(STRING)
    private Turno turno;

    @Column(nullable = false)
    private double qntdProgHa;

    @Enumerated(STRING)
    private CondicaoSolo condicaoSolo;

    @Enumerated(STRING)
    private TipoPreparacaoSolo preparacaoSolo;

    @Column(nullable = false)
    private double doseProgHa;

    @Column(nullable = false)
    private int totaGeral;

    @Column(nullable = false)
    private double area;

    @Enumerated(STRING)
    private CondicaoTempo condicaoTempo;

    @Enumerated(STRING)
    private RestoVegetacao restoVegetacao;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @ManyToOne(optional = false, targetEntity = CentroCusto.class)
    private CentroCusto centroCusto;

    @ManyToOne(optional = false, targetEntity = SistemaDeAplicacao.class)
    private SistemaDeAplicacao sistemaDeAplicacao;

    @ManyToOne(optional = false, targetEntity = Insumo.class)
    private Insumo insumo;

    @ManyToOne(optional = false, targetEntity = Operacao.class)
    private Operacao operacao;

    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataAplicacao() {
        return dataAplicacao;
    }

    public void setDataAplicacao(String dataAplicacao) {
        this.dataAplicacao = dataAplicacao;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }

    public Operacao getOperacao() {
        return operacao;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public double getQntdProgHa() {
        return qntdProgHa;
    }

    public void setQntdProgHa(double qntdProgHa) {
        this.qntdProgHa = qntdProgHa;
    }

    public CondicaoSolo getCondicaoSolo() {
        return condicaoSolo;
    }

    public void setCondicaoSolo(CondicaoSolo condicaoSolo) {
        this.condicaoSolo = condicaoSolo;
    }

    public TipoPreparacaoSolo getPreparacaoSolo() {
        return preparacaoSolo;
    }

    public void setPreparacaoSolo(TipoPreparacaoSolo preparacaoSolo) {
        this.preparacaoSolo = preparacaoSolo;
    }

    public double getDoseProgHa() {
        return doseProgHa;
    }

    public void setDoseProgHa(double doseProgHa) {
        this.doseProgHa = doseProgHa;
    }

    public int getTotaGeral() {
        return totaGeral;
    }

    public void setTotaGeral(int totaGeral) {
        this.totaGeral = totaGeral;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public CondicaoTempo getCondicaoTempo() {
        return condicaoTempo;
    }

    public void setCondicaoTempo(CondicaoTempo condicaoTempo) {
        this.condicaoTempo = condicaoTempo;
    }

    public RestoVegetacao getRestoVegetacao() {
        return restoVegetacao;
    }

    public void setRestoVegetacao(RestoVegetacao restoVegetacao) {
        this.restoVegetacao = restoVegetacao;
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public SistemaDeAplicacao getSistemaDeAplicacao() {
        return sistemaDeAplicacao;
    }

    public void setSistemaDeAplicacao(SistemaDeAplicacao sistemaDeAplicacao) {
        this.sistemaDeAplicacao = sistemaDeAplicacao;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setInsumo(Insumo insumo) {
        this.insumo = insumo;
    }

    public Insumo getInsumo() {
        return insumo;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AplicacaoDeInsumo other = (AplicacaoDeInsumo) obj;
        return this.id.equals(other.id);
    }

}
