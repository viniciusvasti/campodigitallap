
package com.vas.campodigital.models;

import com.vas.campodigital.enums.TotalParcial;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class FechamentoDeCorte extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String dataFechamento;
    
    @Enumerated(STRING)
    private TotalParcial tipoFechamento;
    
    @Column(nullable = false)
    private boolean fichaEntregue = false;
    
    @Column(nullable = false)
    private boolean digitadoPims = false;
    
    @OneToOne(optional = false, targetEntity = OrdemDeCorte.class)
    private OrdemDeCorte ordemDeCorte;
    
    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;
    
    @Column(nullable = false)
    private boolean digitadoSol = false;
    
    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouSol;
    
    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public void setOrdemDeCorte(OrdemDeCorte ordemDeCorte) {
        this.ordemDeCorte = ordemDeCorte;
    }

    public OrdemDeCorte getOrdemDeCorte() {
        return ordemDeCorte;
    }

    public void setTipoFechamento(TotalParcial tipoFechamento) {
        this.tipoFechamento = tipoFechamento;
    }

    public TotalParcial getTipoFechamento() {
        return tipoFechamento;
    }

    public void setDataFechamento(String dataFechamento) {
        this.dataFechamento = dataFechamento;
    }

    public String getDataFechamento() {
        return dataFechamento;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setDigitadoSol(boolean digitadoSol) {
        this.digitadoSol = digitadoSol;
    }

    public void setUsuarioDigitouSol(User usuarioDigitouSol) {
        this.usuarioDigitouSol = usuarioDigitouSol;
    }

    public boolean isDigitadoSOL() {
        return digitadoSol;
    }

    public User getUsuarioDigitouSol() {
        return usuarioDigitouSol;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FechamentoDeCorte other = (FechamentoDeCorte) obj;
        return this.id.equals(other.id);
    }
    
}
