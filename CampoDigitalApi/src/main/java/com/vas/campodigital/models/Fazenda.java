
package com.vas.campodigital.models;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author vinicius
 */
@Entity
@Table(indexes = @Index(columnList = "numero"))
public class Fazenda extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private String numero;

    @OneToMany(mappedBy = "fazenda", targetEntity = Secao.class)
    private Set<Secao> secoes;
    
    @ManyToOne(optional = false)
    private Filial filial;

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSecoes(Set<Secao> secoes) {
        this.secoes = secoes;
    }

    public Set<Secao> getSecoes() {
        return secoes;
    }

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fazenda other = (Fazenda) obj;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return numero + " - " + nome;
    }
    
}
