
package com.vas.campodigital.models;

import com.vas.campodigital.enums.TotalParcial;
import com.vas.campodigital.models.secondary.SistemaDeCorte;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class OrdemDeCorte extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String dataCorte;
    
    @Column(nullable = false)
    private String hora;
    
    @Column(nullable = false)
    private boolean visualizado;
    
    @Enumerated(value = STRING)
    private TotalParcial totalOuParcial;
    
    @Column(nullable = false)
    private double area;
    
    @Column(nullable = false)
    private double estimativaTonelada;
    
    @Column(nullable = false)
    private boolean fichaEntregue = false;
    
    @Column(nullable = false)
    private boolean digitadoPims = false;
    
    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;
    
    @ManyToOne(optional = false, targetEntity = SistemaDeCorte.class)
    private SistemaDeCorte sistemaDeCorte;
    
    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataCorte() {
        return dataCorte;
    }

    public void setDataCorte(String dataCorte) {
        this.dataCorte = dataCorte;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public TotalParcial getTotalOuParcial() {
        return totalOuParcial;
    }

    public void setTotalOuParcial(TotalParcial totalOuParcial) {
        this.totalOuParcial = totalOuParcial;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getEstimativaTonelada() {
        return estimativaTonelada;
    }

    public void setEstimativaTonelada(double estimativaTonelada) {
        this.estimativaTonelada = estimativaTonelada;
    }

    public SistemaDeCorte getSistemaDeCorte() {
        return sistemaDeCorte;
    }

    public void setSistemaDeCorte(SistemaDeCorte sistemaDeCorte) {
        this.sistemaDeCorte = sistemaDeCorte;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdemDeCorte other = (OrdemDeCorte) obj;
        return this.id.equals(other.id);
    }
    
}
