package com.vas.campodigital.models;

import com.vas.campodigital.models.secondary.Equipamento;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class ApontamentoDeCombustivel extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataApontamento;

    @Column(nullable = false)
    private String horaApontamento;

    @Column(nullable = false)
    private String pontoDeLubrificacao;

    @Column(nullable = false)
    private int operador;

    @Column(nullable = false)
    private int combustivel;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    private int odometro;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @ManyToOne(optional = false, targetEntity = Equipamento.class)
    private Equipamento equipamento;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getHoraApontamento() {
        return horaApontamento;
    }

    public void setHoraApontamento(String horaApontamento) {
        this.horaApontamento = horaApontamento;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }

    public int getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(int combustivel) {
        this.combustivel = combustivel;
    }

    public int getOdometro() {
        return odometro;
    }

    public void setOdometro(int odometro) {
        this.odometro = odometro;
    }

    public String getPontoDeLubrificacao() {
        return pontoDeLubrificacao;
    }

    public void setPontoDeLubrificacao(String pontoDeLubrificacao) {
        this.pontoDeLubrificacao = pontoDeLubrificacao;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeCombustivel other = (ApontamentoDeCombustivel) obj;
        return this.id.equals(other.id);
    }

}
