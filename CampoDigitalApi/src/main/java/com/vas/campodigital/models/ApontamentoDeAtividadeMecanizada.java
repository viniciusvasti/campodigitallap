package com.vas.campodigital.models;

import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.models.secondary.Equipamento;
import com.vas.campodigital.models.secondary.Operacao;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class ApontamentoDeAtividadeMecanizada extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataApontamento;

    @Column(nullable = false)
    private int hOperador;

    @Column(nullable = false)
    private int implemento;

    @Column(nullable = false)
    private String horaKmInicial;

    @Column(nullable = false)
    private String horaKmFinal;

    @Column(nullable = false)
    private String horaKmTotal;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @ManyToOne(optional = false, targetEntity = CentroCusto.class)
    private CentroCusto centroCusto;

    @ManyToOne(optional = false, targetEntity = Operacao.class)
    private Operacao operacao;

    @ManyToOne(optional = false, targetEntity = Equipamento.class)
    private Equipamento equipamento;

    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public int gethOperador() {
        return hOperador;
    }

    public void sethOperador(int hOperador) {
        this.hOperador = hOperador;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public Operacao getOperacao() {
        return operacao;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public int getImplemento() {
        return implemento;
    }

    public void setImplemento(int implemento) {
        this.implemento = implemento;
    }

    public String getHoraKmInicial() {
        return horaKmInicial;
    }

    public void setHoraKmInicial(String horaKmInicial) {
        this.horaKmInicial = horaKmInicial;
    }

    public String getHoraKmFinal() {
        return horaKmFinal;
    }

    public void setHoraKmFinal(String horaKmFinal) {
        this.horaKmFinal = horaKmFinal;
    }

    public String getHoraKmTotal() {
        return horaKmTotal;
    }

    public void setHoraKmTotal(String horaKmTotal) {
        this.horaKmTotal = horaKmTotal;
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDeAtividadeMecanizada other = (ApontamentoDeAtividadeMecanizada) obj;
        return this.id.equals(other.id);
    }

}
