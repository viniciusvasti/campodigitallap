package com.vas.campodigital.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.TIMESTAMP;

/**
 *
 * @author vinicius
 */
@MappedSuperclass
public abstract class BaseEntity {
    
    @Column(nullable = false, updatable = false)
    @Temporal(TIMESTAMP)
    private Date createdAt;
    
    @Column(insertable = false)
    @Temporal(TIMESTAMP)
    private Date lastUpdated;
    
    @ManyToOne(targetEntity = User.class)
    private User createdBy;
    
    @ManyToOne(targetEntity = User.class)
    private User updatedBy;
    
    @Column(nullable = false)    
    private boolean ativo = true;

    public Date getCreatedAt() {
        return createdAt;
    }

    @PrePersist
    public void setCreatedAt() {
        this.createdAt = new Date();
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    @PreUpdate
    public void setLastUpdated() {
        this.lastUpdated = new Date();
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public String getReadableDay(Date date) {
        if (date == null) return "";
        return new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(date);
    }

    public String getReadableDateWithoutTime(Date date) {
        if (date == null) return "";
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
