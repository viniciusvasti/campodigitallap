
package com.vas.campodigital.models.secondary;

import com.vas.campodigital.enums.ApplicationPlatform;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 *
 * @author vinicius
 */
@Entity
public class Permissao {
    
    @Id
    private Long id;    
    @Column(nullable = false)
    private String modulo;
    @Column(nullable = false)
    private String endpoint;
    @Column(nullable = false)
    @Enumerated(STRING)
    private ApplicationPlatform applicationPlatform;

    public Permissao(Long id, String modulo, String endpoint, ApplicationPlatform applicationPlatform) {
        this.id = id;
        this.modulo = modulo;
        this.endpoint = endpoint;
        this.applicationPlatform = applicationPlatform;
    }

    public Permissao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public ApplicationPlatform getApplicationPlatform() {
        return applicationPlatform;
    }

    public void setApplicationPlatform(ApplicationPlatform applicationPlatform) {
        this.applicationPlatform = applicationPlatform;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Permissao other = (Permissao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return modulo;
    }
    
}
