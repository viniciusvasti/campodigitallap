package com.vas.campodigital.models;

import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.models.secondary.Equipamento;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class Escala extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String atividade;
    
    @Column(nullable = false)
    private String coletor;
    
    @Column(nullable = false)
    private String complemento;

    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = CentroCusto.class)
    private CentroCusto centroCusto;
    
    @ManyToMany(targetEntity = Equipamento.class)
    private Set<Equipamento> equipamentos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    public String getColetor() {
        return coletor;
    }

    public void setColetor(String coletor) {
        this.coletor = coletor;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public Set<Equipamento> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(Set<Equipamento> equipamentos) {
        this.equipamentos = equipamentos;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Escala other = (Escala) obj;
        return this.id.equals(other.id);
    }

}
