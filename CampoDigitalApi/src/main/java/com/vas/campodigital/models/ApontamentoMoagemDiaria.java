package com.vas.campodigital.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class ApontamentoMoagemDiaria extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataApontamento;

    @Column(nullable = false)
    private int convencionalCrua;

    @Column(nullable = false)
    private int mecanizadaCrua;

    @Column(nullable = false)
    private int convencionalQueimada;

    @Column(nullable = false)
    private int mecanizadaQueimada;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public int getConvencionalCrua() {
        return convencionalCrua;
    }

    public void setConvencionalCrua(int convencionalCrua) {
        this.convencionalCrua = convencionalCrua;
    }

    public int getMecanizadaCrua() {
        return mecanizadaCrua;
    }

    public void setMecanizadaCrua(int mecanizadaCrua) {
        this.mecanizadaCrua = mecanizadaCrua;
    }

    public int getConvencionalQueimada() {
        return convencionalQueimada;
    }

    public void setConvencionalQueimada(int convencionalQueimada) {
        this.convencionalQueimada = convencionalQueimada;
    }

    public int getMecanizadaQueimada() {
        return mecanizadaQueimada;
    }

    public void setMecanizadaQueimada(int mecanizadaQueimada) {
        this.mecanizadaQueimada = mecanizadaQueimada;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoMoagemDiaria other = (ApontamentoMoagemDiaria) obj;
        return this.id.equals(other.id);
    }

}
