package com.vas.campodigital.models;

import com.vas.campodigital.enums.NivelCritico;
import com.vas.campodigital.models.secondary.Permissao;
import java.util.Set;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class Ocorrencia extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;
    @ManyToOne(optional = false, targetEntity = Permissao.class)
    private Permissao permissao;
    private String texto;
    private String location;
    @ManyToMany(targetEntity = UserFile.class)
    private Set<UserFile> fotos;
    @Enumerated(STRING)
    private NivelCritico nivelCritico;
    private boolean concluido;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Set<UserFile> getFotos() {
        return fotos;
    }

    public void setFotos(Set<UserFile> fotos) {
        this.fotos = fotos;
    }

    public NivelCritico getNivelCritico() {
        return nivelCritico;
    }

    public void setNivelCritico(NivelCritico nivelCritico) {
        this.nivelCritico = nivelCritico;
    }

    public boolean isConcluido() {
        return concluido;
    }

    public void setConcluido(boolean concluido) {
        this.concluido = concluido;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ocorrencia other = (Ocorrencia) obj;
        return this.id.equals(other.id);
    }

}
