package com.vas.campodigital.models;

import com.vas.campodigital.models.secondary.SistemaDePlantio;
import com.vas.campodigital.models.secondary.CicloDeDesenvolvimento;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author vinicius
 */
@Entity
public class ApontamentoDePlantio extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String dataApontamento;

    @Column(nullable = false)
    private int variedade;

    @Column(nullable = false)
    private int espacamento;

    @Column(nullable = false)
    private double area;

    @Column(nullable = false)
    private boolean replantacao;

    @Column(nullable = false)
    private boolean localConcluido;

    @Column(nullable = false)
    private int procedenciaMuda;

    @Column(nullable = false)
    private int secaoMuda;

    @Column(nullable = false)
    private int talhaoMuda;

    @Column(nullable = false)
    private int nrCargas;

    @Column(nullable = false)
    private int pesoMedioKg;

    @Column(nullable = false)
    private int nrEquipes;

    @Column(nullable = false)
    private int nrCaminhoes;

    @Column(nullable = false)
    private double distanciaMuda;

    @Column(nullable = false)
    private boolean fichaEntregue = false;

    @Column(nullable = false)
    private boolean digitadoPims = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouPims;

    @Column(nullable = false)
    private boolean digitadoSOL = false;

    @ManyToOne(optional = true, targetEntity = User.class)
    private User usuarioDigitouSol;

    @ManyToOne(optional = false, targetEntity = CicloDeDesenvolvimento.class)
    private CicloDeDesenvolvimento cicloDeDesenvolvimento;

    @ManyToOne(optional = false, targetEntity = SistemaDePlantio.class)
    private SistemaDePlantio sistemaDePlantio;

    @ManyToOne(optional = false, targetEntity = Talhao.class)
    private Talhao talhao;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public boolean isFichaEntregue() {
        return fichaEntregue;
    }

    public void setFichaEntregue(boolean fichaEntregue) {
        this.fichaEntregue = fichaEntregue;
    }

    public String getDataApontamento() {
        return dataApontamento;
    }

    public void setDataApontamento(String dataApontamento) {
        this.dataApontamento = dataApontamento;
    }

    public Talhao getTalhao() {
        return talhao;
    }

    public void setTalhao(Talhao talhao) {
        this.talhao = talhao;
    }

    public int getVariedade() {
        return variedade;
    }

    public void setVariedade(int variedade) {
        this.variedade = variedade;
    }

    public int getEspacamento() {
        return espacamento;
    }

    public void setEspacamento(int espacamento) {
        this.espacamento = espacamento;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public boolean isReplantacao() {
        return replantacao;
    }

    public void setReplantacao(boolean replantacao) {
        this.replantacao = replantacao;
    }

    public boolean isLocalConcluido() {
        return localConcluido;
    }

    public void setLocalConcluido(boolean localConcluido) {
        this.localConcluido = localConcluido;
    }

    public int getProcedenciaMuda() {
        return procedenciaMuda;
    }

    public void setProcedenciaMuda(int procedenciaMuda) {
        this.procedenciaMuda = procedenciaMuda;
    }

    public int getSecaoMuda() {
        return secaoMuda;
    }

    public void setSecaoMuda(int secaoMuda) {
        this.secaoMuda = secaoMuda;
    }

    public int getTalhaoMuda() {
        return talhaoMuda;
    }

    public void setTalhaoMuda(int talhaoMuda) {
        this.talhaoMuda = talhaoMuda;
    }

    public int getNrCargas() {
        return nrCargas;
    }

    public void setNrCargas(int nrCargas) {
        this.nrCargas = nrCargas;
    }

    public int getPesoMedioKg() {
        return pesoMedioKg;
    }

    public void setPesoMedioKg(int pesoMedioKg) {
        this.pesoMedioKg = pesoMedioKg;
    }

    public int getNrEquipes() {
        return nrEquipes;
    }

    public void setNrEquipes(int nrEquipes) {
        this.nrEquipes = nrEquipes;
    }

    public int getNrCaminhoes() {
        return nrCaminhoes;
    }

    public void setNrCaminhoes(int nrCaminhoes) {
        this.nrCaminhoes = nrCaminhoes;
    }

    public double getDistanciaMuda() {
        return distanciaMuda;
    }

    public void setDistanciaMuda(double distanciaMuda) {
        this.distanciaMuda = distanciaMuda;
    }

    public CicloDeDesenvolvimento getCicloDeDesenvolvimento() {
        return cicloDeDesenvolvimento;
    }

    public void setCicloDeDesenvolvimento(CicloDeDesenvolvimento cicloDeDesenvolvimento) {
        this.cicloDeDesenvolvimento = cicloDeDesenvolvimento;
    }

    public SistemaDePlantio getSistemaDePlantio() {
        return sistemaDePlantio;
    }

    public void setSistemaDePlantio(SistemaDePlantio sistemaDePlantio) {
        this.sistemaDePlantio = sistemaDePlantio;
    }

    public void setDigitadoPims(boolean digitadoPims) {
        this.digitadoPims = digitadoPims;
    }

    public void setUsuarioDigitouPims(User usuarioDigitouPims) {
        this.usuarioDigitouPims = usuarioDigitouPims;
    }

    public boolean isDigitadoPims() {
        return digitadoPims;
    }

    public User getUsuarioDigitouPims() {
        return usuarioDigitouPims;
    }

    public void setDigitadoSOL(boolean digitadoSOL) {
        this.digitadoSOL = digitadoSOL;
    }

    public void setUsuarioDigitouSol(User usuarioDigitouSol) {
        this.usuarioDigitouSol = usuarioDigitouSol;
    }

    public boolean isDigitadoSOL() {
        return digitadoSOL;
    }

    public User getUsuarioDigitouSol() {
        return usuarioDigitouSol;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApontamentoDePlantio other = (ApontamentoDePlantio) obj;
        return this.id.equals(other.id);
    }

}
