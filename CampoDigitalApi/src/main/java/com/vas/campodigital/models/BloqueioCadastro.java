package com.vas.campodigital.models;

import com.vas.campodigital.enums.TipoCadastro;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.TIMESTAMP;

/**
 *
 * @author vinicius
 */
@Entity
public class BloqueioCadastro extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private TipoCadastro cadastro;

    @Column(nullable = false)
    @Temporal(TIMESTAMP)
    private Date inicioBloqueio;

    @Column(nullable = false)
    @Temporal(TIMESTAMP)
    private Date fimBloqueio;

    @ManyToOne(optional = false, targetEntity = Fazenda.class)
    private Fazenda fazenda;

    public TipoCadastro getCadastro() {
        return cadastro;
    }

    public void setCadastro(TipoCadastro cadastro) {
        this.cadastro = cadastro;
    }

    public Date getInicioBloqueio() {
        return inicioBloqueio;
    }

    public void setInicioBloqueio(Date inicioBloqueio) {
        this.inicioBloqueio = inicioBloqueio;
    }

    public Date getFimBloqueio() {
        return fimBloqueio;
    }

    public void setFimBloqueio(Date fimBloqueio) {
        this.fimBloqueio = fimBloqueio;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFazenda(Fazenda fazenda) {
        this.fazenda = fazenda;
    }

    public Fazenda getFazenda() {
        return fazenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BloqueioCadastro other = (BloqueioCadastro) obj;
        return this.id.equals(other.id);
    }

}
