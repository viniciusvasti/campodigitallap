package com.vas.campodigital.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vas.campodigital.models.secondary.Equipamento;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author vinicius
 */
@Entity
public class Filial extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String razaoSocial;
    private String nomeFantasia;
    @Column(nullable = false, unique = true)
    private String cnpj;
    
    @ManyToOne(optional = false)
    private Empresa empresa;
    
    @JsonIgnore
    @OneToMany(mappedBy = "filial", targetEntity = Fazenda.class)
    private Set<Fazenda> fazendas;
    
    @JsonIgnore
    @OneToMany(mappedBy = "filial", targetEntity = Funcionario.class, cascade = CascadeType.PERSIST)
    private Set<Funcionario> funcionarios;
    
    @JsonIgnore
    @OneToMany(mappedBy = "filial", targetEntity = Equipamento.class)
    private Set<Equipamento> equipamentos;

    public Filial() {
    }

    public Filial(String razaoSocial, String nomeFantasia, String cnpj) {
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public Set<Funcionario> getFuncionarios() {
        return funcionarios;
    }    

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Set<Fazenda> getFazendas() {
        return fazendas;
    }

    public void setFazendas(Set<Fazenda> fazendas) {
        this.fazendas = fazendas;
    }

    public Set<Equipamento> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(Set<Equipamento> equipamentos) {
        this.equipamentos = equipamentos;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Filial other = (Filial) obj;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return cnpj + " - " + razaoSocial;
    }
        
}
