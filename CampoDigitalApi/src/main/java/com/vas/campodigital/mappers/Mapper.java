/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.campodigital.mappers;

import com.vas.campodigital.dtos.AplicacaoDeInsumoDTO;
import com.vas.campodigital.dtos.ApontamentoChuvaDTO;
import com.vas.campodigital.dtos.ApontamentoDeAreaDTO;
import com.vas.campodigital.dtos.ApontamentoDeAreaPreparoDTO;
import com.vas.campodigital.dtos.ApontamentoDeAtividadeMecanizadaDTO;
import com.vas.campodigital.dtos.ApontamentoDeAtividadesManuaisDTO;
import com.vas.campodigital.dtos.ApontamentoDeCombustivelDTO;
import com.vas.campodigital.dtos.ApontamentoDeLubrificanteDTO;
import com.vas.campodigital.dtos.ApontamentoDePlantioDTO;
import com.vas.campodigital.dtos.ApontamentoDeTerceirosMecanizadoDTO;
import com.vas.campodigital.dtos.ApontamentoMoagemDiariaDTO;
import com.vas.campodigital.dtos.BloqueioCadastroDTO;
import com.vas.campodigital.dtos.CargoDTO;
import com.vas.campodigital.dtos.CentroCustoDTO;
import com.vas.campodigital.dtos.CicloDeDesenvolvimentoDTO;
import com.vas.campodigital.dtos.EmpresaDTO;
import com.vas.campodigital.dtos.EquipamentoDTO;
import com.vas.campodigital.dtos.EscalaDTO;
import com.vas.campodigital.dtos.FazendaDTO;
import com.vas.campodigital.dtos.FechamentoDeCorteDTO;
import com.vas.campodigital.dtos.FilialDTO;
import com.vas.campodigital.dtos.FuncionarioDTO;
import com.vas.campodigital.dtos.InsumoDTO;
import com.vas.campodigital.dtos.OcorrenciaDTO;
import com.vas.campodigital.dtos.OperacaoDTO;
import com.vas.campodigital.dtos.OrdemDeCorteDTO;
import com.vas.campodigital.dtos.OrdemDeServicoDTO;
import com.vas.campodigital.dtos.SecaoDTO;
import com.vas.campodigital.dtos.SistemaDeAplicacaoDTO;
import com.vas.campodigital.dtos.SistemaDeCorteDTO;
import com.vas.campodigital.dtos.SistemaDePlantioDTO;
import com.vas.campodigital.dtos.TalhaoDTO;
import com.vas.campodigital.dtos.TalhaoPlainDTO;
import com.vas.campodigital.dtos.UserDTO;
import com.vas.campodigital.dtos.UserFileDTO;
import com.vas.campodigital.models.AplicacaoDeInsumo;
import com.vas.campodigital.models.ApontamentoChuva;
import com.vas.campodigital.models.ApontamentoDeArea;
import com.vas.campodigital.models.ApontamentoDeAreaPreparo;
import com.vas.campodigital.models.ApontamentoDeAtividadeMecanizada;
import com.vas.campodigital.models.ApontamentoDeAtividadesManuais;
import com.vas.campodigital.models.ApontamentoDeCombustivel;
import com.vas.campodigital.models.ApontamentoDeLubrificante;
import com.vas.campodigital.models.ApontamentoDePlantio;
import com.vas.campodigital.models.ApontamentoDeTerceirosMecanizado;
import com.vas.campodigital.models.ApontamentoMoagemDiaria;
import com.vas.campodigital.models.BloqueioCadastro;
import com.vas.campodigital.models.Empresa;
import com.vas.campodigital.models.Escala;
import com.vas.campodigital.models.Fazenda;
import com.vas.campodigital.models.FechamentoDeCorte;
import com.vas.campodigital.models.Filial;
import com.vas.campodigital.models.Funcionario;
import com.vas.campodigital.models.Ocorrencia;
import com.vas.campodigital.models.OrdemDeCorte;
import com.vas.campodigital.models.OrdemDeServico;
import com.vas.campodigital.models.Secao;
import com.vas.campodigital.models.Talhao;
import com.vas.campodigital.models.User;
import com.vas.campodigital.models.UserFile;
import com.vas.campodigital.models.secondary.Cargo;
import com.vas.campodigital.models.secondary.CentroCusto;
import com.vas.campodigital.models.secondary.CicloDeDesenvolvimento;
import com.vas.campodigital.models.secondary.Equipamento;
import com.vas.campodigital.models.secondary.Insumo;
import com.vas.campodigital.models.secondary.Operacao;
import com.vas.campodigital.models.secondary.SistemaDeAplicacao;
import com.vas.campodigital.models.secondary.SistemaDeCorte;
import com.vas.campodigital.models.secondary.SistemaDePlantio;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author vinicius
 */
public class Mapper {

    public static FazendaDTO mapFazendaToDto(Fazenda source) {
        FazendaDTO dto = new FazendaDTO();
        dto.setAtivo(source.isAtivo());
        dto.setCreatedAt(source.getCreatedAt());
        dto.setFilial(mapFilialToDto(source.getFilial()));
        dto.setId(source.getId());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setNome(source.getNome());
        dto.setNumero(source.getNumero());
        dto.setSecoes(mapSecaoToDto(source.getSecoes()));
        return dto;
    }

    public static FilialDTO mapFilialToDto(Filial source) {
        FilialDTO dto = new FilialDTO();
        dto.setAtivo(true);
        dto.setCnpj(source.getCnpj());
        dto.setEmpresa(mapEmpresaToDto(source.getEmpresa()));
        dto.setEquipamentos(mapEquipamentoToDto(source.getEquipamentos()));
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setNomeFantasia(source.getNomeFantasia());
        dto.setRazaoSocial(source.getRazaoSocial());
        return dto;
    }

    public static EmpresaDTO mapEmpresaToDto(Empresa source) {
        EmpresaDTO dto = new EmpresaDTO();
        dto.setAtivo(true);
        dto.setCnpj(source.getCnpj());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setNomeFantasia(source.getNomeFantasia());
        dto.setRazaoSocial(source.getRazaoSocial());
        return dto;
    }

    public static CargoDTO mapCargoToDto(Cargo source) {
        CargoDTO dto = new CargoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static InsumoDTO mapInsumoToDto(Insumo source) {
        InsumoDTO dto = new InsumoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static EquipamentoDTO mapEquipamentoToDto(Equipamento source) {
        EquipamentoDTO dto = new EquipamentoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static Set<EquipamentoDTO> mapEquipamentoToDto(Set<Equipamento> source) {
        Set<EquipamentoDTO> dto = new HashSet<>();
        source.forEach(obj -> dto.add(mapEquipamentoToDto(obj)));
        return dto;
    }

    public static SecaoDTO mapSecaoToDto(Secao source) {
        SecaoDTO dto = new SecaoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setNumero(source.getNumero());
        dto.setTalhoes(mapTalhaoToDto(source.getTalhoes()));
        return dto;
    }

    public static Set<SecaoDTO> mapSecaoToDto(Set<Secao> source) {
        Set<SecaoDTO> dto = new HashSet<>();
        source.forEach(obj -> dto.add(mapSecaoToDto(obj)));
        return dto;
    }

    public static TalhaoDTO mapTalhaoToDto(Talhao source) {
        TalhaoDTO dto = new TalhaoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setNumero(source.getNumero());
        dto.setFazenda(source.getFazenda());
        return dto;
    }

    public static TalhaoPlainDTO mapTalhaoToPlainDto(Talhao source) {
        TalhaoPlainDTO dto = new TalhaoPlainDTO();
        dto.setAtivo(true);
        dto.setId(source.getId());
        dto.setNumero(source.getNumero());
        dto.setFazenda(source.getFazenda());
        dto.setSecao(source.getSecao().toString());
        dto.setMapaPDF(source.getMapaPDF());
        return dto;
    }

    public static Set<TalhaoDTO> mapTalhaoToDto(Set<Talhao> source) {
        Set<TalhaoDTO> dto = new HashSet<>();
        source.forEach(obj -> dto.add(mapTalhaoToDto(obj)));
        return dto;
    }

    public static UserFileDTO mapUserFileToDto(UserFile source) {
        UserFileDTO dto = new UserFileDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedAt(source.getCreatedAt());
        dto.setId(source.getId());
        dto.setFileName(source.getFileName());
        dto.setPath(source.getPath());
        return dto;
    }

    public static Set<UserFileDTO> mapUserFileToDto(Set<UserFile> source) {
        Set<UserFileDTO> dto = new HashSet<>();
        source.forEach(obj -> dto.add(mapUserFileToDto(obj)));
        return dto;
    }

    public static FuncionarioDTO mapFuncionarioToDto(Funcionario source) {
        if (source == null) {
            return null;
        }
        FuncionarioDTO dto = new FuncionarioDTO();
        dto.setId(source.getId());
        dto.setMatricula(source.getMatricula());
        dto.setFilial(mapFilialToDto(source.getFilial()));
        dto.setCargo(mapCargoToDto(source.getCargo()));
        return dto;
    }

    public static UserDTO mapUserToDto(User source) {
        UserDTO dto = new UserDTO();
        dto.setAtivo(source.isAtivo());
        dto.setId(source.getId());
        dto.setName(source.getName());
        dto.setUsername(source.getUsername());
        dto.setEmail(source.getEmail());
        dto.setFuncionario(mapFuncionarioToDto(source.getFuncionario()));
        dto.setPermissoes(source.getPermissoes());
        return dto;
    }

    public static OcorrenciaDTO mapOcorrenciaToDto(Ocorrencia source) {
        OcorrenciaDTO dto = new OcorrenciaDTO();
        dto.setAtivo(source.isAtivo());
        dto.setCreatedAt(source.getCreatedAt());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setId(source.getId());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setPermissao(source.getPermissao());
        dto.setTexto(source.getTexto());
        dto.setLocation(source.getLocation());
        dto.setFotos(mapUserFileToDto(source.getFotos()));
        dto.setNivelCritico(source.getNivelCritico());
        dto.setConcluido(source.isConcluido());
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static OperacaoDTO mapOperacaoToDto(Operacao source) {
        OperacaoDTO dto = new OperacaoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static SistemaDePlantioDTO mapSistemaDePlantioToDto(SistemaDePlantio source) {
        SistemaDePlantioDTO dto = new SistemaDePlantioDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static SistemaDeCorteDTO mapSistemaDeCorteToDto(SistemaDeCorte source) {
        SistemaDeCorteDTO dto = new SistemaDeCorteDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static SistemaDeAplicacaoDTO mapSistemaDeAplicacaoToDto(SistemaDeAplicacao source) {
        SistemaDeAplicacaoDTO dto = new SistemaDeAplicacaoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static CentroCustoDTO mapCentroCustoToDto(CentroCusto source) {
        CentroCustoDTO dto = new CentroCustoDTO();
        dto.setAtivo(true);
        dto.setLastUpdated(source.getLastUpdated());
        dto.setId(source.getId());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        return dto;
    }

    public static AplicacaoDeInsumoDTO mapAplicacaoDeInsumoToDto(AplicacaoDeInsumo source) {
        AplicacaoDeInsumoDTO dto = new AplicacaoDeInsumoDTO();
        dto.setAtivo(true);
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setId(source.getId());
        dto.setArea(source.getArea());
        dto.setCentroCusto(mapCentroCustoToDto(source.getCentroCusto()));
        dto.setCondicaoSolo(source.getCondicaoSolo());
        dto.setCondicaoTempo(source.getCondicaoTempo());
        dto.setDataAplicacao(source.getDataAplicacao());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setDoseProgHa(source.getDoseProgHa());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setInsumo(mapInsumoToDto(source.getInsumo()));
        dto.setOperacao(mapOperacaoToDto(source.getOperacao()));
        dto.setPreparacaoSolo(source.getPreparacaoSolo());
        dto.setQntdProgHa(source.getQntdProgHa());
        dto.setRestoVegetacao(source.getRestoVegetacao());
        dto.setSistemaDeAplicacao(mapSistemaDeAplicacaoToDto(source.getSistemaDeAplicacao()));
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setTotaGeral(source.getTotaGeral());
        dto.setTurno(source.getTurno());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setVisualizado(source.isVisualizado());
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoChuvaDTO mapApontamentoChuvaToDto(ApontamentoChuva source) {
        ApontamentoChuvaDTO dto = new ApontamentoChuvaDTO();
        dto.setAtivo(true);
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setId(source.getId());
        dto.setHora(source.getHora());
        dto.setElemento(source.getElemento());
        dto.setPosto(source.getPosto());
        dto.setLeitura(source.getLeitura());
        dto.setFilial(mapFilialToDto(source.getFilial()));
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setDigitadoSOL(source.isDigitadoSOL());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setUsuarioDigitouSol(source.getUsuarioDigitouSol() == null ? null : source.getUsuarioDigitouSol().toString());
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeAreaPreparoDTO mapApontamentoDeAreaPreparoToDto(ApontamentoDeAreaPreparo source) {
        ApontamentoDeAreaPreparoDTO dto = new ApontamentoDeAreaPreparoDTO();
        dto.setAtivo(true);
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setId(source.getId());
        dto.setArea(source.getArea());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setLocalConcluido(source.isLocalConcluido());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeAreaDTO mapApontamentoDeAreaToDto(ApontamentoDeArea source) {
        ApontamentoDeAreaDTO dto = new ApontamentoDeAreaDTO();
        dto.setId(source.getId());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setVisualizado(source.isVisualizado());
        dto.setProducao(source.getProducao());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setCentroCusto(mapCentroCustoToDto(source.getCentroCusto()));
        dto.setOperacao(mapOperacaoToDto(source.getOperacao()));
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeAtividadeMecanizadaDTO mapApontamentoDeAtividadeMecanizadaToDto(ApontamentoDeAtividadeMecanizada source) {
        ApontamentoDeAtividadeMecanizadaDTO dto = new ApontamentoDeAtividadeMecanizadaDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataApontamento(source.getDataApontamento());
        dto.sethOperador(source.gethOperador());
        dto.setEquipamento(mapEquipamentoToDto(source.getEquipamento()));
        dto.setOperacao(mapOperacaoToDto(source.getOperacao()));
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setImplemento(source.getImplemento());
        dto.setHoraKmInicial(source.getHoraKmInicial());
        dto.setHoraKmFinal(source.getHoraKmFinal());
        dto.setHoraKmTotal(source.getHoraKmTotal());
        dto.setCentroCusto(mapCentroCustoToDto(source.getCentroCusto()));
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeAtividadesManuaisDTO mapApontamentoDeAtividadesManuaisToDto(ApontamentoDeAtividadesManuais source) {
        ApontamentoDeAtividadesManuaisDTO dto = new ApontamentoDeAtividadesManuaisDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setHoraInicio(source.getHoraInicio());
        dto.setHoraFim(source.getHoraFim());
        dto.setOperacao(mapOperacaoToDto(source.getOperacao()));
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setCentroCusto(mapCentroCustoToDto(source.getCentroCusto()));
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeCombustivelDTO mapApontamentoDeCombustivelToDto(ApontamentoDeCombustivel source) {
        ApontamentoDeCombustivelDTO dto = new ApontamentoDeCombustivelDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setHoraApontamento(source.getHoraApontamento());
        dto.setOperador(source.getOperador());
        dto.setCombustivel(source.getCombustivel());
        dto.setOdometro(source.getOdometro());
        dto.setPontoDeLubrificacao(source.getPontoDeLubrificacao());
        dto.setEquipamento(mapEquipamentoToDto(source.getEquipamento()));
        dto.setQuantidade(source.getQuantidade());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeLubrificanteDTO mapApontamentoDeLubrificanteToDto(ApontamentoDeLubrificante source) {
        ApontamentoDeLubrificanteDTO dto = new ApontamentoDeLubrificanteDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setPontoDeLubrificacao(source.getPontoDeLubrificacao());
        dto.setJustificativaRemontagem(source.getJustificativaRemontagem());
        dto.setObjeto(source.getObjeto());
        dto.setEquipamento(mapEquipamentoToDto(source.getEquipamento()));
        dto.setSistemaVeicular(source.getSistemaVeicular());
        dto.setSubSistemaVeicular(source.getSubSistemaVeicular());
        dto.setLubrificante(source.getLubrificante());
        dto.setQuantidade(source.getQuantidade());
        dto.setFinalidade(source.getFinalidade());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDePlantioDTO mapApontamentoDePlantioToDto(ApontamentoDePlantio source) {
        ApontamentoDePlantioDTO dto = new ApontamentoDePlantioDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setVariedade(source.getVariedade());
        dto.setEspacamento(source.getEspacamento());
        dto.setArea(source.getArea());
        dto.setReplantacao(source.isReplantacao());
        dto.setLocalConcluido(source.isLocalConcluido());
        dto.setProcedenciaMuda(source.getProcedenciaMuda());
        dto.setSecaoMuda(source.getSecaoMuda());
        dto.setTalhaoMuda(source.getTalhaoMuda());
        dto.setNrCargas(source.getNrCargas());
        dto.setPesoMedioKg(source.getPesoMedioKg());
        dto.setNrEquipes(source.getNrEquipes());
        dto.setNrCaminhoes(source.getNrCaminhoes());
        dto.setDistanciaMuda(source.getDistanciaMuda());
        dto.setCicloDeDesenvolvimento(mapCicloDeDesenvolvimentoToDto(source.getCicloDeDesenvolvimento()));
        dto.setSistemaDePlantio(mapSistemaDePlantioToDto(source.getSistemaDePlantio()));
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setDigitadoSOL(source.isDigitadoSOL());
        dto.setUsuarioDigitouSol(source.getUsuarioDigitouSol() == null ? null : source.getUsuarioDigitouSol().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoDeTerceirosMecanizadoDTO mapApontamentoDeTerceirosMecanizadoToDto(ApontamentoDeTerceirosMecanizado source) {
        ApontamentoDeTerceirosMecanizadoDTO dto = new ApontamentoDeTerceirosMecanizadoDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setEquipamento(mapEquipamentoToDto(source.getEquipamento()));
        dto.setOperacao(mapOperacaoToDto(source.getOperacao()));
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setCentroCusto(mapCentroCustoToDto(source.getCentroCusto()));
        dto.setSequencia(source.getSequencia());
        dto.setQuantidade(source.getQuantidade());
        dto.setObservacao(source.getObservacao());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static ApontamentoMoagemDiariaDTO mapApontamentoMoagemDiariaToDto(ApontamentoMoagemDiaria source) {
        ApontamentoMoagemDiariaDTO dto = new ApontamentoMoagemDiariaDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataApontamento(source.getDataApontamento());
        dto.setConvencionalCrua(source.getConvencionalCrua());
        dto.setMecanizadaCrua(source.getMecanizadaCrua());
        dto.setConvencionalQueimada(source.getConvencionalQueimada());
        dto.setMecanizadaQueimada(source.getMecanizadaQueimada());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static BloqueioCadastroDTO mapBloqueioCadastroToDto(BloqueioCadastro source) {
        BloqueioCadastroDTO dto = new BloqueioCadastroDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setCadastro(source.getCadastro());
        dto.setInicioBloqueio(source.getInicioBloqueio());
        dto.setFimBloqueio(source.getFimBloqueio());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static CicloDeDesenvolvimentoDTO mapCicloDeDesenvolvimentoToDto(CicloDeDesenvolvimento source) {
        CicloDeDesenvolvimentoDTO dto = new CicloDeDesenvolvimentoDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setDescricao(source.getDescricao());
        dto.setNumero(source.getNumero());
        dto.setId(source.getId());
        return dto;
    }

    public static EscalaDTO mapEscalaToDto(Escala source) {
        EscalaDTO dto = new EscalaDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setId(source.getId());
        dto.setIdCentroCusto(source.getCentroCusto().getId());
        dto.setIdTalhao(source.getTalhao().getId());
        dto.setIdSecao(source.getTalhao().getSecao().getId());
        dto.setIdFazenda(source.getTalhao().getSecao().getFazenda().getId());
        dto.setAtividade(source.getAtividade());
        dto.setColetor(source.getColetor());
        dto.setComplemento(source.getComplemento());
        dto.setNomeFazenda(source.getTalhao().getFazenda());
        dto.setSecao(source.getTalhao().getSecao().toString());
        dto.setTalhao(source.getTalhao().toString());
        dto.setCentroCusto(source.getCentroCusto().toString());
        dto.setEquipamentos(mapEquipamentoToDto(source.getEquipamentos()));
        dto.setNumeroFazenda(source.getTalhao().getSecao().getFazenda().getNumero());
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static FechamentoDeCorteDTO mapFechamentoDeCorteToDto(FechamentoDeCorte source) {
        FechamentoDeCorteDTO dto = new FechamentoDeCorteDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setOrdemDeCorte(mapOrdemDeCorteToDto(source.getOrdemDeCorte()));
        dto.setTipoFechamento(source.getTipoFechamento());
        dto.setDataFechamento(source.getDataFechamento());
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setDigitadoSol(source.isDigitadoSOL());
        dto.setUsuarioDigitouSol(source.getUsuarioDigitouSol() == null ? null : source.getUsuarioDigitouSol().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static OrdemDeCorteDTO mapOrdemDeCorteToDto(OrdemDeCorte source) {
        OrdemDeCorteDTO dto = new OrdemDeCorteDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setDataCorte(source.getDataCorte());
        dto.setHora(source.getHora());
        dto.setVisualizado(source.isVisualizado());
        dto.setTalhao(mapTalhaoToDto(source.getTalhao()));
        dto.setTotalOuParcial(source.getTotalOuParcial());
        dto.setArea(source.getArea());
        dto.setEstimativaTonelada(source.getEstimativaTonelada());
        dto.setSistemaDeCorte(source.getSistemaDeCorte());
        dto.setDigitadoPims(source.isDigitadoPims());
        dto.setUsuarioDigitouPims(source.getUsuarioDigitouPims() == null ? null : source.getUsuarioDigitouPims().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

    public static OrdemDeServicoDTO mapOrdemDeServicoToDto(OrdemDeServico source) {
        OrdemDeServicoDTO dto = new OrdemDeServicoDTO();
        dto.setCreatedAt(source.getCreatedAt());
        dto.setLastUpdated(source.getLastUpdated());
        dto.setCreatedBy(source.getCreatedBy() == null ? null : source.getCreatedBy().toString());
        dto.setUpdatedBy(source.getUpdatedBy() == null ? null : source.getUpdatedBy().toString());
        dto.setAtivo(source.isAtivo());
        dto.setFichaEntregue(source.isFichaEntregue());
        dto.setOrigem(source.getOrigem());
        dto.setObjeto(source.getObjeto());
        dto.setClassificacaoManutencao(source.getClassificacaoManutencao());
        dto.setMotivo(source.getMotivo());
        dto.setEquipamento(mapEquipamentoToDto(source.getEquipamento()));
        dto.setSaidaPrevista(source.getSaidaPrevista());
        dto.setKmHEquipamento(source.getKmHEquipamento());
        dto.setUsuarioSolicitante(source.getUsuarioSolicitante() == null ? null : source.getUsuarioSolicitante().toString());
        dto.setPontoDeManutencao(source.getPontoDeManutencao());
        dto.setModosDeOperacao(source.getModosDeOperacao());
        dto.setSetoresDaOficina(source.getSetoresDaOficina());
        dto.setObservacao(source.getObservacao());
        dto.setServico(source.getServico());
        dto.setFechada(source.getFechada());
        dto.setUsuarioFechou(source.getUsuarioFechou() == null ? null : source.getUsuarioFechou().toString());
        dto.setId(source.getId());
        dto.setFazenda(mapFazendaToDto(source.getFazenda()));
        dto.setFuncionarioSolicitante(mapFuncionarioToDto(source.getUsuarioSolicitante().getFuncionario()));
        dto.setFuncionario(mapFuncionarioToDto(source.getCreatedBy().getFuncionario()));
        return dto;
    }

}
