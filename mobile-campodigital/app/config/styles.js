import { StyleSheet, Platform } from 'react-native';
import t from 'tcomb-form-native';

const theme = getTheme();

const GlobalStyles = StyleSheet.create({
  container: {
      justifyContent: 'center',
      backgroundColor: theme.light,
  },
  content: {
      paddingLeft: 20,
      paddingRight: 20,
      marginTop: 20,
      marginBottom: 5,
  },
  footer: {
      paddingLeft: 20,
      paddingRight: 20,
      marginTop: 20,
      backgroundColor: theme.light,
  },
  logoContainer: {
      alignItems: 'center',
      flexGrow: 1,
  },
  logo: {
      width: 100,
      height: 100,
      borderRadius: 10,
  },
  submitButton: {
      backgroundColor: theme.light,
      borderColor: theme.primary,
      borderWidth: 2,
      marginBottom: 15,
      shadowRadius: 0,
      shadowOpacity: 0,
  },
  submitButtonText: {
      color: theme.primary,
  },
  drawerItensText: {
    fontSize: 20,
  },
});

const FormStyles = {
  ...t.form.Form.stylesheet,
  controlLabel: {
    normal: {
      fontSize: 14,
      color: theme.primary,
    },
    error: {
      color: theme.danger,
    },
  },
  textbox: {
    normal: {
      fontSize: 16,
      // padding: 6,
      borderWidth: 0,
      marginBottom: 0,
      // backgroundColor: theme.primary,
      // borderRadius: 4,
    },
    error: {
      fontSize: 16,
      borderWidth: 0,
      marginBottom: 0,
    }
  },
  textboxView: {
    normal: {
      borderWidth: 0,
      borderRadius: 0,
      borderBottomWidth: 1,
      marginBottom: 5,
      borderBottomColor: theme.primary,
      // backgroundColor: theme.primary,
      // borderRadius: 4,
    },
    error: {
      borderWidth: 0,
      borderRadius: 0,
      borderBottomWidth: 1,
      marginBottom: 5,
      borderBottomColor: theme.danger,
    }
  },
  select: {
    normal: Platform.select({
      android: {
        paddingLeft: 7,
        color: '#000',
      },
      ios: {}
    }),
    // the style applied when a validation error occours
    error: Platform.select({
      android: {
        paddingLeft: 7,
        color: '#000',
      },
      ios: {}
    })
  },
  pickerTouchable: {
    normal: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center',
    },
    error: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center'
    },
    active: {
      borderBottomWidth: 1,
    }
  },
  pickerContainer: {
    normal: {
      marginBottom: 4,
      borderRadius: 4,
      borderWidth: 1
    },
    error: {
      marginBottom: 4,
      borderRadius: 4,
      borderWidth: 1
    },
    open: {
      backgroundColor: theme.primary,
    }
  },
};

function getTheme() {
  const theming = 'light';
  let myTheme = {};
  if (theming === 'light') {
    myTheme = {
      primary: '#a0be4d',
      secondary: '#3385ff',
      light: '#FFF',
      danger: '#ff8f00',
    };
  } else {
    myTheme = {
      primary: '#000',
    };
  }
  return myTheme;
}

export { GlobalStyles, FormStyles, getTheme };
