export const endpoint = 'operacaos/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_OPERACAO',
  FETCHED_ALL: 'FETCHED_ALL_OPERACAO',
  FETCH_ONE: 'FETCH_ONE_OPERACAO',
  FETCHED_ONE: 'FETCHED_ONE_OPERACAO',
  REQUEST_ERROR: 'REQUEST_ERROR_OPERACAO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_OPERACAO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
