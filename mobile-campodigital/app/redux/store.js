import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
// import { reducer as formReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const store = createStore(
  combineReducers({
    ...reducers,
    // form: formReducer,
  }),
  compose(applyMiddleware(...middlewares))
);
sagaMiddleware.run(rootSaga);
export { store };
