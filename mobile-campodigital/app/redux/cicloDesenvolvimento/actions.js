export const endpoint = 'ciclos-desenvolvimento/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_CICLO_DESENVOLVIMENTO',
  FETCHED_ALL: 'FETCHED_ALL_CICLO_DESENVOLVIMENTO',
  FETCH_ONE: 'FETCH_ONE_CICLO_DESENVOLVIMENTO',
  FETCHED_ONE: 'FETCHED_ONE_CICLO_DESENVOLVIMENTO',
  REQUEST_ERROR: 'REQUEST_ERROR_CICLO_DESENVOLVIMENTO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_CICLO_DESENVOLVIMENTO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
