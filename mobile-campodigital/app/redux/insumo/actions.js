export const endpoint = 'insumos/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_INSUMO',
  FETCHED_ALL: 'FETCHED_ALL_INSUMO',
  FETCH_ONE: 'FETCH_ONE_INSUMO',
  FETCHED_ONE: 'FETCHED_ONE_INSUMO',
  REQUEST_ERROR: 'REQUEST_ERROR_INSUMO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_INSUMO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
