export const endpoint = 'permissoes/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_PERMISSAO',
  FETCHED_ALL: 'FETCHED_ALL_PERMISSAO',
  FETCH_ONE: 'FETCH_ONE_PERMISSAO',
  FETCHED_ONE: 'FETCHED_ONE_PERMISSAO',
  REQUEST_ERROR: 'REQUEST_ERROR_PERMISSAO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_PERMISSAO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
