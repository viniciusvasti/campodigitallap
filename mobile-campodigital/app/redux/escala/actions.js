export const endpoint = 'escalas/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_ESCALA',
  FETCHED_ALL: 'FETCHED_ALL_ESCALA',
  FETCH_ONE: 'FETCH_ONE_ESCALA',
  FETCHED_ONE: 'FETCHED_ONE_ESCALA',
  REQUEST_ERROR: 'REQUEST_ERROR_ESCALA',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_ESCALA',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;