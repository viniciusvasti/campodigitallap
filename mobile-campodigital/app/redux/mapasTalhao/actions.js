export const endpoint = 'mapas-talhao/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_MAPA_TALHAO',
  FETCHED_ALL: 'FETCHED_ALL_MAPA_TALHAO',
  FETCH_ONE: 'FETCH_ONE_MAPA_TALHAO',
  FETCHED_ONE: 'FETCHED_ONE_MAPA_TALHAO',
  REQUEST_ERROR: 'REQUEST_ERROR_MAPA_TALHAO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_MAPA_TALHAO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => {
    return {
      type: actions.CLEAR_RESPOSTA
    }
  },
};


export default actions;