export const endpoint = 'sistemas-plantio/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_SISTEMA_PLANTIO',
  FETCHED_ALL: 'FETCHED_ALL_SISTEMA_PLANTIO',
  FETCH_ONE: 'FETCH_ONE_SISTEMA_PLANTIO',
  FETCHED_ONE: 'FETCHED_ONE_SISTEMA_PLANTIO',
  REQUEST_ERROR: 'REQUEST_ERROR_SISTEMA_PLANTIO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_SISTEMA_PLANTIO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
