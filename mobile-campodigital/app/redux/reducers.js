import Fazenda from './fazenda/reducer';
import ApontamentoChuva from './apontamentoChuva/reducer';
import AplicacaoInsumo from './aplicacaoInsumo/reducer';
import ApontamentoArea from './apontamentoArea/reducer';
import ApontamentoAreaPreparo from './apontamentoAreaPreparo/reducer';
import ApontamentoAtividadeMecanizada from './apontamentoAtividadeMecanizada/reducer';
import ApontamentoAtividadesManuais from './apontamentoAtividadesManuais/reducer';
import ApontamentoCombustivel from './apontamentoCombustivel/reducer';
import ApontamentoLubrificante from './apontamentoLubrificante/reducer';
import ApontamentoMoagemDiaria from './apontamentoMoagemDiaria/reducer';
import ApontamentoPlantio from './apontamentoPlantio/reducer';
import ApontamentoTerceirosMecanizado from './apontamentoTerceirosMecanizado/reducer';
import FechamentoCorte from './fechamentoCorte/reducer';
import OrdemCorte from './ordemCorte/reducer';
import OrdemServico from './ordemServico/reducer';
import CentroCusto from './centroCusto/reducer';
import Operacao from './operacao/reducer';
import Equipamento from './equipamento/reducer';
import SistemaAplicacao from './sistemaAplicacao/reducer';
import SistemaPlantio from './sistemaPlantio/reducer';
import SistemaCorte from './sistemaCorte/reducer';
import CicloDesenvolvimento from './cicloDesenvolvimento/reducer';
import Insumo from './insumo/reducer';
import Escala from './escala/reducer';
import MapasTalhao from './mapasTalhao/reducer';
import Ocorrencia from './ocorrencia/reducer';
import Permissao from './permissao/reducer';

export default {
  Fazenda,
  ApontamentoChuva,
  AplicacaoInsumo,
  ApontamentoArea,
  ApontamentoAreaPreparo,
  ApontamentoAtividadeMecanizada,
  ApontamentoAtividadesManuais,
  ApontamentoCombustivel,
  ApontamentoLubrificante,
  ApontamentoMoagemDiaria,
  ApontamentoPlantio,
  ApontamentoTerceirosMecanizado,
  FechamentoCorte,
  OrdemCorte,
  OrdemServico,
  CentroCusto,
  Operacao,
  Equipamento,
  SistemaAplicacao,
  Insumo,
  Escala,
  SistemaPlantio,
  CicloDesenvolvimento,
  SistemaCorte,
  MapasTalhao,
  Ocorrencia,
  Permissao,
};
