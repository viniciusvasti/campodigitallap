export const endpoint = 'aplicacoes-insumo/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APLICACAO_INSUMO',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APLICACAO_INSUMO',
  FETCH_ALL: 'FETCH_ALL_APLICACAO_INSUMO',
  FETCHED_ALL: 'FETCHED_ALL_APLICACAO_INSUMO',
  FETCH_ONE: 'FETCH_ONE_APLICACAO_INSUMO',
  FETCHED_ONE: 'FETCHED_ONE_APLICACAO_INSUMO',
  REQUEST_ERROR: 'REQUEST_ERROR_APLICACAO_INSUMO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APLICACAO_INSUMO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
