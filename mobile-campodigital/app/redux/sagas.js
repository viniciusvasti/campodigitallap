import { all } from 'redux-saga/effects';
import fazendaSagas from './fazenda/saga';
import apontamentoChuvaSagas from './apontamentoChuva/saga';
import aplicacaoInsumoSagas from './aplicacaoInsumo/saga';
import apontamentoAreaSagas from './apontamentoArea/saga';
import apontamentoAreaPreparoSagas from './apontamentoAreaPreparo/saga';
import apontamentoAtividadeMecanizadaSagas from './apontamentoAtividadeMecanizada/saga';
import apontamentoAtividadesManuaisSagas from './apontamentoAtividadesManuais/saga';
import apontamentoCombustivelSagas from './apontamentoCombustivel/saga';
import apontamentoLubrificanteSagas from './apontamentoLubrificante/saga';
import apontamentoMoagemDiariaSagas from './apontamentoMoagemDiaria/saga';
import apontamentoPlantioSagas from './apontamentoPlantio/saga';
import apontamentoTerceirosMecanizadoSagas from './apontamentoTerceirosMecanizado/saga';
import fechamentoCorteSagas from './fechamentoCorte/saga';
import ordemCorteSagas from './ordemCorte/saga';
import ordemServicoSagas from './ordemServico/saga';
import centroCustoSagas from './centroCusto/saga';
import operacaoSagas from './operacao/saga';
import equipamentoSagas from './equipamento/saga';
import sistemaAplicacaoSagas from './sistemaAplicacao/saga';
import sistemaPlantioSagas from './sistemaPlantio/saga';
import sistemaCorteSagas from './sistemaCorte/saga';
import cicloDesenvolvimentoSagas from './cicloDesenvolvimento/saga';
import insumoSagas from './insumo/saga';
import escalaSagas from './escala/saga';
import mapasTalhaoSagas from './mapasTalhao/saga';
import ocorrenciaSagas from './ocorrencia/saga';
import permissaoSagas from './permissao/saga';

export default function* rootSaga(getState) {
  yield all([
    fazendaSagas(),
    apontamentoChuvaSagas(),
    aplicacaoInsumoSagas(),
    apontamentoAreaSagas(),
    apontamentoAreaPreparoSagas(),
    apontamentoAtividadeMecanizadaSagas(),
    apontamentoAtividadesManuaisSagas(),
    apontamentoCombustivelSagas(),
    apontamentoLubrificanteSagas(),
    apontamentoMoagemDiariaSagas(),
    apontamentoPlantioSagas(),
    apontamentoTerceirosMecanizadoSagas(),
    fechamentoCorteSagas(),
    ordemCorteSagas(),
    ordemServicoSagas(),
    centroCustoSagas(),
    operacaoSagas(),
    equipamentoSagas(),
    sistemaAplicacaoSagas(),
    insumoSagas(),
    escalaSagas(),
    sistemaPlantioSagas(),
    cicloDesenvolvimentoSagas(),
    sistemaCorteSagas(),
    mapasTalhaoSagas(),
    ocorrenciaSagas(),
    permissaoSagas(),
  ]);
}
