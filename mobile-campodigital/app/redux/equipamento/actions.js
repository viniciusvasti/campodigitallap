export const endpoint = 'equipamentos/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_EQUIPAMENTO',
  FETCHED_ALL: 'FETCHED_ALL_EQUIPAMENTO',
  FETCH_ONE: 'FETCH_ONE_EQUIPAMENTO',
  FETCHED_ONE: 'FETCHED_ONE_EQUIPAMENTO',
  REQUEST_ERROR: 'REQUEST_ERROR_EQUIPAMENTO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_EQUIPAMENTO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
