import { AsyncStorage } from 'react-native';
import { all, takeLatest, put, fork, call } from 'redux-saga/effects';
import axios from 'axios';
import actions, { endpoint } from './actions';
import { Constants } from '../../config/constants';

const { apiUrl } = Constants;
const header = {
  headers: {
    Authorization: ''
  }
};

export function* fetchAllAsync() {
  yield put({ type: actions.SET_LOADING, payload: true });
  try {
    header.headers.Authorization = yield call(AsyncStorage.getItem, '@token');
    const response = yield call(axios.get, apiUrl + endpoint, header);
    yield put({ type: actions.FETCHED_ALL, payload: response.data });
  } catch (error) {
    if (!error.response) {
      yield put({ type: actions.REQUEST_ERROR, payload: error.message });
    } else {
      yield put({ type: actions.REQUEST_ERROR, payload: error.response.data.message });
      console.error('get all error response', error.response);
    }
  }
  yield put({ type: actions.SET_LOADING, payload: false });
}

export function* fetchOneAsync({ payload: id }) {
  yield put({ type: actions.SET_LOADING, payload: true });
  try {
    header.headers.Authorization = yield call(AsyncStorage.getItem, '@token');
    const response = yield call(axios.get, apiUrl + endpoint + id, header);
    yield put({ type: actions.FETCHED_ONE, payload: response.data });
  } catch (error) {
    if (!error.response) {
      yield put({ type: actions.REQUEST_ERROR, payload: error.message });
    } else {
      yield put({ type: actions.REQUEST_ERROR, payload: error.response.data.message });
      console.error('get one error response', error.response);
    }
  }
  yield put({ type: actions.SET_LOADING, payload: false });
}

/* WHATCHER */

export function* fetchAllRequest() {
  yield takeLatest(actions.FETCH_ALL, fetchAllAsync);
}

export function* fetchOneRequest() {
  yield takeLatest(actions.FETCH_ONE, fetchOneAsync);
}

export default function* rootSaga() {
  yield all([
    fork(fetchAllRequest),
    fork(fetchOneRequest),
  ]);
}
