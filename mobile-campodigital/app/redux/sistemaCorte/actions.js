export const endpoint = 'sistemas-corte/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_SISTEMA_CORTE',
  FETCHED_ALL: 'FETCHED_ALL_SISTEMA_CORTE',
  FETCH_ONE: 'FETCH_ONE_SISTEMA_CORTE',
  FETCHED_ONE: 'FETCHED_ONE_SISTEMA_CORTE',
  REQUEST_ERROR: 'REQUEST_ERROR_SISTEMA_CORTE',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_SISTEMA_CORTE',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
