export const endpoint = 'ocorrencias/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_OCORRENCIA',
  SAVE_SUCCESS: 'SAVE_SUCCESS_OCORRENCIA',
  FETCH_ALL: 'FETCH_ALL_OCORRENCIA',
  FETCHED_ALL: 'FETCHED_ALL_OCORRENCIA',
  FETCH_ONE: 'FETCH_ONE_OCORRENCIA',
  FETCHED_ONE: 'FETCHED_ONE_OCORRENCIA',
  REQUEST_ERROR: 'REQUEST_ERROR_OCORRENCIA',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_OCORRENCIA',
  SET_LOADING: 'SET_LOADING',
  PATCH_REQUEST: 'PATCH_REQUEST_OCORRENCIA',
  PATCH_SUCCESS: 'PATCH_SUCCESS_OCORRENCIA',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  patch: (entity) => {
    return {
      type: actions.PATCH_REQUEST,
      payload: entity
    }
  },
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
