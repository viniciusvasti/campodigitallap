export const endpoint = 'apontamentos-area-preparo/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_AREA_PREPARO',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_AREA_PREPARO',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_AREA_PREPARO',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_AREA_PREPARO',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_AREA_PREPARO',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_AREA_PREPARO',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_AREA_PREPARO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_AREA_PREPARO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
