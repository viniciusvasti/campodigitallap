export const endpoint = 'ordens-servico/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_ORDEM_SERVICO',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_ORDEM_SERVICO',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_ORDEM_SERVICO',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_ORDEM_SERVICO',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_ORDEM_SERVICO',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_ORDEM_SERVICO',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_ORDEM_SERVICO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_ORDEM_SERVICO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
