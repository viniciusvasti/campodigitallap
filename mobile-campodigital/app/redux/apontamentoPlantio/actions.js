export const endpoint = 'apontamentos-plantio/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_PLANTIO',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_PLANTIO',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_PLANTIO',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_PLANTIO',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_PLANTIO',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_PLANTIO',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_PLANTIO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_PLANTIO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
