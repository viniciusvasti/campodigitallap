export const endpoint = 'apontamentos-moagem-diaria/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_MOAGEM_DIARIA',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_MOAGEM_DIARIA',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_MOAGEM_DIARIA',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_MOAGEM_DIARIA',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_MOAGEM_DIARIA',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_MOAGEM_DIARIA',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_MOAGEM_DIARIA',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_MOAGEM_DIARIA',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
