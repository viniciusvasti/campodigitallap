export const endpoint = 'sistemas-aplicacao/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_SISTEMA_APLICACAO',
  FETCHED_ALL: 'FETCHED_ALL_SISTEMA_APLICACAO',
  FETCH_ONE: 'FETCH_ONE_SISTEMA_APLICACAO',
  FETCHED_ONE: 'FETCHED_ONE_SISTEMA_APLICACAO',
  REQUEST_ERROR: 'REQUEST_ERROR_SISTEMA_APLICACAO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_SISTEMA_APLICACAO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
