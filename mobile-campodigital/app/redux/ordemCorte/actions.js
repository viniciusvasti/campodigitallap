export const endpoint = 'ordens-corte/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_ORDEM_CORTE',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_ORDEM_CORTE',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_ORDEM_CORTE',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_ORDEM_CORTE',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_ORDEM_CORTE',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_ORDEM_CORTE',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_ORDEM_CORTE',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_ORDEM_CORTE',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
