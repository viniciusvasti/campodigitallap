export const endpoint = 'centros-custo/';

const actions = {
  FETCH_ALL: 'FETCH_ALL_CENTRO_CUSTO',
  FETCHED_ALL: 'FETCHED_ALL_CENTRO_CUSTO',
  FETCH_ONE: 'FETCH_ONE_CENTRO_CUSTO',
  FETCHED_ONE: 'FETCHED_ONE_CENTRO_CUSTO',
  REQUEST_ERROR: 'REQUEST_ERROR_CENTRO_CUSTO',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_CENTRO_CUSTO',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  clearResposta: () => ({
    type: actions.CLEAR_RESPOSTA
  }),
};


export default actions;
