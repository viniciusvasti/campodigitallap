export const endpoint = 'apontamentos-lubrificante/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_LUBRIFICANTE',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_LUBRIFICANTE',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_LUBRIFICANTE',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_LUBRIFICANTE',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_LUBRIFICANTE',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_LUBRIFICANTE',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_LUBRIFICANTE',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_LUBRIFICANTE',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
