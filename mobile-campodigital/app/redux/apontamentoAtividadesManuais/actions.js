export const endpoint = 'apontamentos-atividades-manuais/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_ATIVIDADES_MANUAIS',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_ATIVIDADES_MANUAIS',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_ATIVIDADES_MANUAIS',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_ATIVIDADES_MANUAIS',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_ATIVIDADES_MANUAIS',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_ATIVIDADES_MANUAIS',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_ATIVIDADES_MANUAIS',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_ATIVIDADES_MANUAIS',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
