export const endpoint = 'apontamentos-combustivel/';

const actions = {
  SAVE_REQUEST: 'SAVE_REQUEST_APONTAMENTO_COMBUSTIVEL',
  SAVE_SUCCESS: 'SAVE_SUCCESS_APONTAMENTO_COMBUSTIVEL',
  FETCH_ALL: 'FETCH_ALL_APONTAMENTO_COMBUSTIVEL',
  FETCHED_ALL: 'FETCHED_ALL_APONTAMENTO_COMBUSTIVEL',
  FETCH_ONE: 'FETCH_ONE_APONTAMENTO_COMBUSTIVEL',
  FETCHED_ONE: 'FETCHED_ONE_APONTAMENTO_COMBUSTIVEL',
  REQUEST_ERROR: 'REQUEST_ERROR_APONTAMENTO_COMBUSTIVEL',
  CLEAR_RESPOSTA: 'CLEAR_RESPOSTA_APONTAMENTO_COMBUSTIVEL',
  SET_LOADING: 'SET_LOADING',
  fetchAll: () => ({
    type: actions.FETCH_ALL,
  }),
  fetchOne: (id) => ({
    type: actions.FETCH_ONE,
    payload: id
  }),
  save: (entity) => ({
      type: actions.SAVE_REQUEST,
      payload: entity
    }),
  clearResposta: () => ({
      type: actions.CLEAR_RESPOSTA
    }),
};


export default actions;
