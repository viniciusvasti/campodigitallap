import React from 'react';
import { Text } from 'react-native';

const ErrorMessage = (props) => (
    <Text style={styles}>{props.message}</Text>
  );

const styles = {
  color: 'red', fontSize: 18, fontWeight: 'bold'
};

export { ErrorMessage };
