import React from 'react';
import { Icon, Button, } from 'native-base';
import { getTheme } from '../config/styles';

const theme = getTheme();

const DrawerButton = (props) => (
  <Button
    transparent
    onPress={() => props.navigation.navigate('DrawerOpen')}
  >
    <Icon ios='ios-menu' android="md-menu" style={{ fontSize: 30, color: theme.light }} />
  </Button>
);

const BackButton = (props) => (
  <Button
    transparent
    onPress={() => props.navigation.goBack()}
  >
    <Icon
      ios='ios-arrow-back' android="md-arrow-back"
      style={{ fontSize: 30, color: theme.light }}
    />
  </Button>
);

export { DrawerButton, BackButton };
