import React from 'react';
import { View, } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, Content, Fab, Icon, Text, Card, CardItem, Body, Toast, } from 'native-base';
import { DrawerButton } from '../../components';
import { getTheme } from '../../config/styles';
import actions from '../../redux/ocorrencia/actions';
import ModalOcorrencia from './ModalOcorrencia';

const theme = getTheme();

class Ocorrencia extends React.PureComponent {

  constructor(props) {
    super(props);  
    this.state = {
      ocorrenciaModalVisible: false,
      ocorrencia: null,
    };
  };
  
  componentDidMount() {
    this.props.fetchAll();
  }

  componentDidUpdate() {
    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      this.props.fetchAll();
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Ocorrências',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });

  setOcorrenciaModalVisible = (visible, ocorrencia) => {
    this.setState({ ocorrenciaModalVisible: visible, ocorrencia })
  }

  renderOcorrencias = () => this.props.ocorrencias.map(ocorrencia => 
    <Card key={ocorrencia.id}>
      <CardItem button onPress={() => this.setOcorrenciaModalVisible(true, ocorrencia)}>
        <Body>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>
              Código: <Text style={{ fontWeight: 'normal' }}>
                {ocorrencia.id}
              </Text>
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>
              Grupo: <Text style={{ fontWeight: 'normal' }}>
                {ocorrencia.permissao.modulo}
              </Text>
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>
              Cadastrado por: <Text style={{ fontWeight: 'normal' }}>{
                ocorrencia.createdBy.funcionario ?
                ocorrencia.createdBy.funcionario.matricula
                + ' - ' +ocorrencia.createdBy.name
                : ocorrencia.createdBy.name
              }</Text>
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>
              Concluída: <Text style={{ fontWeight: 'normal' }}>{
                ocorrencia.concluido ? 'Sim' : 'Não'
              }</Text>
            </Text>
          </View>
        </Body>
      </CardItem>
    </Card>
  );
  
  render() {
    return (
      <Container>
        <ModalOcorrencia
          modalVisible={this.state.ocorrenciaModalVisible}
          ocorrencia={this.state.ocorrencia}
          close={() => this.setOcorrenciaModalVisible(false, null)}
          patch={this.props.patch}
        />
        <Content padder>
          {this.renderOcorrencias()}
        </Content>
          <Fab
            active
            style={{ backgroundColor: theme.secondary }}
            containerStyle={{ }}
            onPress={() => this.props.navigation.navigate('FormOcorrencia')}
          >
            <Icon ios='ios-add' android="md-add" />
          </Fab>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.Ocorrencia.loading,
  ocorrencias: state.Ocorrencia.all,
  resposta: state.Ocorrencia.resposta,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    patch: actions.patch,
    fetchAll: actions.fetchAll,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Ocorrencia);
