import React from 'react';
import { Text, View, Platform, } from 'react-native';
import { Constants, Location, Permissions, FileSystem, } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Container, Content, Button, Toast, Icon, } from 'native-base';
import actions from '../../redux/ocorrencia/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsPermissao from '../../redux/permissao/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles, getTheme } from '../../config/styles';
import ModalCamera from './ModalCamera';
import ModalGallery from './ModalGallery';

const theme = getTheme();

const optionsNivelCritico = [
  { value: '0', text: 'Baixo' },
  { value: '1', text: 'Médio' },
  { value: '2', text: 'Alto' },
  { value: '3', text: 'Muito Alto' },
];

class FormOcorrencia extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      photosFolder: `${FileSystem.documentDirectory}ocorrencia/${Date.now()}`,
      photos: [],
      galleryModalVisible: false,
      cameraModalVisible: false,
      location: null,
      locationErrorMessage: null,
      value: {formfields: []},
      options: {
        fields: {
          secao: {
            label: 'Seção',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: [],
          },
          talhao: {
            label: 'Talhão',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
          },
          texto: {
            label: 'Descrição da Ocorrência',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.String,
            multiline: true,
          },
          nivelCritico: {
            label: 'Nível Crítico',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsNivelCritico,
          },
        }
      }
    }
  }

  setCameraModalVisible = (visible) => {
    FileSystem.readDirectoryAsync(this.state.photosFolder)
    .then(photos => {
      this.setState({ cameraModalVisible: visible, photos });
    }).catch((e) => {
      this.setState({ cameraModalVisible: visible });
    });
  }

  setGalleryModalVisible = (visible) => {
    this.setState({ galleryModalVisible: visible });
  }

  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Ocorrência',
    headerLeft: (
      <BackButton navigation={navigation} />
    ),
  });

  componentDidMount() {
    this.props.fetchAllFazendas();
    this.props.fetchAllPermissoes();
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        locationErrorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this.getLocationAsync();
    }
    FileSystem.deleteAsync(`${FileSystem.documentDirectory}/ocorrencia`);
  }
  
  getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationErrorMessage: 'Permissão negada para a localização.',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }
    if (prevProps.permissoes !== this.props.permissoes) {
      this.mountSelectPermissao();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  mountSelectPermissao = () => {
    const optionsPermissoes = this.props.permissoes.length ?
      this.props.permissoes
      .map(permissao => (
        {
          value: `${permissao.id}`,
          text: `${permissao.modulo}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          permissao: {
            label: 'Grupo de Usuários',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsPermissoes,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
        
    const formData = new FormData();
    this.state.photos.forEach(photo => {
      // const f = new File(this.state.photosFolder+'/'+photo,photo);
      // formData.append('files', f);
      formData.append('files', {
        uri: this.state.photosFolder+'/'+photo,
        type: 'image/jpeg',
        name: photo
      });
    });
    formData.append('nivelCritico', value.nivelCritico);
    formData.append('permissao', value.permissao);
    formData.append('talhao', value.talhao);
    formData.append('texto', value.texto);
    formData.append(
      'location', this.state.location ? JSON.stringify(this.state.location.coords) : ''
    );

    // console.log('value', dados);
    this.props.save(formData);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      permissao: t.Str,
      texto: t.String,
      nivelCritico: t.Str,
    });
      
    const ApontCh2 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      permissao: t.Str,
      texto: t.String,
      nivelCritico: t.Str,
    });
      
    const ApontCh3 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      talhao: t.Str,
      permissao: t.Str,
      texto: t.String,
      nivelCritico: t.Str,
    });

    if (this.state.value && this.state.value.fazenda) {
      if (this.state.value.secao) {
        return ApontCh3;
      }
      return ApontCh2;
    }

    return ApontCh1;
  }

  onChange = (value) => {

    if (value.fazenda && value.fazenda !== this.state.value.fazenda) {
      const fazenda = this.props.fazendas
      .filter(fazenda => fazenda.id == value.fazenda)[0];
      const optionsSecoes = fazenda.secoes ?
        fazenda.secoes
        .filter(secao => secao.ativo)
        .map(secao => (
          {
            value: `${secao.id}`,
            text: `${secao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.secao.options = optionsSecoes;
      this.setState({ value, options });
    } else if (value.secao && value.secao !== this.state.value.secao) {
      const fazenda = this.props.fazendas
        .filter(fazenda => fazenda.id == value.fazenda)[0];
      const secao = fazenda.secoes
        .filter(secao => secao.id == value.secao)[0];
      const optionsTalhoes = secao.talhoes ?
        secao.talhoes
        .filter(talhao => talhao.ativo)
        .map(talhao => (
          {
            value: `${talhao.id}`,
            text: `${talhao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.talhao.options = optionsTalhoes;
      this.setState({ value, options });
    }
  }

  takePhoto = async () => {
    const permission1 = await Permissions.askAsync(Permissions.CAMERA);
    const permission2 = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (permission1.status === 'granted' && permission2.status === 'granted') {
      this.setCameraModalVisible(true);
    } else {
      Toast.show({
        text: 'A permissão para acesso a câmera não foi concedida',
        duration: 5000
      });
    }
  }

  renderLocationError() {
    return (
      <View>
        <Text style={{ color: 'red', marginBottom: 20 }}>
          {this.state.locationErrorMessage ? this.state.locationErrorMessage : ''}
        </Text>
      </View>
    );
  }
     
  render() {
    if (this.state.locationErrorMessage) {
      console.log(this.state.locationErrorMessage);
    } else if (this.state.location) {
      // console.log(JSON.stringify(this.state.location));
    }
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <ModalCamera
          modalVisible={this.state.cameraModalVisible}
          photosFolder={this.state.photosFolder}
          close={() => this.setCameraModalVisible(false)}
        />
        <ModalGallery
          modalVisible={this.state.galleryModalVisible}
          photosFolder={this.state.photosFolder}
          photos={this.state.photos}
          close={() => this.setGalleryModalVisible(false)}
        />
        <Content style={GlobalStyles.content}>
          {this.renderLocationError()}
          <Button full
            style={{
              backgroundColor: theme.light,
              borderColor: theme.secondary,
              borderWidth: 2,
              marginBottom: 15,
              shadowRadius: 0,
              shadowOpacity: 0,
            }}
            onPress={this.takePhoto}
          >
            <Icon type="MaterialIcons" name="add-a-photo"
              style={{ fontSize: 35, color: theme.secondary }}
            />
            <Text
              style={{
                color: theme.secondary,
              }}
            >
              TIRAR FOTO
            </Text>
          </Button>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
            onChange={this.onChange}
          />
          <Button full
            style={{
              backgroundColor: theme.light,
              borderColor: theme.secondary,
              borderWidth: 2,
              marginBottom: 15,
              shadowRadius: 0,
              shadowOpacity: 0,
            }}
            onPress={() => this.setGalleryModalVisible(true)}
          >
            <Icon type="MaterialIcons" name="add-a-photo"
              style={{ fontSize: 35, color: theme.secondary }}
            />
            <Text
              style={{
                color: theme.secondary,
              }}
            >
              VER FOTOS ({this.state.photos.length})
            </Text>
          </Button>
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.Ocorrencia.resposta,
  tipoResposta: state.Ocorrencia.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
  permissoes: state.Permissao.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllPermissoes: actionsPermissao.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormOcorrencia);
