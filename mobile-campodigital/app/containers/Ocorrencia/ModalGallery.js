import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Modal, } from 'react-native';
import { FileSystem, MediaLibrary, Permissions } from 'expo';
import { Icon, } from 'native-base';
import Photo from './Photo';

export default class ModalGallery extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      selected: [],
    };
  }

  toggleSelection = (uri, isSelected) => {
    let selected = this.state.selected;
    if (isSelected) {
      selected.push(uri);
    } else {
      selected = selected.filter(item => item !== uri);
    }
    this.setState({ selected });
  };

  saveToGallery = async () => {
    const photos = this.state.selected;

    if (photos.length > 0) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

      if (status !== 'granted') {
        throw new Error('Denied CAMERA_ROLL permissions!');
      }

      const promises = photos.map(photoUri => {
        return MediaLibrary.createAssetAsync(photoUri);
      });

      await Promise.all(promises);
      alert('Successfully saved photos to user\'s gallery!');
    } else {
      alert('No photos to save!');
    }
  };

  renderPhoto = fileName => 
    <Photo
      key={fileName}
      uri={`${this.props.photosFolder}/${fileName}`}
      onSelectionToggle={this.toggleSelection}
    />;

  render() {
    return (
      <Modal
        transparent
        animationType="slide"
        visible={this.props.modalVisible}
        onRequestClose={() => {
          this.props.close();
        }}
      >
        <View style={styles.container}>
          <View style={styles.navbar}>
            <TouchableOpacity style={styles.button} onPress={() => this.props.close()}>
              <Icon type="MaterialIcons" name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={this.saveToGallery}>
              <Text style={styles.whiteText}>Save selected to gallery</Text>
            </TouchableOpacity>
          </View>
          <ScrollView contentComponentStyle={{ flex: 1 }}>
            <View style={styles.pictures}>
              {this.props.photos.map(this.renderPhoto)}
            </View>
          </ScrollView>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'white',
  },
  navbar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#4630EB',
  },
  pictures: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 8,
  },
  button: {
    padding: 20,
  },
  whiteText: {
    color: 'white',
  }
});