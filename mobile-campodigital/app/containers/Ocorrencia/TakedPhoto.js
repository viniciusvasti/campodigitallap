import React from 'react';
import { TouchableOpacity, ImageBackground, Text, View, } from 'react-native';
import { Icon, Button, } from 'native-base';
import { getTheme } from '../../config/styles';

const theme = getTheme();
export default class Photo extends React.Component {

  render() {
    const { photo, onPressOk, onPressCancel } = this.props;
    return (
      <ImageBackground
        style={{ width: '100%', height: '100%', alignContent: 'flex-end' }}
        source={{ uri: photo.uri }}
        resizeMode="contain"
        backgroundColor='#000'
      >
        <View style={{ flexDirection: 'row' }}>
          <Button full
            style={{
              flex: 1,
              backgroundColor: theme.primary,
              borderColor: theme.light,
              borderWidth: 2,
              marginBottom: 15,
              shadowRadius: 0,
              shadowOpacity: 0,
            }}
            onPress={onPressOk}
          >
            <Text style={{ color: '#FFF' }}>OK</Text>
          </Button>
          <Button full
            style={{
              flex: 1,
              backgroundColor: theme.danger,
              borderColor: theme.light,
              borderWidth: 2,
              marginBottom: 15,
              shadowRadius: 0,
              shadowOpacity: 0,
            }}
            onPress={onPressCancel}
          >
            <Text style={{ color: '#FFF' }}>DESCARTAR</Text>
          </Button>
        </View>
      </ImageBackground>
    )
  };
}