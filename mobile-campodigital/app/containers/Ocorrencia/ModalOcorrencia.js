import React from 'react';
import { StyleSheet, View, Modal, Alert, Image, AsyncStorage, TouchableOpacity } from 'react-native';
import {
  Icon,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  Body,
  Button,
  Header,
  Left,
  Right,
  Title
} from 'native-base';
import { Marker } from 'react-native-maps';
import { GlobalStyles, getTheme, } from '../../config/styles';
import { Constants } from '../../config/constants';
import { MapView } from 'expo';

const { apiUrl } = Constants;

const theme = getTheme();

export default class ModalOcorrencia extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      verFotos: false,
      verMapa: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('@token').then(
      token => this.setState({ token })
    );
  }

  ocorrenciaConcluida = (v) => {
    const ocorrencia = {
      id: this.props.ocorrencia.id,
      ocorrenciaConcluida: v,
    };
    this.props.patch(ocorrencia);
    this.props.close();
  }

  renderFotos = () => this.props.ocorrencia.fotos.map(foto =>
    <Image
      key={foto.id}
      source={{
        uri: apiUrl+'/ocorrencias/foto/'+foto.fileName,
        method: 'GET',
        headers: {
          Authorization: this.state.token,
        },
      }}
      style={{
        width: 400,
        height: 400,
        marginTop: 15,
        resizeMode: Image.resizeMode.contain,
        alignSelf: 'center',
      }}
    />
  );

  renderMapa = (locationJson) =>
    <MapView
      mapType="satellite"
      style={{
        width: 400,
        height: 400,
      }}
      initialRegion={{
        latitude: locationJson.latitude,
        longitude: locationJson.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }}
    >
      <Marker coordinate={{ latitude: locationJson.latitude, longitude: locationJson.longitude }}>
        <Icon ios="ios-pin" android="md-pin" style={{ color: 'red' }} />
      </Marker>
    </MapView>

  render() {

    const {
      id,
      talhao,
      permissao,
      texto,
      location,
      nivelCritico,
      concluido,
      createdBy,
      fotos,
    } = this.props.ocorrencia || {};
    
    const locationJson = location ? JSON.parse(location.replace('\\','')) : undefined;

    return (
      <Modal
        transparent
        animationType="slide"
        visible={this.props.modalVisible}
        onRequestClose={() => {
          this.props.close();
        }}
      >
        <Container style={GlobalStyles.container}>
          <Header>
            <Left>
              <Icon
                ios="ios-arrow-back"
                android="md-arrow-back"
                onPress={() => this.props.close()}
                style={{ color: '#FFF' }}
              />
            </Left>
            <Body>
              <Title>Ocorrência</Title>
            </Body>
            <Right />
          </Header>
          <Content style={GlobalStyles.content}>
            { this.props.ocorrencia ?
            <Card key={id} style={{ paddingBottom: 15 }}>
              <CardItem>
                <Body>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Código: <Text style={{ fontWeight: 'normal' }}>
                        {id}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Funcionário: <Text style={{ fontWeight: 'normal' }}>
                        {createdBy.funcionario ?
                                createdBy.funcionario.matricula + ' - ' + createdBy.name
                              : createdBy.name}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Fazenda: <Text style={{ fontWeight: 'normal' }}>
                        {talhao.fazenda}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Talhão: <Text style={{ fontWeight: 'normal' }}>
                        {talhao.numero}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Grupo: <Text style={{ fontWeight: 'normal' }}>
                        {permissao.modulo}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Nível Crítico: <Text style={{ fontWeight: 'normal' }}>
                        {nivelCritico}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Texto: <Text style={{ fontWeight: 'normal' }}>
                        {texto}
                      </Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Concluída: <Text style={{ fontWeight: 'normal' }}>
                        {concluido ? 'Sim' : 'Não'}
                      </Text>
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => this.setState({ verFotos: !this.state.verFotos })}
                  >
                    <Text style={{ fontWeight: 'bold', color: theme.secondary }}>
                      {this.state.verFotos ? 'Esconder Fotos' : 'Ver Fotos'}
                    </Text>
                  </TouchableOpacity>
                  {
                    locationJson ?
                    <TouchableOpacity
                      style={{ flexDirection: 'row' }}
                      onPress={() => this.setState({ verMapa: !this.state.verMapa })}
                    >
                      <Text style={{ fontWeight: 'bold', color: theme.secondary }}>
                        {this.state.verMapa ? 'Esconder Mapa' : 'Ver Mapa'}
                      </Text>
                    </TouchableOpacity>
                    : <View/>
                  }
                </Body>
              </CardItem>
              {
                this.state.verFotos ?
                this.renderFotos()
                : <View/>
              }
              {
                this.state.verMapa ?
                this.renderMapa(locationJson)
                : <View/>
              }
            </Card>
            : ''}
            {!concluido ?
              <Button full
                style={{
                  backgroundColor: theme.light,
                  borderColor: theme.primary,
                  borderWidth: 2,
                  marginBottom: 15,
                  shadowRadius: 0,
                  shadowOpacity: 0,
                }}
                onPress={() => Alert.alert(
                  'Concluída',
                  'Informar que a ocorrência foi resolvida?',
                  [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => this.ocorrenciaConcluida(true)},
                  ],
                  { cancelable: true }
                )}
              >
                <Icon ios="ios-checkmark" android="md-checkmark"
                  style={{ fontSize: 35, color: theme.primary }}
                />
                <Text
                  style={{
                    color: theme.primary,
                  }}
                >
                  CONCLUÍDA
                </Text>
              </Button>
            : <View/>}
          </Content>
        </Container>
      </Modal>
    );
  }
}