import React from 'react';
import { Image, AsyncStorage } from 'react-native';
import { Container, Content, Text, List, ListItem, Icon } from 'native-base';
import { GlobalStyles } from '../../config/styles';

const routes = [
  {
    id: 'Home', text: 'Início', iconIos: 'ios-home', iconAndroid: 'md-home',
  },
  {
    id: 'Ocorrencia', text: 'Ocorrência', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'MapasTalhao', text: 'Mapas', iconIos: 'ios-map', iconAndroid: 'md-map',
  },
  {
    id: 'Escala', text: 'Escala', iconIos: 'ios-construct', iconAndroid: 'md-construct',
  },
  {
    id: 'ApontamentoChuva', text: 'Apontamentos de Chuva', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'AplicacaoInsumo', text: 'Aplicacao de Insumos', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoArea', text: 'Apontamento de Área', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoAreaPreparo', text: 'Apontamento de Área de Preparo', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoAtividadeMecanizada', text: 'Apontamento de Atividade Mecanizada', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoAtividadesManuais', text: 'Apontamento de Atividades Manuais', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoCombustivel', text: 'Apontamento de Combustível', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoLubrificante', text: 'Apontamento de Lubrificante', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoMoagemDiaria', text: 'Apontamento de Moagem Diária', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoPlantio', text: 'Apontamento de Plantio', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'ApontamentoTerceirosMecanizado', text: 'Apontamento de Terceiros Mecanizado', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'FechamentoCorte', text: 'Fechamento de Corte', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'OrdemCorte', text: 'Ordem de Corte', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
  {
    id: 'OrdemServico', text: 'Ordem de Servico', iconIos: 'ios-list-box', iconAndroid: 'md-list-box',
  },
];
class SideBar extends React.Component {
  logout = async () => {
    await AsyncStorage.removeItem('@token');
    this.props.navigation.navigate('LoginForm')
  }

  render() {
    return (
      <Container>
        <Content>
          <Image
            source={{
              uri: 'https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/drawer-cover.png'
            }}
            style={{
              height: 120,
              alignSelf: 'stretch',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          />
          <Image
            square
            style={{ height: 80, width: 70 }}
            source={{
              uri: 'https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/logo.png'
            }}
          />
          <List
            dataArray={routes}
            renderRow={data => (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data.id)}
                >
                  <Icon ios={data.iconIos} android={data.iconAndroid} style={{ fontSize: 20 }} />
                  <Text style={GlobalStyles.drawerItensText}> {data.text}</Text>
                </ListItem>
              )}
          />
          <ListItem
            button
            onPress={() => this.logout()}
          >
            <Icon ios="ios-log-out" android="md-log-out" style={{ fontSize: 20 }} />
            <Text style={GlobalStyles.drawerItensText}> Sair</Text>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

export default SideBar;
