import React from 'react';
import { Text, } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, Content, Fab, Icon } from 'native-base';
import { DrawerButton } from '../../components';
import { getTheme } from '../../config/styles';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsCentroCusto from '../../redux/centroCusto/actions';
import actionsOperacao from '../../redux/operacao/actions';
import actionsSistemaAplicacao from '../../redux/sistemaAplicacao/actions';
import actionsInsumo from '../../redux/insumo/actions';

const theme = getTheme();

class AplicacaoInsumo extends React.PureComponent {

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Aplicação de Insumo',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });
  
  render() {
    return (
      <Container>
        <Content>
          <Text>Aplicação de Insumo</Text>
        </Content>
          <Fab
            active
            style={{ backgroundColor: theme.secondary }}
            containerStyle={{ }}
            onPress={() => this.props.navigation.navigate('FormAplicacaoInsumo')}
          >
            <Icon ios='ios-add' android="md-add" />
          </Fab>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllCentrosCusto: actionsCentroCusto.fetchAll,
    fetchAllOperacoes: actionsOperacao.fetchAll,
    fetchAllSistemasAplicacao: actionsSistemaAplicacao.fetchAll,
    fetchAllInsumos: actionsInsumo.fetchAll,
  }, dispatch
);

export default connect(null, mapDispatchToProps)(AplicacaoInsumo);
