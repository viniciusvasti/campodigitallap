import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/aplicacaoInsumo/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsCentroCusto from '../../redux/centroCusto/actions';
import actionsOperacao from '../../redux/operacao/actions';
import actionsSistemaAplicacao from '../../redux/sistemaAplicacao/actions';
import actionsInsumo from '../../redux/insumo/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

const optionsTurno = [
  { value: 'D', text: 'Dia' },
  { value: 'N', text: 'Noite' },
];

const optionsCondicaoSolo = [
  { value: 'S', text: 'Seco' },
  { value: 'U', text: 'Pouco Úmido' },
  { value: 'U', text: 'Úmido' },
];

const optionsPreparacaoSolo = [
  { value: 'B', text: 'Bom' },
  { value: 'R', text: 'Ruim' },
];

const optionsCondicaoTempo = [
  { value: 'L', text: 'Limpo' },
  { value: 'N', text: 'Nublado' },
  { value: 'P', text: 'Parcialmente Nublado' },
];

const optionsRestoVegetacao = [
  { value: 'A', text: 'Aceitável' },
  { value: 'P', text: 'Pouca' },
  { value: 'R', text: 'Prejudicial á Aplicação' },
];
class FormAplicacaoInsumo extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: {formfields: []},
      options: {
        fields: {
          dataAplicacao: {
            label: 'Data da Aplicação',
            error: 'Campo obrigatório!',
            mode: 'date',
            config: {
              format: (date) => moment(date).format('DD/MM/YYYY'),
            }
          },
          turno: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsTurno,
          },
          qntdProgHa: {
            label: 'Qtda. Prog/Há',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          condicaoSolo: {
            label: 'Condição do Solo',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsCondicaoSolo,
          },
          preparacaoSolo: {
            label: 'Preparação do Solo',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsPreparacaoSolo,
          },
          doseProgHa: {
            label: 'Dose Prog/Há',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          totaGeral: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          area: {
            label: 'Área',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          condicaoTempo: {
            label: 'Condição do Tempo',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsCondicaoTempo,
          },
          restoVegetacao: {
            label: 'Resto Vegetação',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsRestoVegetacao,
          },
          secao: {
            label: 'Seção',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: [],
          },
          talhao: {
            label: 'Talhão',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
          },
        }
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Aplicação de Insumo',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.props.fetchAllFazendas();
    this.props.fetchAllCentrosCusto();
    this.props.fetchAllOperacoes();
    this.props.fetchAllSistemasAplicacao();
    this.props.fetchAllInsumos();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }
    if (prevProps.centrosCusto !== this.props.centrosCusto) {
      this.mountSelectCentroCusto();
    }
    if (prevProps.operacoes !== this.props.operacoes) {
      this.mountSelectOperacao();
    }
    if (prevProps.sistemasAplicacao !== this.props.sistemasAplicacao) {
      this.mountSelectSistemasAplicacao();
    }
    if (prevProps.insumos !== this.props.insumos) {
      this.mountSelectInsumo();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  mountSelectCentroCusto = () => {
    const optionsCentrosCusto = this.props.centrosCusto ?
    this.props.centrosCusto
    .filter(cc => cc.ativo)
    .map(cc => (
      {
        value: `${cc.id}`,
        text: `${cc.numero} - ${cc.descricao}`,
      }
    )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          centroCusto: {
            label: 'Centro de Custo',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsCentrosCusto,
          },
        },
      }
    });
  }

  mountSelectOperacao = () => {
    const optionsOperacao = this.props.operacoes ?
      this.props.operacoes
      .filter(op => op.ativo)
      .map(op => (
        {
          value: `${op.id}`,
          text: `${op.numero} - ${op.descricao}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          operacao: {
            label: 'Operação',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsOperacao,
          },
        },
      }
    });
  }

  mountSelectSistemasAplicacao = () => {
    const optionsSistemasAplicacao = this.props.sistemasAplicacao ?
      this.props.sistemasAplicacao
      .filter(sa => sa.ativo)
      .map(sa => (
        {
          value: `${sa.id}`,
          text: `${sa.numero} - ${sa.descricao}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          sistemaDeAplicacao: {
            label: 'Sistema de Aplicação',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsSistemasAplicacao,
          },
        },
      }
    });
  }

  mountSelectInsumo = () => {
    const optionsInsumos = this.props.insumos ?
      this.props.insumos
      .filter(i => i.ativo)
      .map(i => (
        {
          value: `${i.id}`,
          text: `${i.numero} - ${i.descricao}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          insumo: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsInsumos,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      dataAplicacao: moment(value.dataAplicacao).format('DD/MM/YYYY'),
      hora: moment(value.hora).format('HH:mm'),
      fazenda: { id: value.fazenda },
      secao: { id: value.secao },
      talhao: { id: value.talhao },
      centroCusto: { id: value.centroCusto },
      operacao: { id: value.operacao },
      sistemaDeAplicacao: { id: value.sistemaDeAplicacao },
      insumo: { id: value.insumo },
    };
    console.log('value', dados);
    this.props.save(dados);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      dataAplicacao: t.Date,
      centroCusto: t.Str,
      operacao: t.Str,
      sistemaDeAplicacao: t.Str,
      turno: t.Str,
      qntdProgHa: t.Number,
      condicaoSolo: t.Str,
      preparacaoSolo: t.Str,
      insumo: t.Str,
      doseProgHa: t.Number,
      totaGeral: t.Number,
      area: t.Number,
      condicaoTempo: t.Str,
      restoVegetacao: t.Str,
    });
      
    const ApontCh2 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      dataAplicacao: t.Date,
      centroCusto: t.Str,
      operacao: t.Str,
      sistemaDeAplicacao: t.Str,
      turno: t.Str,
      qntdProgHa: t.Number,
      condicaoSolo: t.Str,
      preparacaoSolo: t.Str,
      insumo: t.Str,
      doseProgHa: t.Number,
      totaGeral: t.Number,
      area: t.Number,
      condicaoTempo: t.Str,
      restoVegetacao: t.Str,
    });
      
    const ApontCh3 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      talhao: t.Str,
      dataAplicacao: t.Date,
      centroCusto: t.Str,
      operacao: t.Str,
      sistemaDeAplicacao: t.Str,
      turno: t.Str,
      qntdProgHa: t.Number,
      condicaoSolo: t.Str,
      preparacaoSolo: t.Str,
      insumo: t.Str,
      doseProgHa: t.Number,
      totaGeral: t.Number,
      area: t.Number,
      condicaoTempo: t.Str,
      restoVegetacao: t.Str,
    });

    if (this.state.value && this.state.value.fazenda) {
      if (this.state.value.secao) {
        return ApontCh3;
      }
      return ApontCh2;
    }

    return ApontCh1;
  }

  onChange = (value) => {
    // TODO remover
    const optionsTalhoes = this.props.fazendas ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    if (value.fazenda && value.fazenda !== this.state.value.fazenda) {
      const fazenda = this.props.fazendas
      .filter(fazenda => fazenda.id == value.fazenda)[0];
      const optionsSecoes = fazenda.secoes ?
        fazenda.secoes
        .filter(secao => secao.ativo)
        .map(secao => (
          {
            value: `${secao.id}`,
            text: `${secao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.secao.options = optionsSecoes;
      this.setState({ value, options });
    } else if (value.secao && value.secao !== this.state.value.secao) {
      const fazenda = this.props.fazendas
        .filter(fazenda => fazenda.id == value.fazenda)[0];
      const secao = fazenda.secoes
        .filter(secao => secao.id == value.secao)[0];
      const optionsTalhoes = secao.talhoes ?
        secao.talhoes
        .filter(talhao => talhao.ativo)
        .map(talhao => (
          {
            value: `${talhao.id}`,
            text: `${talhao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.talhao.options = optionsTalhoes;
      this.setState({ value, options });
    }
  }
     
  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
            onChange={this.onChange}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.AplicacaoInsumo.resposta,
  tipoResposta: state.AplicacaoInsumo.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
  centrosCusto: state.CentroCusto.all,
  operacoes: state.Operacao.all,
  sistemasAplicacao: state.SistemaAplicacao.all,
  insumos: state.Insumo.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllCentrosCusto: actionsCentroCusto.fetchAll,
    fetchAllOperacoes: actionsOperacao.fetchAll,
    fetchAllSistemasAplicacao: actionsSistemaAplicacao.fetchAll,
    fetchAllInsumos: actionsInsumo.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormAplicacaoInsumo);
