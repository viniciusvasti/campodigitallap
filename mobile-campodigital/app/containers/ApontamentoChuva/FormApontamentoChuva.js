import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/apontamentoChuva/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

class FormApontamentoChuva extends React.PureComponent {

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Apontamento de Chuva',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.form.getComponent('elemento').refs.input.focus();
  }

  componentDidUpdate() {
    if (this.props.resposta) {
      console.log('this.props.resposta', this.props.resposta);
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      dataApontamento: moment(value.dataApontamento).format('DD/MM/YYYY'),
      hora: moment(value.hora).format('HH:mm'),
    };
    // console.log('value', apontamento);    
    this.props.save(dados);
  }
    
  render() {

    console.log('this.props.resposta', this.props.resposta);
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;
  
    const ApontCh = t.struct({
      dataApontamento: t.Date,
      hora: t.Date,
      elemento: t.Number,
      posto: t.Number,
      leitura: t.Number,
    });

    const options = {
      fields: {
        dataApontamento: {
          onSubmitEditing: () => this.form.getComponent('hora').refs.input.focus(),
          label: 'Data do Apontamento',
          error: 'Campo obrigatório!',
          mode: 'date',
          config: {
            format: (date) => moment(date).format('DD/MM/YYYY'),
          }
        },
        hora: {
          onSubmitEditing: () => this.form.getComponent('elemento').refs.input.focus(),
          error: 'Campo obrigatório!',
          mode: 'time',
          config: {
            format: (date) => moment(date).format('HH:mm'),
          }
        },
        elemento: {
          onSubmitEditing: () => this.form.getComponent('posto').refs.input.focus(),
          error: 'Campo obrigatório!',
        },
        posto: {
          onSubmitEditing: () => this.form.getComponent('leitura').refs.input.focus(),
          error: 'Campo obrigatório!',
        },
        leitura: {
          error: 'Campo obrigatório!',
        },
      },
    };

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={ApontCh}
            options={options}
            stylesheet={FormStyles}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.ApontamentoChuva.resposta,
  tipoResposta: state.ApontamentoChuva.tipoResposta,
  loading: state.ApontamentoChuva.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormApontamentoChuva);
