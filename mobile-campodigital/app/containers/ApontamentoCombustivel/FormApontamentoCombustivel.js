import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/apontamentoCombustivel/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsEquipamento from '../../redux/equipamento/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

class FormApontamentoCombustivel extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: {formfields: []},
      options: {
        fields: {
          dataApontamento: {
            label: 'Data do Apontamento',
            error: 'Campo obrigatório!',
            mode: 'date',
            config: {
              format: (date) => moment(date).format('DD/MM/YYYY'),
            }
          },
          horaApontamento: {
            label: 'Horário',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            mode: 'time',
            config: {
              format: (date) => moment(date).format('HH:mm'),
            }
          },
          operador: {
            error: 'Campo obrigatório!',
          },
          pontoDeLubrificacao: {
            label: 'Ponto de Lubrificação',
            error: 'Campo obrigatório!',
          },
          combustivel: {
            label: 'Combustível',
            error: 'Campo obrigatório!',
          },
          quantidade: {
            error: 'Campo obrigatório!',
          },
          odometro: {
            label: 'Horim/Odôm',
            error: 'Campo obrigatório!',
          },
        }
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Apontamento de Combustível',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.props.fetchAllFazendas();
    this.props.fetchAllEquipamentos();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }
    if (prevProps.equipamentos !== this.props.equipamentos) {
      this.mountSelectEquipamento();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  mountSelectEquipamento = () => {
    const optionsEquipamentos = this.props.equipamentos ?
    this.props.equipamentos
    .filter(eq => eq.ativo)
    .map(eq => (
      {
        value: `${eq.id}`,
        text: `${eq.numero} - ${eq.descricao}`,
      }
    )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          equipamento: {
            label: 'Equipamento',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsEquipamentos,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      dataApontamento: moment(value.dataApontamento).format('DD/MM/YYYY'),
      horaApontamento: moment(value.horaApontamento).format('HH:mm'),
      fazenda: { id: value.fazenda },
      equipamento: { id: value.equipamento },
    };
    console.log('value', dados);
    this.props.save(dados);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      dataApontamento: t.Date,
      equipamento: t.Str,
      horaApontamento: t.Date,
      pontoDeLubrificacao: t.String,
      operador: t.Number,
      combustivel: t.Number,
      quantidade: t.Number,
      odometro: t.Number,
    });

    return ApontCh1;
  }
     
  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.ApontamentoCombustivel.resposta,
  tipoResposta: state.ApontamentoCombustivel.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
  equipamentos: state.Equipamento.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllEquipamentos: actionsEquipamento.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormApontamentoCombustivel);
