import React from 'react';
import moment from 'moment';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, Content, Fab, Icon } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { DrawerButton } from '../../components';
import { getTheme } from '../../config/styles';
import actions from '../../redux/escala/actions';

const theme = getTheme();

class Escala extends React.PureComponent {

  componentDidMount() {
    this.props.fetchAll();
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Escala',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });
  
  renderEscalas(escalas) {
    return escalas.map((escala, index) => (
      <Grid
        key={escala.id}
        style={{
          padding: 8,
          borderWidth: 1,
        }}
      >
        <Row>
          <Col>
            <Text>Fazenda: {escala.nomeFazenda}</Text>
          </Col>
          <Col>
            <Text>Número da Fazenda: {escala.numeroFazenda}</Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <Text>Seção: {escala.secao}</Text>
          </Col>
          <Col>
            <Text>Talhão: {escala.talhao}</Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <Text>Atividade: {escala.atividade}</Text>
          </Col>
          <Col>
            <Text>Coletor: {escala.coletor}</Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <Text>Centro de Custo: {escala.centroCusto}</Text>
          </Col>
          <Col>
            <Text>Complemento: {escala.complemento}</Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <Text>Equipamentos:&nbsp;
            {
              escala.equipamentos.map((equip, index) => {
                return (
                  equip.numero + (index === escala.equipamentos.length-1 ? '' : ', ')
                );
              })
            }
            </Text>
          </Col>
        </Row>
      </Grid>
    ));
  }

  render() {
    return (
      <Container>
        <Content>
          <Grid style={{ padding: 5 }} >
            <Row>
              <Col>
                <Text style={{ fontWeight: 'bold' }} >
                  ESCALA DE TROCA
                </Text>
              </Col>
              <Col>
                <Text style={{ fontWeight: 'bold' }} >
                  DATA: {moment(new Date()).format('DD/MM/YYYY')}
                </Text>
              </Col>
            </Row>
          </Grid>
          {this.renderEscalas(this.props.escalas)}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.Escala.resposta,
  tipoResposta: state.Escala.tipoResposta,
  loading: state.Escala.loading,
  escalas: state.Escala.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchAll: actions.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Escala);
