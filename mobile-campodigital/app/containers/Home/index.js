import React from 'react';
import { DrawerNavigator } from 'react-navigation';
import Home from './Home.js';
import MapasTalhao from '../MapasTalhao';
import Escala from '../Escala';
import ApontamentoChuva from '../ApontamentoChuva';
import AplicacaoInsumo from '../AplicacaoInsumo';
import ApontamentoArea from '../ApontamentoArea';
import ApontamentoAreaPreparo from '../ApontamentoAreaPreparo';
import ApontamentoAtividadeMecanizada from '../ApontamentoAtividadeMecanizada';
import ApontamentoAtividadesManuais from '../ApontamentoAtividadesManuais';
import ApontamentoCombustivel from '../ApontamentoCombustivel';
import ApontamentoLubrificante from '../ApontamentoLubrificante';
import ApontamentoMoagemDiaria from '../ApontamentoMoagemDiaria';
import ApontamentoPlantio from '../ApontamentoPlantio';
import ApontamentoTerceirosMecanizado from '../ApontamentoTerceirosMecanizado';
import FechamentoCorte from '../FechamentoCorte';
import OrdemCorte from '../OrdemCorte';
import OrdemServico from '../OrdemServico';
import Ocorrencia from '../Ocorrencia';
import SideBar from '../SideBar/SideBar';

const HomeRouter = DrawerNavigator(
  {
    Home: { screen: Home },
    Ocorrencia,
    MapasTalhao,
    Escala,
    AplicacaoInsumo: { screen: AplicacaoInsumo },
    ApontamentoChuva: { screen: ApontamentoChuva },
    ApontamentoArea: { screen: ApontamentoArea },
    ApontamentoAreaPreparo: { screen: ApontamentoAreaPreparo },
    ApontamentoAtividadeMecanizada: { screen: ApontamentoAtividadeMecanizada },
    ApontamentoAtividadesManuais: { screen: ApontamentoAtividadesManuais },
    ApontamentoCombustivel: { screen: ApontamentoCombustivel },
    ApontamentoLubrificante: { screen: ApontamentoLubrificante },
    ApontamentoMoagemDiaria: { screen: ApontamentoMoagemDiaria },
    ApontamentoPlantio: { screen: ApontamentoPlantio },
    ApontamentoTerceirosMecanizado: { screen: ApontamentoTerceirosMecanizado },
    FechamentoCorte: { screen: FechamentoCorte },
    OrdemCorte: { screen: OrdemCorte },
    OrdemServico: { screen: OrdemServico },
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);
export default HomeRouter;
