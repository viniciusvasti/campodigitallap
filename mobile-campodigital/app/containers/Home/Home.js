import React, { Component } from 'react';
import { Container, Body, Content, Text, Card, CardItem } from 'native-base';
import { DrawerButton } from '../../components';

class Home extends Component {

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Home',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });

  render() {
    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>Bem vindo ao CampoDigital!</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default Home;
