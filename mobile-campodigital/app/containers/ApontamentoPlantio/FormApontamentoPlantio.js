import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/apontamentoPlantio/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsCicloDesenvolvimento from '../../redux/cicloDesenvolvimento/actions';
import actionsSistemaPlantio from '../../redux/sistemaPlantio/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

class FormApontamentoPlantio extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: {formfields: []},
      options: {
        fields: {
          dataApontamento: {
            label: 'Data do Apontamento',
            error: 'Campo obrigatório!',
            mode: 'date',
            config: {
              format: (date) => moment(date).format('DD/MM/YYYY'),
            }
          },
          variedade: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          espacamento: {
            label: 'Espaçamento',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          area: {
            label: 'Área',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          replantacao: {
            label: 'Replantação?',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          localConcluido: {
            label: 'Local Concluído?',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          procedenciaMuda: {
            label: 'Procedência (muda)',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          secaoMuda: {
            label: 'Seção (muda)',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          talhaoMuda: {
            label: 'Talhão (muda)',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          nrCargas: {
            label: 'Nº de Cargas',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          pesoMedioKg: {
            label: 'Peso Médio(Kg)',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          nrEquipes: {
            label: 'Nº de Equipes',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          nrCaminhoes: {
            label: 'Nº de Caminhões',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          distanciaMuda: {
            label: 'Distância da Muda',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          area: {
            label: 'Área',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          area: {
            label: 'Área',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          secao: {
            label: 'Seção',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: [],
          },
          talhao: {
            label: 'Talhão',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
          },
        }
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Apontamento de Plantio',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.props.fetchAllFazendas();
    this.props.fetchAllCiclosDesenvolvimento();
    this.props.fetchAllSistemasPlantio();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }
    if (prevProps.ciclosDesenvolvimento !== this.props.ciclosDesenvolvimento) {
      this.mountSelectCicloDesenvolvimento();
    }
    if (prevProps.sistemasPlantio !== this.props.sistemasPlantio) {
      this.mountSelectSistemasPlantio();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  mountSelectCicloDesenvolvimento = () => {
    const optionsCiclosDesenvolvimento = this.props.ciclosDesenvolvimento ?
    this.props.ciclosDesenvolvimento
    .filter(cc => cc.ativo)
    .map(cc => (
      {
        value: `${cc.id}`,
        text: `${cc.numero} - ${cc.descricao}`,
      }
    )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          cicloDeDesenvolvimento: {
            label: 'Ciclo de Desenvolvimento',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsCiclosDesenvolvimento,
          },
        },
      }
    });
  }

  mountSelectSistemasPlantio = () => {
    const optionsSistemasPlantio = this.props.sistemasPlantio ?
      this.props.sistemasPlantio
      .filter(sa => sa.ativo)
      .map(sa => (
        {
          value: `${sa.id}`,
          text: `${sa.numero} - ${sa.descricao}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          sistemaDePlantio: {
            label: 'Sistema de Plantio',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsSistemasPlantio,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      dataApontamento: moment(value.dataApontamento).format('DD/MM/YYYY'),
      fazenda: { id: value.fazenda },
      secao: { id: value.secao },
      talhao: { id: value.talhao },
      cicloDeDesenvolvimento: { id: value.cicloDeDesenvolvimento },
      sistemaDePlantio: { id: value.sistemaDePlantio },
    };
    console.log('value', dados);
    this.props.save(dados);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      dataApontamento: t.Date,
      cicloDeDesenvolvimento: t.Str,
      sistemaDePlantio: t.Str,
      variedade: t.Number,
      espacamento: t.Number,
      area: t.Number,
      replantacao: t.Boolean,
      localConcluido: t.Boolean,
      procedenciaMuda: t.Number,
      secaoMuda: t.Number,
      talhaoMuda: t.Number,
      nrCargas: t.Number,
      pesoMedioKg: t.Number,
      nrEquipes: t.Number,
      nrCaminhoes: t.Number,
      distanciaMuda: t.Number,
    });
      
    const ApontCh2 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      dataApontamento: t.Date,
      cicloDeDesenvolvimento: t.Str,
      sistemaDePlantio: t.Str,
      variedade: t.Number,
      espacamento: t.Number,
      area: t.Number,
      replantacao: t.Boolean,
      localConcluido: t.Boolean,
      procedenciaMuda: t.Number,
      secaoMuda: t.Number,
      talhaoMuda: t.Number,
      nrCargas: t.Number,
      pesoMedioKg: t.Number,
      nrEquipes: t.Number,
      nrCaminhoes: t.Number,
      distanciaMuda: t.Number,
    });
      
    const ApontCh3 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      talhao: t.Str,
      dataApontamento: t.Date,
      cicloDeDesenvolvimento: t.Str,
      sistemaDePlantio: t.Str,
      variedade: t.Number,
      espacamento: t.Number,
      area: t.Number,
      replantacao: t.Boolean,
      localConcluido: t.Boolean,
      procedenciaMuda: t.Number,
      secaoMuda: t.Number,
      talhaoMuda: t.Number,
      nrCargas: t.Number,
      pesoMedioKg: t.Number,
      nrEquipes: t.Number,
      nrCaminhoes: t.Number,
      distanciaMuda: t.Number,
    });

    if (this.state.value && this.state.value.fazenda) {
      if (this.state.value.secao) {
        return ApontCh3;
      }
      return ApontCh2;
    }

    return ApontCh1;
  }

  onChange = (value) => {
    // TODO remover
    const optionsTalhoes = this.props.fazendas ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    if (value.fazenda && value.fazenda !== this.state.value.fazenda) {
      const fazenda = this.props.fazendas
      .filter(fazenda => fazenda.id == value.fazenda)[0];
      const optionsSecoes = fazenda.secoes ?
        fazenda.secoes
        .filter(secao => secao.ativo)
        .map(secao => (
          {
            value: `${secao.id}`,
            text: `${secao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.secao.options = optionsSecoes;
      this.setState({ value, options });
    } else if (value.secao && value.secao !== this.state.value.secao) {
      const fazenda = this.props.fazendas
        .filter(fazenda => fazenda.id == value.fazenda)[0];
      const secao = fazenda.secoes
        .filter(secao => secao.id == value.secao)[0];
      const optionsTalhoes = secao.talhoes ?
        secao.talhoes
        .filter(talhao => talhao.ativo)
        .map(talhao => (
          {
            value: `${talhao.id}`,
            text: `${talhao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.talhao.options = optionsTalhoes;
      this.setState({ value, options });
    }
  }
     
  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
            onChange={this.onChange}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.ApontamentoPlantio.resposta,
  tipoResposta: state.ApontamentoPlantio.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
  ciclosDesenvolvimento: state.CicloDesenvolvimento.all,
  sistemasPlantio: state.SistemaPlantio.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllCiclosDesenvolvimento: actionsCicloDesenvolvimento.fetchAll,
    fetchAllSistemasPlantio: actionsSistemaPlantio.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormApontamentoPlantio);
