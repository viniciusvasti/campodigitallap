import React from 'react';
import { Text, AsyncStorage, Alert, View, Image } from 'react-native';
import t from 'tcomb-form-native';
import { Container, Content, Button } from 'native-base';
import axios from 'axios';
import { Spinner, ErrorMessage } from '../../components';
import styles from './styles';
import { Constants } from '../../config/constants';
import logoImg from '../../resources/images/logo.png';
import { getTheme } from '../../config/styles';

const theme = getTheme();

class LoginForm extends React.PureComponent {
  
  static navigationOptions = {
      title: 'LOGIN'
  };

  constructor(props) {
    super(props);
    this.state = { error: '', loading: false };
  }

  componentDidMount() {
    // give focus to the name textbox
    this.form.getComponent('username').refs.input.focus();
  }

  onLoginSuccess = (token) => {
    try {
      console.log('storing token');
      AsyncStorage.setItem('@token', token)
        .then(() =>
          console.log('token stored')
        );
      Alert.alert('You are Logged In');
      this.setState({ loading: false });
      this.props.navigation.navigate('AppLoading');
    } catch (error) {
      console.error(`${error}`);
      Alert.alert('Error', 'Oops! Something gone wrong.');
    }
  }

  onLoginFail = (response) => {
    // console.error(`${JSON.stringify(response.response)}`);
    try {
      if (response.response && response.response.status === 403) {
        this.setState({
          error: 'Usuário e/ou senha estão incorretos.',
          loading: false
        });
      } else {
        //Alert.alert('Error', `${response.response.data} - Oops! Something gone wrong.`);
        this.setState({ error: response.response.data, loading: false });
      }
    } catch (error) {
      //console.error(`${error}`);
      this.setState({ error: 'Unavailable Server. Please try again.', loading: false });
    }
  }

  login = () => {
    const value = this.form.getValue();
    if (!value) return;
    this.setState({ error: '', loading: true });

    axios.post(`${Constants.apiUrl}login`, {
        username: value.username.trim(),
        password: value.password
      })
      .then(async (response) => {
        console.log(response.status);
        if (response.headers.authorization) {
          this.onLoginSuccess(response.headers.authorization);
        } else {
          console.log(`${response}`);
          Alert.alert('Error', 'Oops! Something gone wrong.');
        }
      })
      .catch(response => {
        console.log('post', response);
        this.onLoginFail(response);
      });
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button full style={styles.loginButton} onPress={this.login}>
        <Text>Login</Text>
      </Button>
    );
  }

  renderError() {
    if (!this.state.error) {
      return;
    }

    return (
      <ErrorMessage message={this.state.error} />
    );
  }

  render() {
    const Form = t.form.Form;
  
    const User = t.struct({
      username: t.String,
      password: t.String,
    });
  
    const options = {
      fields: {
        username: {
          onSubmitEditing: () => this.form.getComponent('password').refs.input.focus(),
          error: 'Campo obrigatório!',
        },
        password: {
          onSubmitEditing: this.login,
          label: 'Senha',
          error: 'Campo obrigatório!',
          type: 'password',
        },
      },
    };

    const formStyles = {
      ...Form.stylesheet,
      controlLabel: {
        normal: {
          fontSize: 18,
          color: theme.danger,
        },
        error: {
          color: theme.danger,
        },
      },
      textbox: {
        normal: {
          fontSize: 18,
          padding: 6,
          backgroundColor: '#60ad5e',
          borderRadius: 4,
        }
      },
    };

    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <View style={styles.logoContainer}>
            <Image
              source={logoImg}
              style={styles.logo}
            />
          </View>
          <Form
            ref={c => (this.form = c)}
            type={User}
            options={options}
            stylesheet={formStyles}
          />
          {this.renderError()}
          {this.renderButton()}
        </Content>
      </Container>
    );
  }
}

export default LoginForm;
