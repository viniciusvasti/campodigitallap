import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        justifyContent: 'center',
        padding: 20,
        backgroundColor: '#2e7d32',
    },
    content: {
        paddingTop: 20,
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
    },
    logo: {
        width: 100,
        height: 100,
        borderRadius: 10,
    },
    loginButton: {
        backgroundColor: '#fbc02d',
        // backgroundColor: '#1a237e',
    },
});
