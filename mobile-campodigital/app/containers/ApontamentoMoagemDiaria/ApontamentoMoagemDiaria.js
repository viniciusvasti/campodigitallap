import React from 'react';
import { Text, } from 'react-native';
import { Container, Content, Fab, Icon } from 'native-base';
import { DrawerButton } from '../../components';
import { getTheme } from '../../config/styles';

const theme = getTheme();

class ApontamentoMoagemDiaria extends React.PureComponent {

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Apontamentos de Moagem Diária',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });
  
  render() {
    return (
      <Container>
        <Content>
          <Text>Apontamentos de Moagem Diária</Text>
        </Content>
          <Fab
            active
            style={{ backgroundColor: theme.secondary }}
            containerStyle={{ }}
            onPress={() => this.props.navigation.navigate('FormApontamentoMoagemDiaria')}
          >
            <Icon ios='ios-add' android="md-add" />
          </Fab>
      </Container>
    );
  }
}

export default ApontamentoMoagemDiaria;
