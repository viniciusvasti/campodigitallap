import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/apontamentoMoagemDiaria/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

class FormApontamentoMoagemDiaria extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: {formfields: []},
      options: {
        fields: {
          dataApontamento: {
            label: 'Data do Apontamento',
            error: 'Campo obrigatório!',
            mode: 'date',
            config: {
              format: (date) => moment(date).format('DD/MM/YYYY'),
            }
          },
          convencionalCrua: {
            error: 'Campo obrigatório!',
          },
          mecanizadaCrua: {
            error: 'Campo obrigatório!',
          },
          convencionalQueimada: {
            error: 'Campo obrigatório!',
          },
          mecanizadaQueimada: {
            error: 'Campo obrigatório!',
          },
        }
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Apontamento de Moagem Diária',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.props.fetchAllFazendas();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      dataApontamento: moment(value.dataApontamento).format('DD/MM/YYYY'),
      fazenda: { id: value.fazenda },
    };
    console.log('value', dados);
    this.props.save(dados);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      dataApontamento: t.Date,
      convencionalCrua: t.Number,
      mecanizadaCrua: t.Number,
      convencionalQueimada: t.Number,
      mecanizadaQueimada: t.Number,
    });

    return ApontCh1;
  }
     
  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.ApontamentoMoagemDiaria.resposta,
  tipoResposta: state.ApontamentoMoagemDiaria.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormApontamentoMoagemDiaria);
