import React from 'react';
import { Text, } from 'react-native';
import { Container, Content, Fab, Icon } from 'native-base';
import { DrawerButton } from '../../components';
import { getTheme } from '../../config/styles';

const theme = getTheme();

class ApontamentoArea extends React.PureComponent {

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Apontamentos de Área',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });
  
  render() {
    return (
      <Container>
        <Content>
          <Text>Apontamentos de Area</Text>
        </Content>
          <Fab
            active
            style={{ backgroundColor: theme.secondary }}
            containerStyle={{ }}
            onPress={() => this.props.navigation.navigate('FormApontamentoArea')}
          >
            <Icon ios='ios-add' android="md-add" />
          </Fab>
      </Container>
    );
  }
}

export default ApontamentoArea;
