import React, { Component } from 'react';
import { View, TouchableOpacity, Modal, StyleSheet, Dimensions } from 'react-native';
import { 
  Container,
  Header,
  Content,
  Footer,
  Text, } from 'native-base';
// import Pdf from 'react-native-pdf';
import { getTheme, } from '../../config/styles';

const theme = getTheme();

class ModalFile extends Component {
  render() {
    return (
      <Modal
        transparent
        animationType="slide"
        visible={this.props.modalVisible}
        onRequestClose={() => {
          alert('Modal has been closed.');
        }}
      >
        <Container
          style={{
            backgroundColor: '#FFF'
          }}
        >
          <Header style={{ backgroundColor: theme.primary }}>
            <Text
              style={{ textAlign: 'left', fontWeight: 'bold', fontSize: 18, alignSelf: 'center', color: '#FFF' }}
            >
              Novo lançamento financeiro:
            </Text>
          </Header>
          <Content contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}>
            {/* <Pdf
              source={source}
              onLoadComplete={(numberOfPages,filePath)=>{
                  console.log(`number of pages: ${numberOfPages}`);
              }}
              onPageChanged={(page,numberOfPages)=>{
                  console.log(`current page: ${page}`);
              }}
              onError={(error)=>{
                  console.log(error);
              }}
              style={styles.pdf}
            /> */}
          </Content>
          <Footer>
            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
                height: 60,
                backgroundColor: theme.primary,
              }}
              onPress={() => {
                this.props.close();
              }}
            >
              <Text style={{ fontSize: 20, color: '#FFF' }}>Fechar</Text>
            </TouchableOpacity>
          </Footer>
        </Container>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});

export default ModalFile;
