import React, { Component } from 'react';
import { View, TouchableOpacity, AsyncStorage } from 'react-native';
import { Text, } from 'native-base';
import axios from 'axios';
import ModalFile from './ModalFile';
import { Constants } from '../../config/constants';

const { apiUrl } = Constants;

class Item extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      fileData: '',
    };
  }

  setModalVisible = async () => {
    const navigate = this.props.navigate;
    const userToken = await AsyncStorage.getItem('@token');
    axios.get(apiUrl+'mapas-talhao/'+this.props.mapa.mapaPDF.fileName+'/base64', {
      headers: {
        Authorization: userToken,
      },
      // responseType: 'blob',
    })
    .then(response => {
      navigate('PDFFile', { fileData: response.data })
    })
    .catch(error => {
      console.log(error);
    });
  }

  render() {
    const { numero, fazenda, secao, mapaPDF } = this.props.mapa;
    return (
      <View
        style={{ flexDirection: 'row', marginRight: 10, borderBottomWidth: 1 }}
      >
        <ModalFile
          modalVisible={this.state.modalVisible}
          fileData={this.state.fileData}
          close={() => this.setModalVisible(false)}
        />
        <View style={{ flex: 15, margin: 15, }}>
          <View>
            <Text style={{ fontSize: 15 }}>
              Fazenda: <Text style={{ fontWeight: 'bold', marginLeft: 5, fontSize: 15, }}>
                  {fazenda}
                </Text>
            </Text>
            <Text style={{ fontSize: 15 }}>
              Seção: <Text style={{ fontWeight: 'bold', marginLeft: 5, fontSize: 15, }}>
                  {secao}
                </Text>
            </Text>
            <Text style={{ fontSize: 15 }}>
              Talhão: <Text style={{ fontWeight: 'bold', marginLeft: 5, fontSize: 15, }}>
                  {numero}
                </Text>
            </Text>
            <TouchableOpacity onPress={() => this.setModalVisible()}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  color: '#060',
                }}
              >
                Abrir mapa {mapaPDF.fileName}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default Item;
