import React, { Component } from 'react';
import { View, StyleSheet, } from 'react-native';
import PDFReader from 'rn-pdf-reader-js';
import { BackButton } from '../../components';

class PDFFile extends Component {

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Mapa',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  render() {
    const { fileData } = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <PDFReader
          source={{ base64: 'data:application/pdf;base64,' + fileData }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});

export default PDFFile;
