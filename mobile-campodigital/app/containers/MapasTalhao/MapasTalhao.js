import React from 'react';
import moment from 'moment';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, Content, Fab, Icon } from 'native-base';
import { DrawerButton } from '../../components';
import { getTheme } from '../../config/styles';
import actions from '../../redux/mapasTalhao/actions';
import Item from './Item';

const theme = getTheme();

class MapasTalhao extends React.PureComponent {

  componentDidMount() {
    this.props.fetchAll();
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Mapas',
      headerLeft: (
        <DrawerButton navigation={navigation} />
      ),
    });

  render() {
    return (
      <Container>
        <Content>
          {
            this.props.mapas.map(mapa =>
              <Item mapa={mapa} key={mapa.id} navigate={this.props.navigation.navigate} />)
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.MapasTalhao.resposta,
  tipoResposta: state.MapasTalhao.tipoResposta,
  loading: state.MapasTalhao.loading,
  mapas: state.MapasTalhao.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchAll: actions.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(MapasTalhao);
