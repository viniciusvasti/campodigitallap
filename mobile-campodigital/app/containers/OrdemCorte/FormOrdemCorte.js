import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/ordemCorte/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsSistemaCorte from '../../redux/sistemaCorte/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

const optionsTotalParcial = [
  { value: 'T', text: 'Total' },
  { value: 'P', text: 'Parcial' },
];

class FormOrdemCorte extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: {formfields: []},
      options: {
        fields: {
          dataCorte: {
            label: 'Data da Ordem de Corte',
            error: 'Campo obrigatório!',
            mode: 'date',
            config: {
              format: (date) => moment(date).format('DD/MM/YYYY'),
            }
          },
          hora: {
            label: 'Hora da Ordem de Corte',
            error: 'Campo obrigatório!',
            mode: 'time',
            config: {
              format: (date) => moment(date).format('HH:mm'),
            }
          },
          area: {
            label: 'Área',
            error: 'Campo obrigatório!',
          },
          estimativaTonelada: {
            label: 'Estimativa (Tonelada)',
            error: 'Campo obrigatório!',
          },
          totalOuParcial: {
            label: 'Total ou Parcial',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsTotalParcial,
          },
          secao: {
            label: 'Seção',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: [],
          },
          talhao: {
            label: 'Talhão',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
          },
        }
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Ordem de Corte',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.props.fetchAllFazendas();
    this.props.fetchAllSistemasCorte();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }
    if (prevProps.sistemasCorte !== this.props.sistemasCorte) {
      this.mountSelectSistemasCorte();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  mountSelectSistemasCorte = () => {
    const optionsSistemasCorte = this.props.sistemasCorte ?
      this.props.sistemasCorte
      .filter(sa => sa.ativo)
      .map(sa => (
        {
          value: `${sa.id}`,
          text: `${sa.numero} - ${sa.descricao}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          sistemaDeCorte: {
            label: 'Sistema de Corte',
            onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsSistemasCorte,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      dataCorte: moment(value.dataCorte).format('DD/MM/YYYY'),
      hora: moment(value.hora).format('HH:mm'),
      fazenda: { id: value.fazenda },
      secao: { id: value.secao },
      talhao: { id: value.talhao },
      sistemaDeCorte: { id: value.sistemaDeCorte },
    };
    console.log('value', dados);
    this.props.save(dados);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      dataCorte: t.Date,
      hora: t.Date,
      sistemaDeCorte: t.Str,
      totalOuParcial: t.String,
      area: t.Number,
      estimativaTonelada: t.Number,
    });
      
    const ApontCh2 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      dataCorte: t.Date,
      hora: t.Date,
      sistemaDeCorte: t.Str,
      totalOuParcial: t.String,
      area: t.Number,
      estimativaTonelada: t.Number,
    });
      
    const ApontCh3 = t.struct({
      fazenda: t.Str,
      secao: t.Str,
      talhao: t.Str,
      dataCorte: t.Date,
      hora: t.Date,
      sistemaDeCorte: t.Str,
      totalOuParcial: t.String,
      area: t.Number,
      estimativaTonelada: t.Number,
    });

    if (this.state.value && this.state.value.fazenda) {
      if (this.state.value.secao) {
        return ApontCh3;
      }
      return ApontCh2;
    }

    return ApontCh1;
  }

  onChange = (value) => {
    // TODO remover
    const optionsTalhoes = this.props.fazendas ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    if (value.fazenda && value.fazenda !== this.state.value.fazenda) {
      const fazenda = this.props.fazendas
      .filter(fazenda => fazenda.id == value.fazenda)[0];
      const optionsSecoes = fazenda.secoes ?
        fazenda.secoes
        .filter(secao => secao.ativo)
        .map(secao => (
          {
            value: `${secao.id}`,
            text: `${secao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.secao.options = optionsSecoes;
      this.setState({ value, options });
    } else if (value.secao && value.secao !== this.state.value.secao) {
      const fazenda = this.props.fazendas
        .filter(fazenda => fazenda.id == value.fazenda)[0];
      const secao = fazenda.secoes
        .filter(secao => secao.id == value.secao)[0];
      const optionsTalhoes = secao.talhoes ?
        secao.talhoes
        .filter(talhao => talhao.ativo)
        .map(talhao => (
          {
            value: `${talhao.id}`,
            text: `${talhao.numero}`,
          }
        )) : [];

      options = this.state.options;
      options.fields.talhao.options = optionsTalhoes;
      this.setState({ value, options });
    }
  }
     
  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
            onChange={this.onChange}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.OrdemCorte.resposta,
  tipoResposta: state.OrdemCorte.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
  sistemasCorte: state.SistemaCorte.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllSistemasCorte: actionsSistemaCorte.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormOrdemCorte);
