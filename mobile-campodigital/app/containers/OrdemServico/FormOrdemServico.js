import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import t from 'tcomb-form-native';
import { Text, View, } from 'react-native';
import { Container, Content, Button, Toast } from 'native-base';
import actions from '../../redux/ordemServico/actions';
import actionsFazenda from '../../redux/fazenda/actions';
import actionsEquipamento from '../../redux/equipamento/actions';
import { BackButton } from '../../components';
import { GlobalStyles, FormStyles } from '../../config/styles';

class FormOrdemServico extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: {formfields: []},
      options: {
        fields: {
          dataSaida: {
            onSubmitEditing: () => this.form.getComponent('hora').refs.input.focus(),
            label: 'Data do Apontamento',
            error: 'Campo obrigatório!',
            mode: 'date',
            config: {
              format: (date) => moment(date).format('DD/MM/YYYY'),
            }
          },
          horaSaida: {
            onSubmitEditing: () => this.form.getComponent('elemento').refs.input.focus(),
            error: 'Campo obrigatório!',
            mode: 'time',
            config: {
              format: (date) => moment(date).format('HH:mm'),
            }
          },
          origem: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          objeto: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          classificacaoManutencao: {
            label: 'Classificação da Manutenção',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          motivo: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          saidaPrevista: {
            label: 'Saída Prevista',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          kmHEquipamento: {
            label: 'Km/H equipamento',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          pontoDeManutencao: {
            label: 'Ponto de Manutenção',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          modosDeOperacao: {
            label: 'Modos de Operação',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          setoresDaOficina: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          observacao: {
            label: 'Observação',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
          servico: {
            label: 'Serviço',
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
          },
        }
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
      headerTitle: 'Abertura de OS',
      headerLeft: (
        <BackButton navigation={navigation} />
      ),
    });

  componentDidMount() {
    this.props.fetchAllFazendas();
    this.props.fetchAllEquipamentos();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fazendas !== this.props.fazendas) {
      this.mountSelectFazenda();
    }
    if (prevProps.equipamentos !== this.props.equipamentos) {
      this.mountSelectEquipamento();
    }

    if (this.props.resposta) {
      const erro = this.props.tipoResposta !== 'success' ? 'Erro: ' : '';
      Toast.show({
        text: erro + this.props.resposta,
        duration: 5000
      });
      this.props.clearResposta();
      if (this.props.tipoResposta === 'success') {
        this.props.navigation.goBack();
      }
    }
  }

  mountSelectFazenda = () => {
    const optionsFazendas = this.props.fazendas.length ?
      this.props.fazendas
      .filter(fazenda => fazenda.ativo)
      .map(fazenda => (
        {
          value: `${fazenda.id}`,
          text: `${fazenda.numero} - ${fazenda.nome}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          fazenda: {
            label: 'Fazenda',
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsFazendas,
          },
        },
      }
    });
  }

  mountSelectEquipamento = () => {
    const optionsEquipamentos = this.props.equipamentos ?
      this.props.equipamentos
      .filter(i => i.ativo)
      .map(i => (
        {
          value: `${i.id}`,
          text: `${i.numero} - ${i.descricao}`,
        }
      )) : [];

    this.setState({
      options: {
        fields: {
          ...this.state.options.fields,
          equipamento: {
            // onSubmitEditing: () => this.form.getComponent('').refs.input.focus(),
            error: 'Campo obrigatório!',
            factory: t.form.Select,
            options: optionsEquipamentos,
          },
        },
      }
    });
  }

  submit = () => {
    const value = this.form.getValue();
    if (!value) return;
    const dados = {
      ...value,
      fazenda: { id: value.fazenda },
      equipamento: { id: value.equipamento },
      saidaPrevista: moment(value.dataSaida).format('DD/MM/YYYY') + ' ' + moment(value.horaSaida).format('HH:mm')
    };
    console.log('value', dados);
    this.props.save(dados);
  }

  createFields = () => {
    const ApontCh1 = t.struct({
      fazenda: t.Str,
      equipamento: t.Str,
      dataSaida: t.Date,
      horaSaida: t.Date,
      origem: t.String,
      objeto: t.Number,
      classificacaoManutencao: t.Number,
      motivo: t.String,
      kmHEquipamento: t.Number,
      pontoDeManutencao: t.Number,
      modosDeOperacao: t.Number,
      setoresDaOficina: t.Number,
      observacao: t.String,
      servico: t.String,
    });

    return ApontCh1;
  }
     
  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
          }}
        >
          <Text>Loading...</Text>
        </View>
      );
    }

    const Form = t.form.Form;

    return (
      <Container style={GlobalStyles.container}>
        <Content style={GlobalStyles.content}>
          <Form
            ref={c => (this.form = c)}
            type={this.createFields()}
            value={this.state.value}
            options={this.state.options}
            stylesheet={FormStyles}
            onChange={this.onChange}
          />
          <Button full style={GlobalStyles.submitButton} onPress={this.submit}>
            <Text style={GlobalStyles.submitButtonText} >SALVAR</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  resposta: state.OrdemServico.resposta,
  tipoResposta: state.OrdemServico.tipoResposta,
  loading: state.Fazenda.loading,
  fazendas: state.Fazenda.all,
  equipamentos: state.Equipamento.all,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: actions.save,
    clearResposta: actions.clearResposta,
    fetchAllFazendas: actionsFazenda.fetchAll,
    fetchAllEquipamentos: actionsEquipamento.fetchAll,
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(FormOrdemServico);
