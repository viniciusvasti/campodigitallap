import React from 'react';
import Expo from 'expo';
import { StackNavigator } from 'react-navigation';
import { Root } from 'native-base';
import { Provider } from 'react-redux';
import LoginForm from './app/containers/Login';
import AppLoading from './app/containers/Login/AppLoading';
import FormApontamentoChuva from './app/containers/ApontamentoChuva/FormApontamentoChuva';
import FormAplicacaoInsumo from './app/containers/AplicacaoInsumo/FormAplicacaoInsumo';
import FormApontamentoArea from './app/containers/ApontamentoArea/FormApontamentoArea';
import FormApontamentoAreaPreparo
  from './app/containers/ApontamentoAreaPreparo/FormApontamentoAreaPreparo';
import FormApontamentoAtividadeMecanizada
  from './app/containers/ApontamentoAtividadeMecanizada/FormApontamentoAtividadeMecanizada';
import FormApontamentoAtividadesManuais
  from './app/containers/ApontamentoAtividadesManuais/FormApontamentoAtividadesManuais';
import FormApontamentoCombustivel
  from './app/containers/ApontamentoCombustivel/FormApontamentoCombustivel';
import FormApontamentoLubrificante
  from './app/containers/ApontamentoLubrificante/FormApontamentoLubrificante';
import FormApontamentoMoagemDiaria
  from './app/containers/ApontamentoMoagemDiaria/FormApontamentoMoagemDiaria';
import FormApontamentoPlantio from './app/containers/ApontamentoPlantio/FormApontamentoPlantio';
import FormApontamentoTerceirosMecanizado
  from './app/containers/ApontamentoTerceirosMecanizado/FormApontamentoTerceirosMecanizado';
import FormFechamentoCorte from './app/containers/FechamentoCorte/FormFechamentoCorte';
import FormOrdemCorte from './app/containers/OrdemCorte/FormOrdemCorte';
import FormOrdemServico from './app/containers/OrdemServico/FormOrdemServico';
import FormOcorrencia from './app/containers/Ocorrencia/FormOcorrencia';
import PDFFile from './app/containers/MapasTalhao/PDFFile';
import { store } from './app/redux/store';
import Home from './app/containers/Home';
import { getTheme } from './app/config/styles';

global.theme = global.theme || 'light';

const theme = getTheme();
// https://reactnavigation.org/docs/getting-started.html
const AuthStack = StackNavigator(
  { Login: LoginForm },
  {
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
        backgroundColor: theme.primary,
      },
    }
  }
);

const AppStack = StackNavigator(
  {
    Inicio: Home,
    LoginForm,
    FormApontamentoChuva: { screen: FormApontamentoChuva },
    FormAplicacaoInsumo,
    FormApontamentoArea,
    FormApontamentoAreaPreparo,
    FormApontamentoAtividadeMecanizada,
    FormApontamentoAtividadesManuais,
    FormApontamentoCombustivel,
    FormApontamentoLubrificante,
    FormApontamentoMoagemDiaria,
    FormApontamentoPlantio,
    FormApontamentoTerceirosMecanizado,
    FormFechamentoCorte,
    FormOrdemCorte,
    FormOrdemServico,
    FormOcorrencia,
    PDFFile,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
        backgroundColor: theme.primary,
      },
      headerTitleStyle: {
        color: theme.light,
      },
    }
  }
);

const SwitchNavigator = StackNavigator(
  {
    AppLoading,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AppLoading',
  }
);

// const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
// const store = createStoreWithMiddleware(reducers);

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isReady: true
    };
  }
  
  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('native-base/Fonts/Ionicons.ttf')
    });
    this.setState({ isReady: true });
  }

  componentWillUnmount() {
    this.setState({ isReady: false });
  }

  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    
    return (
      <Root>
        <Provider store={store}>
          <SwitchNavigator />
        </Provider>
      </Root>
    );
  }
}

export default App;
